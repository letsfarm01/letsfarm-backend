const configs = {
    MONGO_OPTIONS: {
        useNewUrlParser: true,
        autoIndex: false, // Don't build indexes
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        reconnectInterval: 500, // Reconnect every 500ms
        poolSize: 10, // Maintain up to 10 socket connections
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0
    },
    MCLOUD_OPTIONS: {
        retryWrites: true,
        w: 'majority',
        useNewUrlParser: true
    },
    // useUnifiedTopology: true,
    VERSION: 'v1',
    BASE_URL: '/api/',
    SECRETENTITY: '4sW_udEk5FMqHZGsrPiykafaa973757067sW_uddEk5FsW_udEk5FMqHZGsrPiykafaaMqHZGsrPiykafaatjqebjf',
    ACTION_KEY: '12384-09875poiuyty-987to890pojrt9-2976tred;sdfguytr08j-987to890pojrt9-2tred;sdfguytr08j-987to890pojrt9-2tred;sdfguytr08j-arokoyuolalekan',
    SYSTEM_KEY: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwZXJtaXNzaW9uIjoiTEVWRUxfT05FIiwicm9sZSI6IkFETUlOIiwicGFzc3dvcmQiOm51bGwsImFjY2Vzc19zdGF0dXMiOiJBTExPV0VEIiwiX2lkIjoiNWJkZWVjNzY1YjI5NDkwNDUwOTA0YzdlIiwic3RhZmZfaWQiOiIyMzQiLCJmaXJzdF9uYW1lIjoiT2xhbGVrYW4iLCJsYXN0X25hbWUiOiJBcm9rb3l1IiwiZW1haWwiOiJhcm9rb3l1b2xhbGVrYW5AZ21haWwuY29tIiwiY3JlYXRlZEF0IjoiMjAxOC0xMS0wNFQxMjo1NjoyMy4zMjZaIiwidXBkYXRlZEF0IjoiMjAxOC0xMS0wNFQxMjo1NjoyMy4zMjZaIiwiX192IjowLCJzZWNyZXRUb2tlbiI6IkdlT09IaUhZfjdJeF9lN3pKVHhseiIsImFjY2Vzc0tleSI6IjEyMzg0LTA5ODc1cG9pdXl0eS05ODd0bzg5MHBvanJ0OS0yOTc2dHJlZDtzZGZndXl0cjA4ai05ODd0bzg5MHBvanJ0OS0ydHJlZDtzZGZndXl0cjA4ai05ODd0bzg5MHBvanJ0OS0ydHJlZDtzZGZndXl0cjA4ai1hcm9rb3l1b2xhbGVrYW4iLCJpYXQiOjE1NDIyNzc2OTcsImV4cCI6MTU0MjMyMDg5N30.rqXYoyMRWggS2RiIsyt_w2OSiL9SAVwRm8mAxHkkSl0`,
    EMAIL_VALIDATION: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/


};
module.exports = configs;

// accessKey: 'D1XKQQ4PNG0CIPKN03KQBK0BQHOIA9MH',
