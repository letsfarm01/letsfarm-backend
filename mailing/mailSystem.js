const respHandler = require('../services/responseHandler');
const config = require('../config/constants');
const winston = require('../config/winston');
const templates = require('../config/email-template');
const EmailTemplates = require("swig-email-templates");
const appRoot = require("app-root-path") ;
const MailJet = require('node-mailjet').connect(process.env.MAILJET_APIKEY, process.env.MAILJET_APISECRET);
const sendEmail = MailJet.post('send', {'version': 'v3.1'});
let swig_templates = new EmailTemplates({ root: `${appRoot}/templates` });

const mailController = {
    sendText: (to, subject, text, cc=null) => {
        return new Promise((resolve, reject) => {
            const request = sendEmail.request({
                "Messages":[
                    {
                        "From": {
                            "Email": `${process.env.MAILJET_USER_EMAIL}`,
                            "Name": `${process.env.MAILJET_USER_FULLNAMES}`
                        },
                        "To": [
                            {
                                "Email": to.email,
                                "Name": to.full_name
                            }
                        ],
                        "Subject": subject,
                        "TextPart": text,

                        // "Cc": cc ? `${cc},${process.env.MAILJET_ADMIN_EMAIL}` : `${process.env.MAILJET_ADMIN_EMAIL}`
                    }
                ]
            });

            request
                .then((result) => {
                    console.log(result.body);
                    resolve(result.body);
                })
                .catch((err) => {
                    console.log(err.statusCode);
                    winston.logger.error(`Error while sending mail`, err.toString());
                    reject(err);
                })
        });
    },
    sendHtml: (to, subjectParam, templateName=null, context, cc=null) => {
        // tslint:disable-next-line
        // console.log("templateEmail", cc);
        const CC = [
            {
                "Email": process.env.MAILJET_ADMIN_EMAIL
            },
            {
                "Email": "info@letsfarm.co"
            }
        ];
        if(cc){
            // console.log('Constructor ', cc.constructor.name);
            if(cc.constructor.name === 'String') {
                CC.push({ "Email": cc });
                // console.log('CC NEW ', CC);

            } else if(cc.constructor.name === 'Array') {
                for(let i of cc) {
                    CC.push({"Email": cc[i]});
                }
                // console.log('CC NEW 2', CC);
            } else {
                // do nothing
            }
        }
        if (!templateName) return mailController.sendText(to, subjectParam, context, CC);
        // console.log('CC ', CC);
        return new Promise((resolve, reject) => {
            swig_templates.render(
                `${templateName}.html`,
                { ...context,
                    logoUrl: "https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/logo%2FLetsfarm%20Logo.png?alt=media&token=5035eeb2-d137-4532-b074-98a6c2704f07",
                },
                (templateErr, html, text, _subject) => {
                    if (templateErr) {
                        winston.logger.error(`Emailing service issue `, templateErr.toString());
                        resolve(templateErr);
                    }
                    const request = sendEmail.request({
                            "Messages":[
                                {
                                    "From": {
                                        "Email": `${process.env.MAILJET_USER_EMAIL}`,
                                        "Name": `${process.env.MAILJET_USER_FULLNAMES}`
                                    },
                                    "To": [
                                        {
                                            "Email": to.email,
                                            "Name": to.full_name
                                        }
                                    ],
                                    "Cc": CC,
                                    "Subject": subjectParam,
                                    "TextPart": text,
                                    "HTMLPart": html
                                }
                            ]
                        });

                    request
                        .then((result) => {
                            console.log('EMAIL SENDER ', result.body);
                            resolve(result.body);
                        })
                        .catch((err) => {
                            console.log(err.message);
                            winston.logger.error(`Error while sending mail${err.toString()}`);
                            reject(err);
                        })
                });
        });
    },
    sendHtmlManyTo: (to, subjectParam, templateName=null, context, cc=null) => {
        // tslint:disable-next-line
        // console.log("templateEmail", cc);
        const CC = [
            {
                "Email": process.env.MAILJET_ADMIN_EMAIL
            },
            {
                "Email": "info@letsfarm.co"
            }
        ];
        if(cc){
            // console.log('Constructor ', cc.constructor.name);
            if(cc.constructor.name === 'String') {
                CC.push({ "Email": cc });
                // console.log('CC NEW ', CC);

            } else if(cc.constructor.name === 'Array') {
                for(let i of cc) {
                    CC.push({"Email": cc[i]});
                }
                // console.log('CC NEW 2', CC);
            } else {
                // do nothing
            }
        }
        if (!templateName) return mailController.sendText(to, subjectParam, context, CC);
        // console.log('CC ', CC);
        return new Promise((resolve, reject) => {
            swig_templates.render(
                `${templateName}.html`,
                { ...context,
                    logoUrl: "https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/logo%2FLetsfarm%20Logo.png?alt=media&token=5035eeb2-d137-4532-b074-98a6c2704f07",
                },
                (templateErr, html, text, _subject) => {
                    if (templateErr) {
                        winston.logger.error(`Emailing service issue `, templateErr.toString());
                        resolve(templateErr);
                    }
                    const request = sendEmail.request({
                            "Messages":[
                                {
                                    "From": {
                                        "Email": `${process.env.MAILJET_USER_EMAIL}`,
                                        "Name": `${process.env.MAILJET_USER_FULLNAMES}`
                                    },
                                    "To": to,
                                    "Cc": CC,
                                    "Subject": subjectParam,
                                    "TextPart": text,
                                    "HTMLPart": html
                                }
                            ]
                        });

                    request
                        .then((result) => {
                            console.log('EMAIL SENDER ', result.body);
                            resolve(result.body);
                        })
                        .catch((err) => {
                            console.log(err.message);
                            winston.logger.error(`Error while sending mail`, err.toString());
                            reject(err);
                        })
                });
        });
    }
};
module.exports = mailController;
