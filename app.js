const express = require('express');
const dotenv = require('dotenv').config();
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const config = require('./config/constants');
const winston = require('./config/winston');
const cronJob = require('./services/cronJob');
const helmet = require('helmet');
const rateLimit = require("express-rate-limit");
const Sentry = require('@sentry/node');
const compression = require('compression');
Sentry.init({ dsn: process.env.SENTRY });

const app = express();
// compress responses
app.use(compression());
app.use(helmet());
app.use(Sentry.Handlers.requestHandler());
app.set('trust proxy', 1);
if (dotenv.error) {
    winston.logger.log('error', `Error with dotenv`);
    throw dotenv.error;
} else {
    /**
     * Connection begins
     */
    const dbName = process.env.NODE_ENV === 'development' ? process.env.DB_NAME_DEV : process.env.DB_NAME_PROD;
    const connectionString = process.env.NODE_ENV === 'development' ? process.env.CONNECTION_STRING : process.env.CONNECTION_STRING_PROD;
    console.log('DB NAME =>  ', dbName);
    mongoose.set('useFindAndModify', false);
    mongoose.set("useCreateIndex", true);
    mongoose.connect(connectionString,{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error, unable to connect to database!'));
    db.once('open', () => {
        // connected
        console.log('Connect to server');
        winston.logger.log('info', `Connected correctly to server  ${dbName}`);
    });
// Connection ends here.

    // CronJob connector
    cronJob.runCronJobs();
}


const indexRouter = require('./routes/index');
const authRouter = require('./routes/auth');
const usersRouter = require('./routes/users');
const newsletterRouter = require('./routes/newsletter');
const contactUsRouter = require('./routes/contact-us');
const faqRouter = require('./routes/faq');
const farmStatusRouter = require('./routes/farm-status');
const farmTypeRouter = require('./routes/farm-type');
const farmShopRouter = require('./routes/farm-shop');
const farmProductRouter = require('./routes/farm-product');
const testimonialRouter = require('./routes/testimonial');
const eduCategoryRouter = require('./routes/edu-category');
const educationRouter = require('./routes/education');
const educationLikeRouter = require('./routes/education-like');
const blogLikeRouter = require('./routes/blog-like');
const blogCategoryRouter = require('./routes/blog-category');
const blogRouter = require('./routes/blog');
const educationCommentRouter = require('./routes/education-comment');
const blogCommentRouter = require('./routes/blog-comment');
const cartRouter = require('./routes/cart');
const dashboardRouter = require('./routes/dashboard');
const paymentChannelRouter = require('./routes/payment-channel');
const checkoutRouter = require('./routes/checkout-cart');
const profileRouter = require('./routes/profile');
const sponsorRouter = require('./routes/sponsorship');
const bankRouter = require('./routes/banks');
const transactionRouter = require('./routes/transaction');
const walletTransactionRouter = require('./routes/wallet-transaction');
const walletRouter = require('./routes/wallet');
const fundWalletRouter = require('./routes/make-payment');
const orderRouter = require('./routes/order');
const productOrderRouter = require('./routes/product-order');
const teamGroupRouter = require('./routes/team-group');
const teamMemberRouter = require('./routes/team-members');
const accountManagerRouter = require('./routes/account-manager');
const archiveSponsorRouter = require('./routes/achive-sponsor');
const referralRouter = require('./routes/referral');
const productTypeRouter = require('./routes/product-type');
const productStatusRouter = require('./routes/product-status');
const productCheckoutRouter = require('./routes/product-management');
const addressBookRouter = require('./routes/shipping_address');
const completedSponsorRouter = require('./routes/completed-sponsor');
const withdrawalRequestRouter = require('./routes/withdrawal-request');
const updatesRouter = require('./routes/updates');
const repayRouter = require('./routes/repay');




app.use(logger('dev'));
app.use(logger("combined", { stream: winston.Winston.stream }));
app.use(express.json());
// app.use(dotenv);
// app.use(bodyParser.json());

// so we can get the client's IP address
app.enable("trust proxy");
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: "999091990mb"}));
app.use(bodyParser.urlencoded({limit: "9999091990mb", extended: true, parameterLimit:90000}));
app.use(fileUpload({
    limits: { fileSize: 999909050 * 1024 * 1024 },
}));

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 150 // limit each IP to 150 requests per windowMs
});

//  apply to all requests
app.use(limiter);


app.use( (req, res, next) => {
    /*   const allowedOrigins = ['https://letsfarm.com.ng', 'http://test.letsfarm.com.ng', 'http://localhost'];
     const origin = req.headers.origin;
     if(allowedOrigins.indexOf(origin) > -1){
         res.setHeader('Access-Control-Allow-Origin', origin);
     }*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, PATCH, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Letsfarm, Stamp");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Cache-Control', 'max-age=0, no-cache, must-revalidate, proxy-revalidate, private');
    res.set("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    res.set("Pragma", "no-cache");
    next();
});

app.use(`${config.BASE_URL}${config.VERSION}/swagger-doc-letsfarm`, swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// app.use(`/`, indexRouter);
app.use(`${config.BASE_URL}${config.VERSION}/`, indexRouter);
app.use(`${config.BASE_URL}${config.VERSION}/auth`, authRouter);
app.use(`${config.BASE_URL}${config.VERSION}/users`, usersRouter);
app.use(`${config.BASE_URL}${config.VERSION}/newsletter`, newsletterRouter);
app.use(`${config.BASE_URL}${config.VERSION}/contact-us`, contactUsRouter);
app.use(`${config.BASE_URL}${config.VERSION}/faq`, faqRouter);
app.use(`${config.BASE_URL}${config.VERSION}/farm-status`, farmStatusRouter);
app.use(`${config.BASE_URL}${config.VERSION}/product-status`, productStatusRouter);
app.use(`${config.BASE_URL}${config.VERSION}/farm-type`, farmTypeRouter);
app.use(`${config.BASE_URL}${config.VERSION}/product-type`, productTypeRouter);
app.use(`${config.BASE_URL}${config.VERSION}/farm-shop`, farmShopRouter);
app.use(`${config.BASE_URL}${config.VERSION}/testimonial`, testimonialRouter);
app.use(`${config.BASE_URL}${config.VERSION}/education-category`, eduCategoryRouter);
app.use(`${config.BASE_URL}${config.VERSION}/education`, educationRouter);
app.use(`${config.BASE_URL}${config.VERSION}/education-like`, educationLikeRouter);
app.use(`${config.BASE_URL}${config.VERSION}/blog-like`, blogLikeRouter);
app.use(`${config.BASE_URL}${config.VERSION}/blog`, blogRouter);
app.use(`${config.BASE_URL}${config.VERSION}/blog-category`, blogCategoryRouter);
app.use(`${config.BASE_URL}${config.VERSION}/blog-comment`, blogCommentRouter);
app.use(`${config.BASE_URL}${config.VERSION}/education-comment`, educationCommentRouter);
app.use(`${config.BASE_URL}${config.VERSION}/cart`, cartRouter);
app.use(`${config.BASE_URL}${config.VERSION}/dashboard`, dashboardRouter);
app.use(`${config.BASE_URL}${config.VERSION}/payment-channel`, paymentChannelRouter);
app.use(`${config.BASE_URL}${config.VERSION}/checkout-cart`, checkoutRouter);
app.use(`${config.BASE_URL}${config.VERSION}/product-checkout`, productCheckoutRouter);
app.use(`${config.BASE_URL}${config.VERSION}/profile`, profileRouter);
app.use(`${config.BASE_URL}${config.VERSION}/sponsorship`, sponsorRouter);
app.use(`${config.BASE_URL}${config.VERSION}/bank`, bankRouter);
app.use(`${config.BASE_URL}${config.VERSION}/transaction`, transactionRouter);
app.use(`${config.BASE_URL}${config.VERSION}/wallet-transaction`, walletTransactionRouter);
app.use(`${config.BASE_URL}${config.VERSION}/wallet`, walletRouter);
app.use(`${config.BASE_URL}${config.VERSION}/fund-wallet`, fundWalletRouter);
app.use(`${config.BASE_URL}${config.VERSION}/orders`, orderRouter);
app.use(`${config.BASE_URL}${config.VERSION}/product-order`, productOrderRouter);
app.use(`${config.BASE_URL}${config.VERSION}/team-group`, teamGroupRouter);
app.use(`${config.BASE_URL}${config.VERSION}/team-member`, teamMemberRouter);
app.use(`${config.BASE_URL}${config.VERSION}/contact-account-manager`, accountManagerRouter);
app.use(`${config.BASE_URL}${config.VERSION}/archive-sponsor`, archiveSponsorRouter);
app.use(`${config.BASE_URL}${config.VERSION}/referral`, referralRouter);
app.use(`${config.BASE_URL}${config.VERSION}/address-book`, addressBookRouter);
app.use(`${config.BASE_URL}${config.VERSION}/farm-product`, farmProductRouter);
app.use(`${config.BASE_URL}${config.VERSION}/completed-sponsor`, completedSponsorRouter);
app.use(`${config.BASE_URL}${config.VERSION}/withdrawal-request`, withdrawalRequestRouter);
app.use(`${config.BASE_URL}${config.VERSION}/updates`, updatesRouter);
app.use(`${config.BASE_URL}${config.VERSION}/repay`, repayRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const msg = 'This resources you are trying to access is not found on this server.';
    const err = new Error(msg);
    err.status = 'FAILURE';
    err.code = 404;
    err.msg = msg;
    res.status(404).send(err);
});

// error handler
app.use((err, req, res, next) => {
    const error = new Error('Internal server Error');
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500).send(error + err);
});

module.exports = app;
