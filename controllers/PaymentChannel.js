const PaymentChannel = require('../models/PaymentChannel');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const PaymentChannelController = {
    getPaymentChannels: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'role', (role) => {
                let cond = {};
                const {can_fund_wallet} = req.query;
                if(role.toLowerCase() !== 'super') {
                    cond = {is_active: true};
                    if(can_fund_wallet === 'true') {
                        cond = {$and: [{is_active: true}, {can_fund: true}]};
                    }
                }
                console.log('cond ', cond, req.query);
                PaymentChannel.find(cond)
                    .exec((err, paymentChannels) => {
                        if (err) {
                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment channels.', err);
                        } else if (validate.resultFound(paymentChannels, res)) {
                            const data = validate.formatData(paymentChannels);
                            respHandler.sendSuccess(res, 200, 'Payment channel listed successfully', data);
                        } else {
                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment channels.', err);
                        }
                    })
            });
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment channels.', err);
        }
    },
    createPaymentChannel: (req, res) => {
        try {
            PaymentChannel.create(req.body, (error, paymentChannel) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new payment channel.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Payment channel added successfully', paymentChannel);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new payment channel.', err);
        }
    },
    getPaymentChannel: (req, res) => {
        try {
            PaymentChannel.findById(req.params.id).exec((err, paymentChannel) => {
                if (err || !paymentChannel){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get payment Channel by id.', err);
                } else if (validate.resultFound(paymentChannel, res)) {
                    const data = validate.formatData(paymentChannel);
                    respHandler.sendSuccess(res, 200, 'Payment Channel fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get payment Channel.', err);
        }
    },
    putPaymentChannel: (req, res) => {
        try {
            console.log('This is body ', req.body);
            PaymentChannel.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, paymentChannel) => {
                if (err || !paymentChannel){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update payment Channel.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Payment Channel updated successfully!', paymentChannel);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update payment Channel');
        }
    },
    deletePaymentChannel: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                PaymentChannel.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete payment Channel.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Payment Channel deleted by id successfully!', {});
                });
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete payment Channel by id');
        }
    },
};

module.exports = PaymentChannelController;