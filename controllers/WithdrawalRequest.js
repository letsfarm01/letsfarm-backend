const WithdrawalRequest = require('../models/WithdrawalRequest');
const User = require('../models/Users');
const WalletTransaction = require('../models/WalletTransaction');
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const BankSetting = require('../models/BankSetting');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const paystackService = require('../services/paystackServices');
const bcrypt = require('bcrypt');
const winston = require('../config/winston');
const nanoid = require('nanoid');
const moment = require('moment');
const mailer = require('../mailing/mailSystem');

const WithdrawalRequestController = {
    getWithdrawalRequests: (req, res) => {
        try {
            const { page, limit, status } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 900000;
            let cond = {};
            if(status && status !== 'ALL') {
                cond = {status}
            } else if(!status) {
                cond = {status: {$ne: 'APPROVED'}}
            }
            WithdrawalRequest.find(cond)
                .populate('userId', 'first_name last_name email full_name phone_number')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .sort('-createdAt')
                .exec((err, requests) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list withdrawal request.', err);
                    } else if (validate.resultFound(requests, res)) {
                        const data = validate.formatData(requests);
                        respHandler.sendSuccess(res, 200, 'Withdrawal requests listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list withdrawal requests.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list withdrawal request.', err);
        }
    },

    newWithdrawalRequest: async (req, res) => {
        try {
            if(!req.body.password || !req.body.userId || !req.body.amount) {
                respHandler.sendError(res, 401, 'FAILURE', 'Not permitted to this resource', {});
            } else {
                const password = req.body.password;
                const user = await User.findById(req.body.userId).exec();
                if(user) {
                    bcrypt.compare(password, user.password, (err, response) => {
                        if(!response) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Incorrect account password', err);
                        } else {
                            Wallet.findOne({userId: user._id}, (wError, wallet) => {
                                if(wError || !wallet) {
                                    respHandler.sendError(res, 400, 'FAILURE', 'No valid wallet, contact admin!!', wError);
                                } else if(parseFloat(req.body.amount) > parseFloat(wallet.wallet_balance) ) {
                                    respHandler.sendError(res, 400, 'FAILURE', 'Insufficient wallet balance!', {});
                                } else {
                                    BankSetting.findOne({userId: user._id}, (bankError, bankDetails) => {
                                        if(bankError || !bankDetails) {
                                            respHandler.sendError(res, 400, 'FAILURE', 'No valid bank account details, set bank details on profile!', bankError);
                                        } else {
                                            if(bankDetails && (!bankDetails.account_number || !bankDetails.bank_name || !bankDetails.verified)) {
                                                respHandler.sendError(res, 400, 'FAILURE', 'Incomplete bank account details, set bank details on profile!', {});
                                            } else {
                                                paystackService.checkTransferRecipient(res, bankDetails, () => {
                                                    WithdrawalRequest.create({
                                                        userId: user._id,
                                                        amount: parseFloat(req.body.amount),
                                                        wallet_version_before_approval: wallet,
                                                        bank_name: bankDetails.bank_name,
                                                        account_number: bankDetails.account_number,
                                                        account_name: bankDetails.account_name,
                                                        bvn: bankDetails.bvn
                                                    }, (wRError, withdrawalRequest) => {
                                                        if(wRError) {
                                                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to request for withdrawal at the moment, contact admin!', wRError);
                                                        } else {
                                                            const newBalance = parseFloat(wallet.wallet_balance) - parseFloat(req.body.amount);
                                                            Wallet.findOneAndUpdate({userId: user._id}, {$set: {wallet_balance: newBalance}},
                                                                {new: true}, (e, walletSuc) => {
                                                                    if(e) {
                                                                        winston.logger.log('error', `Error while updating wallet balance - ${JSON.stringify(e)} --- Wallet fund withdraw request`);
                                                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet balance', e);
                                                                    } else {
                                                                        respHandler.sendSuccess(res, 200, 'Withdrawal request sent successful', {});
                                                                    }
                                                                });

                                                        }
                                                    });
                                                });
                                            }
                                        }

                                    });
                                }
                            });

                        }});
                } else {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to find user', {});
                }
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to send withdrawal request.', err);
        }
    },
    acceptWithdrawalRequest: async (req, res) => {
        try {
            const {requestId, userId} = req.body;
            const _user = await User.findById(userId).exec();

            Wallet.findOne({userId: userId}, (eWallet, wallet) => {
                if(eWallet || !wallet) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to find user wallet', eWallet);
                } else {
                    BankSetting.findOne({userId: userId}).exec((bankError, bankSetting) => {
                        if(bankError || !bankSetting) {
                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to find user bank details', bankError);
                        } else {
                            WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'APPROVED', approvalTime: moment.now(), new_version_after_approval: wallet}}, { new: true }, (err, withdrawalRequest) => {
                                if (err || !withdrawalRequest){
                                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to reject withdrawal request!.', err);
                                } else {
                                    paystackService.transferMoneyToBankAccount({
                                        narration: `Withdrawal from wallet by ${_user && _user.full_name} on ${moment.now()}`,
                                        transfer_recipient: bankSetting.transfer_recipient,
                                        withdrawalRequest: withdrawalRequest._id,
                                        amount: withdrawalRequest.amount},
                                        (paystackError)=> {
                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to withdraw money from paystack', paystackError);
                                    },
                                        (pResponse, pData) => {
                                        Transaction.create({orderReference: `WFW-${controllerService.getRandomInt(999999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`,
                                            total_price: withdrawalRequest.amount,
                                            payment_channel: process.env.NODE_ENV === 'production' ? '5edbfc6cd04e430017f89318' : '5edc0b6bc9567500173af058',
                                            userId: userId,
                                            narration: 'Withdraw from wallet for withdrawal request - ' + withdrawalRequest._id
                                        } , (tErr, transaction) => {
                                            if(tErr){
                                                winston.logger.log('error', `Error: unable to create transaction - ${JSON.stringify(tErr)} --- Transaction table`);
                                            } else {
                                                const newBalance = parseFloat(withdrawalRequest.wallet_version_before_approval.wallet_balance) - parseFloat(withdrawalRequest.amount);
                                                WalletTransaction.create({
                                                    narration: 'Withdrawal to bank account on ' + moment.now(),
                                                    type: 'DEBIT',
                                                    new_wallet_balance: newBalance,
                                                    old_wallet_balance: withdrawalRequest.wallet_version_before_approval.wallet_balance,
                                                    amount: withdrawalRequest.amount,
                                                    transactionId: transaction._id,
                                                    userId: userId
                                                }, (err_, walletTrans) => {
                                                    if(err_) {
                                                        console.log('Error ', err_);
                                                        winston.logger.log('info', `Error: unable to create wallet transaction - ${JSON.stringify(err_)} --- Wallet transaction table`);
                                                        respHandler.sendSuccess(res, 200, pResponse || 'Withdrawal from wallet request approved successfully!', pData);
                                                    } else {
                                                        winston.logger.log('info', `Withdrawal request sent to paystack queue`);
                                                        respHandler.sendSuccess(res, 200, pResponse || 'Withdrawal from wallet request approved successfully!', pData);
                                                    }
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            });

        } catch(err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to approve withdrawal request', err);
        }
    },
    manualApproveWithdrawalRequest: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const {requestId} = req.body;
            WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'APPROVED', approvalTime: moment.now()}}, { new: true }, (err, withdrawalRequest) => {
                if (err || !withdrawalRequest){
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to approve withdrawal request!.', err);
                } else {
                    Wallet.findOne({userId: withdrawalRequest.userId}, (wError, wallet) => {
                        if(wError || !wallet) {
                            WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'PENDING'}}, { new: true }, () => {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to approve withdrawal request!.', wError);
                            });
                            } else {
                            Transaction.create({orderReference: `M-WFW-${controllerService.getRandomInt(999999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`,
                                total_price: withdrawalRequest.amount,
                                payment_channel: process.env.NODE_ENV === 'production' ? '5edbfc9cd04e430017f8931a' : '5edc0b8ec9567500173af05a',
                                userId: withdrawalRequest.userId,
                                narration: 'Withdraw from wallet for withdrawal request - ' + withdrawalRequest._id
                            } , (tErr, transaction) => {
                                if(tErr){
                                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to create transaction for withdrawal request!.', e);
                                    winston.logger.log('error', `Error: unable to create transaction - ${JSON.stringify(tErr)} --- Transaction table`);
                                } else {
                                    const newBalance = parseFloat(withdrawalRequest.wallet_version_before_approval.wallet_balance) - parseFloat(withdrawalRequest.amount);
                                    WalletTransaction.create({
                                        narration: 'Withdrawal to bank account on ' + moment.now(),
                                        type: 'DEBIT',
                                        new_wallet_balance: newBalance,
                                        old_wallet_balance: withdrawalRequest.wallet_version_before_approval.wallet_balance,
                                        amount: withdrawalRequest.amount,
                                        transactionId: transaction._id,
                                        userId: withdrawalRequest.userId
                                    }, (err_, walletTrans) => {
                                        if(err_) {
                                            console.log('Error ', err_);
                                            winston.logger.log('info', `Error: unable to create wallet transaction - ${JSON.stringify(err_)} --- Wallet transaction table`);
                                            sendMail(withdrawalRequest);
                                            respHandler.sendSuccess(res, 200, 'Withdrawal from wallet request approved successfully!, but unable to create a wallet transaction', walletTrans);
                                        } else {
                                            winston.logger.log('info', `Withdrawal request approved successfully!`);
                                            sendMail(withdrawalRequest);
                                            respHandler.sendSuccess(res, 200, 'Withdrawal from wallet request approved successfully!', walletTrans);
                                        }
                                    });
                                }
                            });

                        }
                    });
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to approve withdrawal request');
        }
    },
    rejectWithdrawalRequest: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const {requestId} = req.body;
            WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'REJECTED', rejectTime: moment.now()}}, { new: true }, (err, withdrawalRequest) => {
                if (err || !withdrawalRequest){
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to reject withdrawal request!.', err);
                } else {

                    Wallet.findOne({userId: withdrawalRequest.userId}, (wError, wallet) => {
                        if(wError || !wallet) {
                            WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'PENDING'}}, { new: true }, (err, withdrawalRequest) => {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to reject withdrawal request!.', wError);
                            });
                            } else {
                            const newBalance = parseFloat(wallet.wallet_balance) + parseFloat(withdrawalRequest.amount);
                            Wallet.findOneAndUpdate({userId: withdrawalRequest.userId}, {$set: {wallet_balance: newBalance}},
                                {new: true}, (e, walletSuc) => {
                                if(e || !walletSuc) {
                                    WithdrawalRequest.findByIdAndUpdate(requestId, {$set: {status: 'PENDING'}}, { new: true }, (err, withdrawalRequest) => {
                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to reject withdrawal request!.', e);
                                    });
                                } else {
                                    User.findById(withdrawalRequest.userId, (e, user) => {
                                        if(e || !user) {} else {
                                            mailer.sendHtml({email: user.email, full_name: user.full_name},
                                                'Request to withdraw rejected', 'WithdrawRejected', {
                                                    full_name: user.full_name, title: 'Request to withdraw rejected' });
                                        }
                                        respHandler.sendSuccess(res, 200, 'Withdrawal request rejected successfully!', withdrawalRequest);
                                    });
                                }
                                });
                        }
                    });
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to reject withdrawal request');
        }
    },
    getWithdrawalRequest: (req, res) => {
        try {
            WithdrawalRequest.findById(req.params.id).exec((err, withdrawalRequest) => {
                if (err || !withdrawalRequest){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get withdrawal request by id.', err);
                } else if (validate.resultFound(withdrawalRequest, res)) {
                    const data = validate.formatData(withdrawalRequest);
                    respHandler.sendSuccess(res, 200, 'Withdrawal request fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get withdrawal request.', err);
        }
    },
    putWithdrawalRequest: (req, res) => {
        try {
            console.log('This is body ', req.body);
            WithdrawalRequest.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, withdrawalRequest) => {
                if (err || !withdrawalRequest){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update withdrawal request.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Withdrawal request updated successfully!', withdrawalRequest);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update withdrawal request');
        }
    }
};

module.exports = WithdrawalRequestController;
function sendMail(withdrawalRequest) {
    User.findById(withdrawalRequest.userId, (e, user) => {
        if(e || !user) {} else {
            mailer.sendHtml({email: user.email, full_name: user.full_name},
                'Request to withdraw approved!', 'WithdrawApproved', {
                    full_name: user.full_name, amount: 'NGN ' + withdrawalRequest.amount, title: 'Request to withdraw approved' });
        }
    });
}