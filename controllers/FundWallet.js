const PaymentChannel = require('../models/PaymentChannel');
const PaystackReference = require('../models/PaystackReference');
const BankPaymentProof = require('../models/BankPaymentProof');
const User = require('../models/Users');
const WalletTransaction = require('../models/WalletTransaction');
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const respHandler = require('../services/responseHandler');
const controllerService = require('../services/controllerServices');
const nanoid = require('nanoid');
const request = require('request');
const winston = require('../config/winston');
const moment = require('moment');

const FundWalletController = {
    createFundWallet: (req, res) => {
        try {
            const {payment_gateway, amount, userId} = req.body;
            controllerService.getLoginUser(req, 'userId', (_userId) => {
                console.log('USER ', userId, payment_gateway);
                if(userId !== _userId) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with wallet funding, invalid credentials', {});
                } else if(!payment_gateway) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with wallet funding', {});
                } else {
                    PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                        console.log('GATEWAY ', paymentGateway);
                        if(gatewayError) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                        } else if(!paymentGateway.is_active) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                        } else if(!paymentGateway.can_fund) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow to fund wallet!', {});
                        } else if(amount > 10000000) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Amount is beyond allowed limit', {});
                        } else {
                            FundWalletController.proceedToPaymentInit(req, res, paymentGateway);
                        }
                    });
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to fund wallet.', err);
        }
    },
    fundWalletWithExitingCard: (req, res) => {
        try {
            const { payment_gateway, amount, userId, card_id } = req.body;
            controllerService.getLoginUser(req, 'userId', (_userId) => {
                console.log('USER ', userId, payment_gateway);
                if(userId !== _userId) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with wallet funding, invalid credentials', {});
                } else if(!payment_gateway) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with wallet funding', {});
                } else {
                    User.findById(userId, (uError, user) => {
                        if(uError) {
                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to retrieve user', {});
                        } else {
                            PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                                console.log('GATEWAY ', paymentGateway);
                                if(gatewayError) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                                } else if(!paymentGateway.is_active) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                                } else if(!paymentGateway.can_fund) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow to fund wallet!', {});
                                } else if(paymentGateway.name.toLowerCase() !== 'paystack') {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Only paystack is allowed!', {});
                                } else if(!card_id) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Card Id not provided!', {});
                                } else if(amount > 10000000) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Amount is beyond allowed limit', {});
                                } else {
                                    const date = new Date();
                                    const year = date.getFullYear();
                                    // const month = date.getMonth() + 1;
                                    PaystackReference.findOne({$and: [{_id: card_id}, {reusable: true}, {exp_year: {$gte: year}}]},
                                        (cardErr, cardSuc) => {
                                            if(cardErr || !cardSuc) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Invalid payment card selected!', cardErr);
                                            } else {
                                                checkPaymentAuth({
                                                    email: user.email,
                                                    authorization_code: cardSuc.authorization_code,
                                                    amount: amount
                                                }, (error, body, statusCode) => {
                                                    const response = JSON.parse(body);
                                                    if(error){
                                                        respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', error);
                                                    } else if(response.status === false) {
                                                        respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                                                    } else if(statusCode !== 200) {
                                                        respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                                                    } else {
                                                        const generateRef = `${controllerService.getRandomInt(9999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`;
                                                        const narrate = `Funding of Wallet via ${paymentGateway.name}`;
                                                        Transaction.create({orderReference: generateRef,
                                                            total_price: amount,
                                                            status: 'INITIATED',
                                                            payment_channel: paymentGateway._id,
                                                            userId: userId, narration: narrate
                                                        } , (err, transaction) => {
                                                            if(err) {
                                                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to create transaction', err);
                                                            } else if(transaction) {
                                                                User.findById(transaction.userId, (err, user) => {
                                                                    if(err) {
                                                                        respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
                                                                    } else {
                                                                        recurrentPayment({email: user.email, full_name: user.full_name,
                                                                            first_name: user.first_name, last_name: user.last_name,
                                                                            authorization_code: cardSuc.authorization_code,
                                                                            amount: transaction.total_price,
                                                                            transaction: transaction.toJSON()}, (error, body, statusCode) => {
                                                                            if(error){
                                                                                //handle errors
                                                                                console.log('PAYSTACK ERROR', error);
                                                                                respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', error);
                                                                            } else {
                                                                                const response = JSON.parse(body);
                                                                                if(response.status === false) {
                                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', response);
                                                                                } else if(statusCode !== 200) {
                                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', response);
                                                                                } else {
                                                                                    approveWalletFunding(res, transaction, (walletTransaction) => {
                                                                                        respHandler.sendSuccess(res, 200, response.message + ', wallet credited!', {
                                                                                            body: response,
                                                                                            walletTransaction
                                                                                        });
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            } else {
                                                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to process transaction', err);
                                                            }
                                                        });

                                                    }
                                                });
                                            }
                                        });
                                }
                            });
                        }
                    });
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to fund wallet.', err);
        }
    },
    proceedToPaymentInit: (req, res, paymentChannel) => {
        const {amount, userId} = req.body;
        const generateRef = `${controllerService.getRandomInt(9999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`;
        const narrate = `Funding of Wallet via ${paymentChannel.name}`;
        Transaction.create({orderReference: generateRef,
            total_price: amount,
            transactionType: 'WALLET',
            payment_channel: paymentChannel._id,
            userId: userId, narration: narrate
        } , (err, transaction) => {
            if(err) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to generate payment link', err);
            } else if(transaction) {
                // todo: decide on payment URL
                // switch channel
                FundWalletController.decideOnPayment(req, res, transaction, paymentChannel);
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to save transaction record', err);
            }
        });
    },
    decideOnPayment: (req, res, transaction, paymentChannel) => {
        switch (paymentChannel.name.toLowerCase()) {
            case 'paystack': {
                usePayStack(req, res, transaction, paymentChannel);
                break;
            }
            case 'bank transfer': {
                transferToBankAccount(req, res, transaction, paymentChannel);
                break;
            }
            case 'flutterwave': {
                useFlutter(req, res, transaction, paymentChannel);
                break;
            }
            default: {
                usePayStack(req, res, transaction, paymentChannel);
                break;
            }
        }
    },
    verifyPayment: (req, res) => {
        try {
            const transId = req.body.transactionId;
            if(transId) {
                Transaction.findById(transId, (err, transaction) => {
                    if(err) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Transaction does not exist on our system!', err);
                    } else {
                        verifyPayment(transaction.paymentReference, (erro, body, statusCode) => {
                            // console.log('RESPONSE ', body, statusCode, erro);
                            const response = JSON.parse(body);
                            if(erro) {
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', erro);
                            } else if(statusCode !== 200){
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', erro);
                            } else {
                                if(response.data.status === 'success') {
                                    // console.log('AUTHORIZATION ', response.data);
                                    const authorization = response.data.authorization;
                                    approveWalletFunding(res, transaction, (walletTransaction) => {
                                        PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                            respHandler.sendSuccess(res, 200, response.message + ', wallet credited!', {
                                                body: response,
                                                walletTransaction
                                            });
                                        });
                                    });
                                } else {
                                    Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED', paystack_status: response.data.status}}, {new: true}, () => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', response);
                                }
                            }
                        });
                    }
                });
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', {});
            }
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', error);
        }
    },
    confirmBankTransfer: (req, res) => {
        const {transactionId } = req.body;
        Transaction.findByIdAndUpdate(transactionId, {$set: {status: 'CONFIRMED'}}, {new: true}, (error, transaction) => {
            if(error) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to confirm transaction', error);
            } else {
                approveWalletFunding(res,  transaction, (walletTransaction) => {
                    BankPaymentProof.findOneAndUpdate({transactionId: transaction._id}, {$set: { is_confirmed: true }}, {new: true}, (err, bankProofSus) => {   });
                    respHandler.sendSuccess(res, 200, 'Bank transfer Payment completed, wallet credited!', {
                        walletTransaction
                    });
                });

            }
        })
    },
    cancelTransaction: (req, res) => {
        const {transactionId } = req.body;
        Transaction.findByIdAndUpdate(transactionId, {$set: {status: 'CANCELED'}}, {new: true}, (error, transaction) => {
            if(error) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to cancel transaction', error);
            } else {
                BankPaymentProof.findOneAndUpdate({transactionId: transaction._id}, {$set: { is_confirmed: true, condition: 'Transaction Canceled' }}, {new: true}, (err, bankProofSus) => {   });
                respHandler.sendSuccess(res, 200, 'Transaction canceled successfully', {});
            }
        })
    },
    reInitTransaction: (req, res) => {
        const {transactionId } = req.body;
        Transaction.findByIdAndUpdate(transactionId, {$set: {status: 'INITIATED'}}, {new: true}, (error, transaction) => {
            if(error) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to re-initiate transaction', error);
            } else {
                BankPaymentProof.findOneAndUpdate({transactionId: transaction._id}, {$set: { is_confirmed: false, condition: 'Transaction Re-Initiated' }}, {new: true}, (err, bankProofSus) => {   });
                respHandler.sendSuccess(res, 200, 'Transaction re-initiated successfully', {});
            }
        })
    },
    getUserSavedCards: (req, res) => {
        const {userId } = req.params;
        const date = new Date();
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        console.log('DATE ', userId, parseInt(year, 10), parseInt(month, 10));
        PaystackReference.find({$and: [{userId: userId}, {reusable: true}, {exp_year: {$gte: parseInt(year, 10)}}]},
            (error, savedCards) => {
            if(error) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to list saved cards', error);
            } else {
                respHandler.sendSuccess(res, 200, 'Saved cards listed successfully', savedCards);
            }
        })
    },
    deleteSavedCard: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                PaystackReference.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete saved card.', err);
                    }
                    cancelCard(() => {
                        respHandler.sendSuccess(res, 200, 'Card deleted successfully!', {});
                    });
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete card');
        }
    },
};

module.exports = FundWalletController;

function approveWalletFunding(res, transaction, cb=null) {
    Wallet.findOne({userId: transaction.userId}, (walletError, walletSuccess) => {
        if(walletError || !walletSuccess) {
            winston.logger.log('error', `Error: Unable to find wallet data ---- ${JSON.stringify(transaction)}`);
            respHandler.sendError(res, 401, 'FAILURE', 'Unable to credit wallet balance!', walletError);
        } else {
            let wallet_balance = parseFloat(walletSuccess.wallet_balance) || 0;
            let amountToInput  = parseFloat(transaction.total_price) || 0;
            let new_wallet_balance = wallet_balance + amountToInput;
            Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {});
                WalletTransaction.create({
                narration: transaction.narration,
                old_wallet_balance: wallet_balance,
                type: 'CREDIT',
                new_wallet_balance: new_wallet_balance,
                amount: amountToInput,
                userId: transaction.userId,
                transactionId: transaction._id
            }, (wtErr, walletTransaction) => {
                if(wtErr) {
                    winston.logger.log('error', `Error: Unable to save wallet-transaction record ---- ${JSON.stringify(wtErr)} --- ${JSON.stringify(walletTransaction)}`);
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to save wallet-transaction record!', wtErr);
                } else {
                    Wallet.findByIdAndUpdate(walletSuccess._id, {$set: {wallet_balance: new_wallet_balance, last_fund_date: moment.now()}}, {new: true}, (errUpdate, sucUpdate) => {
                        if(errUpdate) {
                            winston.logger.log('error', `Error: Unable to update wallet balance after funding ---- ${JSON.stringify(errUpdate)} --- ${JSON.stringify(walletSuccess)}`);
                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to update wallet balance after funding!', errUpdate);
                        } else {
                            if(cb) {
                                cb(walletTransaction);
                            } else {
                                respHandler.sendSuccess(res, 200, 'Transaction completed, wallet credited!')
                            }
                        }
                    });
                }
            });
        }
    })
}


function usePayStack(req, res, transaction, paymentChannel) {
    User.findById(transaction.userId, (err, user) => {
        if(err) {
            // do nothing
            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
        } else {
            controllerService.calculateTransactionCharge(transaction.total_price, (charge, subTotal, amountToPay) => {
                initializePayment({email: user.email, full_name: user.full_name,
                    first_name: user.first_name, last_name: user.last_name,
                    amount: amountToPay,
                    transaction: transaction.toJSON()}, (error, body, statusCode) => {
                    if(error){
                        //handle errors
                        console.log('PAYSTACK ERROR', error);
                        respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', error);
                    } else {
                        const response = JSON.parse(body);
                        // console.log('BODY ', response, error);
                        if(response.status === false) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                        } else if(statusCode !== 200) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                        } else {
                            Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: response.data.reference}}, {new: true},
                                (errorTransaction, sucTransaction) => {
                                    if(errorTransaction || !sucTransaction) {
                                        // log error
                                        winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                        respHandler.sendError(res, 401, 'FAILURE', 'Error with update transaction!', errorTransaction);
                                    } else {
                                        respHandler.sendSuccess(res, 200, 'Transaction created successfully!', {
                                            authorization_url: response.data.authorization_url,
                                            details: {
                                                charge: charge,
                                                subTotal: subTotal,
                                                total: amountToPay,
                                                transactionId: transaction._id,
                                                full_name: user.full_name,
                                                email: user.email,
                                                ref: response.data.reference,
                                                access_code: response.data.access_code
                                            }
                                        });
                                    }
                                });
                        }
                    }
                });
            });
        }
    });
}
function transferToBankAccount(req, res, transaction, paymentChannel) {
    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: null}}, {new: true},
        (errorTransaction, sucTransaction) => {
            if(errorTransaction) {
                // log error
                winston.logger.log('error', `Error: while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}--bank transfer`);
                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete checkout using bank transfer', errorTransaction);
            } else {
                respHandler.sendSuccess(res, 200, 'Transaction initiated with bank transfer, proceed to payment!', {
                    transactionRef: transaction.orderReference,
                    transactionId: transaction._id,
                    amount: transaction.total_price
                });
            }
        });
}
function useFlutter(req, res, transaction, paymentChannel) {
    respHandler.sendSuccess(res, 200, 'Flutter wave is not setup for payment, please consider using paystack', {});
}

function initializePayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/wallet-history/${data.transaction._id}`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/initialize',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    console.log('OPTIONS ', options);
    const callback = (error, response, body)=>{
        // console.log('Error ', error, response);
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function recurrentPayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        authorization_code: data.authorization_code,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/wallet-history/${data.transaction._id}`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/charge_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}
function checkPaymentAuth(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        authorization_code: data.authorization_code
    };
    const options = {
        url : 'https://api.paystack.co/transaction/check_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function verifyPayment(ref, cb) {
    const options = {
        url : 'https://api.paystack.co/transaction/verify/' + encodeURIComponent(ref),
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }
    };
    const callback = (error, response, body) => {
        return cb(error, body, response.statusCode);
    };
    request.get(options, callback);
}
function cancelCard(cb) {
    const form = {
        authorization_code: encodeURIComponent(data.authorization_code)
    };
    const options = {
        url : 'https://api.paystack.co/customer/deactivate_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }, form
    };
    const callback = (error, response, body) => {
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}