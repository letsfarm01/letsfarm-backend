const TeamMember = require('../models/TeamMember');
const TeamGroup = require('../models/TeamGroup');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');

const TeamMemberController = {
    getTeamMembers: (req, res) => {
        try {
            let result = {};
            TeamGroup.find({})
                .sort({'sort_order': 1})
                .exec((err, teamGroups) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list team members.', err);
                    } else {
                        eachAsync(teamGroups, (teamGroup, i, done) => {
                            result[teamGroup.name] = [];
                            TeamMember.find({teamGroup: teamGroup._id})
                                .populate('teamGroup', '_id name')
                                .exec((error, teamMembers) => {
                                if(error || !teamMembers.length) {
                                    done();
                                } else {
                                    result[teamGroup.name] = teamMembers;
                                    done();
                                }
                            });
                        }, () => {
                            respHandler.sendSuccess(res, 200, 'Team members listed successfully', {result, teamGroups});
                        });
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list team members.', err);
        }
    },
    createTeamMember: (req, res) => {
        try {
            TeamMember.create(req.body, (error, teamMember) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create team member.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Team member created successfully', teamMember);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create team member.', err);
        }
    },
    getTeamMember: (req, res) => {
        try {
            TeamMember.findById(req.params.id).exec((err, teamMember) => {
                if (err || !teamMember){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get team member by id.', err);
                } else if (validate.resultFound(teamMember, res)) {
                    const data = validate.formatData(teamMember);
                    respHandler.sendSuccess(res, 200, 'Team member fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get team member.', err);
        }
    },
    putTeamMember: (req, res) => {
        try {
            console.log('This is body ', req.body);

            TeamMember.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, teamMember) => {
                if (err || !teamMember){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update team member.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Team member updated successfully!', teamMember);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update team member');
        }
    },
    deleteTeamMember: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                TeamMember.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete team member.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Team member deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete team member by id');
        }
    },
};

module.exports = TeamMemberController;