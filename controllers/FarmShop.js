const FarmShop = require('../models/FarmShop');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const FarmShopController = {
    getFarmShops: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await FarmShop.countDocuments({}).exec();
            FarmShop.find({})
                .populate('farm_type')
                .populate('farm_status')
                .exec((err, farmShop) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm shops.', err);
                    } else if (validate.resultFound(farmShop, res)) {
                        // const data = validate.formatData(farmShop);
                        const sorter = farmShop.sort(function(a, b){
                            const textA = a.farm_status.name.toLowerCase();
                            const textB = b.farm_status.name.toLowerCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });
                        // console.log('AAAAAAAA ', sorter, farmShop);
                        const data = {
                            data: validate.formatData(sorter),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Farm shops listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm shops.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm shops.', err);
        }
    },
    createFarmShop: (req, res) => {
        try {
            const newFarmShop = req.body;
            if(newFarmShop && !newFarmShop.farm_name) {
                respHandler.sendError(res, 400, 'FAILURE', 'Farm name is required!.', {});
            } else if(newFarmShop && !newFarmShop.farm_location) {
                respHandler.sendError(res, 400, 'FAILURE', 'Farm Location is required!.', {});
            } else if(newFarmShop && !newFarmShop.percentage_to_gain) {
                respHandler.sendError(res, 400, 'FAILURE', 'Percentage to gain is required!.', {});
            } else if(newFarmShop && newFarmShop.percentage_to_gain <= 0) {
                respHandler.sendError(res, 400, 'FAILURE', 'Percentage to gain is invalid!.', {});
            }  else if(newFarmShop && !newFarmShop.amount_to_invest) {
                respHandler.sendError(res, 400, 'FAILURE', 'Amount to invest is required!.', {});
            }  else if(newFarmShop && newFarmShop.amount_to_invest < 0) {
                respHandler.sendError(res, 400, 'FAILURE', 'Amount to invest is invalid!.', {});
            }  else if(newFarmShop && !newFarmShop.duration) {
                respHandler.sendError(res, 400, 'FAILURE', 'Investment duration is required!.', {});
            }  else if(newFarmShop && !newFarmShop.display_img) {
                respHandler.sendError(res, 400, 'FAILURE', 'Display Image is required!.', {});
            }  else if(newFarmShop && !newFarmShop.description) {
                respHandler.sendError(res, 400, 'FAILURE', 'Description is required!.', {});
            } else {
                FarmShop.create(newFarmShop, (error, farmShop) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm shop.', error);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Farm shop created successfully', farmShop);
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm shop.', err);
        }
    },
    searchFarmShop: async (req, res) => {
        try {
            const { farm_name } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await FarmShop.countDocuments({farm_name: new RegExp(`^${farm_name}.*`, "si")}).exec();
            FarmShop.find({farm_name: new RegExp(`^${farm_name}.*`, "si")})
                .populate('farm_type')
                .populate('farm_status')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, farmShop) => {
                if (err || !farmShop){
                    respHandler.sendError(res, 404, 'FAILURE', 'No farm shop matches that name!', err);
                } else if (validate.resultFound(farmShop, res)) {
                    // const data = validate.formatData(farmShop);
                    const data = {
                        data: validate.formatData(farmShop),
                        page: currentPage += 1,
                        limit: currentLimit,
                        total: totalCount
                    };
                    respHandler.sendSuccess(res, 200, 'Farm shop retrieved successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search farm shop by name.', err);
        }
    },
    filterFarmShop: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const farmQuery = getSearchQuery(req);
            farmQuery
                .populate('farm_status')
                .populate('farm_type')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .exec((err, farmShop) => {
                if (err || !farmShop){
                    respHandler.sendError(res, 404, 'FAILURE', 'No farm shop matches this filters!', err);
                } else if (validate.resultFound(farmShop, res)) {
                    // const data = validate.formatData(farmShop);
                    const data = {
                        data: validate.formatData(farmShop),
                        page: currentPage += 1,
                        limit: currentLimit
                    };
                    respHandler.sendSuccess(res, 200, 'Farm shop filtered successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter farm shop.', err);
        }
    },
    getFarmShopById: (req, res) => {
        try {
            FarmShop.findById(req.params.id)
                .populate('farm_type')
                .populate('farm_status')
                .exec((err, farmShop) => {
                if (err || !farmShop){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm shop by id.', err);
                } else if (validate.resultFound(farmShop, res)) {
                    const data = validate.formatData(farmShop);
                    respHandler.sendSuccess(res, 200, 'Farm shop fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm shop.', err);
        }
    },
    putFarmShop: (req, res) => {
        try {
            console.log('This is body ', req.body);
            FarmShop.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, farmShop) => {
                if (err || !farmShop){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update farm shop.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm shop updated successfully!', farmShop);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update farm shop');
        }
    },
    deleteFarmShop: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                FarmShop.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete farm shop.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Farm shop deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete farm shop by id');
        }
    },
};

module.exports = FarmShopController;

function getSearchQuery(req) {
    const { farm_type, farm_status } = req.body;
    const query = FarmShop.find({});
    const queryTerm = [ ];

    if (farm_type.length) {
        for(let i of farm_type){

            if(farm_status.length) {
                for(let j of farm_status){
                    // queryTerm.push({ farm_status: j });
                    queryTerm.push({ $and: [{farm_type: i}, { farm_status: j }] });

                }
            } else {
                queryTerm.push({ farm_type: i });
            }
        }
    } else if(farm_status.length && !farm_type.length) {
        for(let i of farm_status){
            queryTerm.push({ farm_status: i });
        }
    } else {

    }

         /*
    if (farm_status.length) {
        for(let i of farm_status){
            queryTerm.push({ farm_status: i });
        }
    }*/

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}