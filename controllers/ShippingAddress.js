const ShippingAddress = require('../models/ShippingAddress');
const Users = require('../models/Users');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const ShippingAddressController = {
    getShippingAddress: (req, res) => {
        try {
            ShippingAddress.find({})
                .populate('userId', 'first_name last_name email full_name _id')
                .exec((err, shippingAddresses) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list shipping addresses.', err);
                    } else if (validate.resultFound(shippingAddresses, res)) {
                        const data = validate.formatData(shippingAddresses);
                        respHandler.sendSuccess(res, 200, 'Shipping addresses listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list shipping addresses.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list shipping addresses.', err);
        }
    },
    createShippingAddress: (req, res) => {
        try {
            ShippingAddress.create(req.body, (error, shipping) => {
                if(error) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to create shipping address.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Shipping address created successfully!', shipping);
                }
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to create shipping address.', err);
        }
    },

    setDefaultAddress: (req, res) => {
        try {
            const { shippingAddressId } = req.body;
            ShippingAddress.updateMany({userId: req.params.userId}, {$set: {default: false}}, {multi: true}, () => {
                ShippingAddress.findByIdAndUpdate(shippingAddressId, {$set: {default: true}}, { new: true }, (err, shippingAddress) => {
                    if (err || !shippingAddress){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to set default shipping address.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Default shipping address updated successfully!', shippingAddress);
                });
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update shipping address');
        }
    },

    putShippingAddress: (req, res) => {
        try {
                ShippingAddress.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, shippingAddress) => {
                    if (err || !shippingAddress){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to update shipping address.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Shipping address updated successfully!', shippingAddress);
                });

        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update shipping address');
        }
    },

    getUserShippingAddresses: (req, res) => {
        try {
            ShippingAddress.find({userId: req.params.userId})
                .populate('userId', 'first_name last_name email full_name _id')
                .exec((err, shippingAddresses) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user shipping addresses.', err);
                    } else if (validate.resultFound(shippingAddresses, res)) {
                        const data = validate.formatData(shippingAddresses);
                        respHandler.sendSuccess(res, 200, 'Shipping addresses fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get shipping address.', err);
        }
    },
    getUserDefaultShippingAddresses: (req, res) => {
        try {
            ShippingAddress.findOne({$and: [{userId: req.params.userId}, {default: true}]})
                .exec((err, shippingAddresses) => {
                    if (err || !shippingAddresses){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user shipping addresses.', err);
                    } else if (validate.resultFound(shippingAddresses, res)) {
                        const data = validate.formatData(shippingAddresses);
                        respHandler.sendSuccess(res, 200, 'Shipping addresses fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get default shipping address.', err);
        }
    },
    getShippingAddressById: (req, res) => {
        try {
            ShippingAddress.findById(req.params.id)
                .populate('userId', 'first_name last_name email full_name _id')
                .exec((err, shippingAddress) => {
                    if (err || !shippingAddress){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get shipping address by id.', err);
                    } else if (validate.resultFound(shippingAddress, res)) {
                        const data = validate.formatData(shippingAddress);
                        respHandler.sendSuccess(res, 200, 'Shipping address fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get shipping address.', err);
        }
    },
    deleteShippingAddress: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                ShippingAddress.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete shipping address.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Shipping address deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete shipping address by id');
        }
    },
};

module.exports = ShippingAddressController;