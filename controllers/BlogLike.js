const BlogLike = require('../models/BlogLikes');
const Blog = require('../models/Blog');
const respHandler = require('../services/responseHandler');
const eachAsync = require('each-async');
const BlogLikeController = {
    likePost: (req, res) => {
        try {
            const {userId, blogId, actionType, userIdentity} = req.body;
            BlogLike.find({ $and: [{userId: userId}, {blogId: blogId}]}, (err, likes) => {
                if(likes && likes.length) {
                    eachAsync(likes, (like, i, done) => {
                        Blog.findById(blogId, (e, blog) => {
                            let total_likes = blog.total_likes || 0;
                            let total_dislikes = blog.total_dislikes || 0;
                            let update = {};
                            if(like.actionType === 'LIKE') {
                                total_likes -= 1;
                                update = {total_likes: total_likes < 0 ? '0' : total_likes};
                            }
                            if(like.actionType === 'DISLIKE') {
                                total_dislikes -= 1;
                                update = {total_dislikes: total_dislikes < 0 ? '0' : total_dislikes};
                            }
                            Blog.findByIdAndUpdate(blog, {$set: update}, {new: true}, () => {  done();   })
                        })
                    }, () => {
                        BlogLike.findOneAndDelete({$and: [{userId: userId}, {blogId: blogId}]}).exec();
                        proceedToFeeling(req, res);
                    });
                } else {
                    proceedToFeeling(req, res);
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to express feeling for this post!', err);
        }
    },
    checkFeelingOnPost: (req, res) => {
        const {userId, blogId} = req.body;
        try {
            BlogLike.findOne({ $and: [{userId: userId}, {blogId: blogId}]}, (err, like) => {
                if(like){
                    respHandler.sendSuccess(res, 200, 'Feelings status retrieved', {
                        liked: (like.actionType === 'LIKE' ),
                        disliked: (like.actionType === 'DISLIKE' )
                    });
                }
            });
        } catch(error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to check user feelings status', error);
        }

    }
};

module.exports = BlogLikeController;

function proceedToFeeling(req, res) {
    const {userId, blogId,actionType, userIdentity} = req.body;
    BlogLike.create({userId, blogId,actionType, userIdentity}, (error, feeling) => {
        if(error) {
            respHandler.sendError(res, 404, 'FAILURE', 'Feeling expressed successfully!', error);
        } else {
            proceedToUpdate(blogId, actionType);
            respHandler.sendSuccess(res, 200, 'Unable to save feelings for this post.', feeling);
        }
    })
}
function proceedToUpdate(blog, actionType) {
    Blog.findById(blog, (e, blog) => {
        let likes = blog.total_likes || 0;
        let dislikes = blog.total_dislikes || 0;
        let update = {};
        if(actionType === 'LIKE') {
            likes += 1;
            update = {total_likes: likes};
        }
        if(actionType === 'DISLIKE') {
            dislikes += 1;
            update = {total_dislikes: dislikes};
        }
        Blog.findByIdAndUpdate(blog, {$set: update}, {new: true}, () => {     })
    })
}