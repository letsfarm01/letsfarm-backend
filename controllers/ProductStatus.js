const ProductStatus = require('../models/ProductStatus');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const ProductStatusController = {
    getProductStatus: (req, res) => {
        try {
            ProductStatus.find({})
                .exec((err, productStatus) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product status.', err);
                    } else if (validate.resultFound(productStatus, res)) {
                        const data = validate.formatData(productStatus);
                        respHandler.sendSuccess(res, 200, 'Product status listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product status.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product status.', err);
        }
    },
    getActiveProductStatus: (req, res) => {
        try {
            ProductStatus.find({status: true})
                .exec((err, productStatus) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product status.', err);
                    } else if (validate.resultFound(productStatus, res)) {
                        const data = validate.formatData(productStatus);
                        respHandler.sendSuccess(res, 200, 'Product status listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product status.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product status.', err);
        }
    },
    createProductStatus: (req, res) => {
        try {
            ProductStatus.create(req.body, (error, productStatus) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create product status.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Product status created successfully', productStatus);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new product status.', err);
        }
    },
    getProductStatusById: (req, res) => {
        try {
            ProductStatus.findById(req.params.id).exec((err, productStatus) => {
                if (err || !productStatus){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get product status by id.', err);
                } else if (validate.resultFound(productStatus, res)) {
                    const data = validate.formatData(productStatus);
                    respHandler.sendSuccess(res, 200, 'Product status fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get product status.', err);
        }
    },
    putProductStatus: (req, res) => {
        try {
            console.log('This is body ', req.body);
            ProductStatus.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, productStatus) => {
                if (err || !productStatus){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update product status.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Product status updated successfully!', productStatus);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update product status');
        }
    },
    deleteProductStatus: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                ProductStatus.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete product status.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Product status deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete product status by id');
        }
    },
};

module.exports = ProductStatusController;