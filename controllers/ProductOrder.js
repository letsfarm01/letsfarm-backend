const Order = require('../models/ProductOrder');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const ProductOrderController = {
    getOrders: async (req, res) => {
        try {
            const { page, limit, status } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            let cond = {};
            if(status === 'ALL') {
                cond = {};
            } else {
                cond = {status};
            }
            const totalCount = await Order.countDocuments(cond).exec();
            Order.find(cond)
                .populate('payment_channel')
                .populate('userId', 'first_name last_name full_name email phone_number user_image_url address country city')
                .populate('productId')
                .populate('shipping_address')
                .exec((err, order) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product orders.', err);
                    } else if (validate.resultFound(order, res)) {
                        const data = {
                            data: validate.formatData(order),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Product Orders listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product orders.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product orders.', err);
        }
    },
    getUserOrders: (req, res) => {
        try {
            Order.find({userId: req.params.userId})
                .populate('payment_channel')
                .populate('productId')
                .populate('shipping_address')
                .exec((err, orders) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user\'s orders.', err);
                    } else if (validate.resultFound(orders, res)) {
                        const data = validate.formatData(orders);
                        respHandler.sendSuccess(res, 200, 'User\'s orders fetched successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list orders.', err);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get orders.', err);
        }
    },

    getOrder: (req, res) => {
        try {
            Order.findById(req.params.id)
                .populate('userId', 'first_name last_name full_name email phone_number user_image_url address country city')
                .populate('payment_channel')
                .populate('productId')
                .populate('shipping_address')
                .exec((err, order) => {
                    if (err || !order){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get order by id.', err);
                    } else if (validate.resultFound(order, res)) {
                        const data = validate.formatData(order);
                        respHandler.sendSuccess(res, 200, 'Order fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get order.', err);
        }
    },
    updateOrder: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Order.findByIdAndUpdate(req.params.id, {$set: {status: req.body.status}}, { new: true }, (err, order) => {
                if (err || !order){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update order status.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Order status updated successfully!', order);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update order status');
        }
    },
    getOrderByRef: (req, res) => {
        try {
            Order.findOne({orderReference: req.params.ref})
                .populate('userId', 'first_name last_name full_name email phone_number user_image_url address country city')
                .populate('productId')
                .populate('shipping_address')
                .exec((err, order) => {
                    if (err){
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get order by order reference.', err);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Order fetched successfully', order);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get orders.', err);
        }
    }
};

module.exports = ProductOrderController;
