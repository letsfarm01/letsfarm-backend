const EducationComment = require('../models/EducationComment');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const EducationCommentController = {
    getEducationComments: (req, res) => {
        try {
            EducationComment.find({educationId: req.params.educationId})
                .exec((err, educationComments) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education comments.', err);
                    } else if (validate.resultFound(educationComments, res)) {
                        const data = validate.formatData(educationComments);
                        respHandler.sendSuccess(res, 200, 'Education Comments listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education comments.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education comments.', err);
        }
    },
    createEducationComment: (req, res) => {
        try {
            EducationComment.create(req.body, (error, educationComment) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to save education comment.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Education Comment saved successfully', educationComment);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create save education comment.', err);
        }
    },
    getEducationCommentById: (req, res) => {
        try {
            EducationComment.findById(req.params.id).exec((err, educationComment) => {
                if (err || !educationComment){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education comment by id.', err);
                } else if (validate.resultFound(educationComment, res)) {
                    const data = validate.formatData(educationComment);
                    respHandler.sendSuccess(res, 200, 'Education comment fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education comment.', err);
        }
    },
    deleteEducationComment: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                EducationComment.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete education comment.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Education Comment deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete education comment by id');
        }
    },
};

module.exports = EducationCommentController;