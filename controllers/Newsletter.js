const Newsletter = require('../models/Newsletter');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const mailer = require('../mailing/mailSystem');

const NewsletterController = {
    getNewsletters: (req, res) => {
        try {
            Newsletter.find({}).exec((err, newsletters) => {
                if (err) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list newsletters.', err);
                }
                // console.log('Newsletters ', newsletters);
                if (validate.resultFound(newsletters, res)) {
                    const data = validate.formatData(newsletters);
                    respHandler.sendSuccess(res, 200, 'Newsletters listed successfully', data);
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list newsletters.', err);
        }
    },
    createNewsletter: (req, res) => {
        try {
            Newsletter.create({email: req.body.email}, (error, newsletter) => {
                if(error) {
                    if(error.name === 'ValidationError') {
                        respHandler.sendError(res, 401, 'FAILURE', 'Email already exist, thank you', error);
                    } else {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to add email to newsletter.', error);
                    }
                } else {
                    mailer.sendHtml({email: newsletter.email, full_name: `Letsfarm Subscriber`},
                        'Successfully subscribe to Letsfarm', 'NewsletterSubscriber',
                        {unsubscribeLink: `${process.env.FRONT_END_URL}unsubscribe-newsletter/${newsletter.email}`});
                    respHandler.sendSuccess(res, 200, 'You have successfully subscribe to Letsfarm\'s newsletter.', newsletter);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add email to newsletter.', err);
        }
    },
    getNewsletter: (req, res) => {
        try {
            Newsletter.findById(req.params.id).exec((err, newsletter) => {
                if (err || !newsletter){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get newsletter.', err);
                }
                if(validate.resultFound(newsletter, res)) {
                    const data = validate.formatData(newsletter);
                    respHandler.sendSuccess(res, 200, 'Newsletter fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get newsletter.', err);
        }
    },
    putNewsletter: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const _newsletter = req.body;
            if (_newsletter.password) {
                _newsletter.password = null;
                delete _newsletter.password
            }
            Newsletter.findByIdAndUpdate(req.params.id, {$set: _newsletter}, { new: true }, (err, newsletter) => {
                if (err || !newsletter){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update newsletter.', err);
                }
                if(newsletter) {
                    newsletter.password = null;
                    respHandler.sendSuccess(res, 200, 'Newsletter updated successfully!', newsletter);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update newsletter');
        }
    },
   deleteNewsletter: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Newsletter.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete newsletter.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Newsletter deleted successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete newsletter');
        }
    },
};

module.exports = NewsletterController;