const FarmProduct = require('../models/FarmProduct');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const FarmProductController = {
    getFarmProducts: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await FarmProduct.countDocuments({}).exec();
            FarmProduct.find({})
                .populate('product_type')
                .populate('product_status')
                .exec((err, farmProduct) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
                    } else if (validate.resultFound(farmProduct, res)) {
                        // const data = validate.formatData(farmProduct);
                        const sorter = farmProduct.sort(function(a, b){
                            const textA = a.product_status.name.toLowerCase();
                            const textB = b.product_status.name.toLowerCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });
                        // console.log('AAAAAAAA ', sorter, farmProduct);
                        const data = {
                            data: validate.formatData(sorter),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Farm products listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
        }
    },
    getDistinctFarmProducts: async (req, res) => {
        try {
            FarmProduct.find({})
                .populate('product_type')
                .populate('product_status')
                .exec((err, farmProduct) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
                    } else if (validate.resultFound(farmProduct, res)) {
                        // const data = validate.formatData(farmProduct);
                        const sorter = farmProduct.sort(function(a, b){
                            const textA = a.product_status.name.toLowerCase();
                            const textB = b.product_status.name.toLowerCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });
                        const data = {
                            data: sorter
                        };
                        respHandler.sendSuccess(res, 200, 'Farm products listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm products.', err);
        }
    },
    createFarmProduct: (req, res) => {
        try {
            const newFarmProduct = req.body;
            if(newFarmProduct && !newFarmProduct.product_name) {
                respHandler.sendError(res, 400, 'FAILURE', 'Product name is required!.', {});
            } else if(newFarmProduct && !newFarmProduct.price) {
                respHandler.sendError(res, 400, 'FAILURE', 'product price is required!.', {});
            } else if(newFarmProduct && newFarmProduct.price < 0) {
                respHandler.sendError(res, 400, 'FAILURE', 'Product price is invalid!.', {});
            } else if(newFarmProduct && !newFarmProduct.display_img) {
                respHandler.sendError(res, 400, 'FAILURE', 'Display Image is required!.', {});
            } else {
                FarmProduct.create(newFarmProduct, (error, farmProduct) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm product.', error);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Farm product created successfully', farmProduct);
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm product.', err);
        }
    },
    searchFarmProduct: async (req, res) => {
        try {
            const { product_name } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await FarmProduct.countDocuments({product_name: new RegExp(`^${product_name}.*`, "si")}).exec();
            FarmProduct.find({product_name: new RegExp(`^${product_name}.*`, "si")})
                .populate('product_type')
                .populate('product_status')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, farmProduct) => {
                    if (err || !farmProduct){
                        respHandler.sendError(res, 404, 'FAILURE', 'No farm product matches that name!', err);
                    } else if (validate.resultFound(farmProduct, res)) {
                        // const data = validate.formatData(farmProduct);
                        const data = {
                            data: validate.formatData(farmProduct),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Farm product retrieved successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search farm product by name.', err);
        }
    },
    filterFarmProduct: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const farmQuery = getSearchQuery(req);
            farmQuery
                .populate('product_status')
                .populate('product_type')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .exec((err, farmProduct) => {
                    if (err || !farmProduct){
                        respHandler.sendError(res, 404, 'FAILURE', 'No farm product matches this filters!', err);
                    } else if (validate.resultFound(farmProduct, res)) {
                        // const data = validate.formatData(farmProduct);
                        const data = {
                            data: validate.formatData(farmProduct),
                            page: currentPage += 1,
                            limit: currentLimit
                        };
                        respHandler.sendSuccess(res, 200, 'Farm product filtered successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter farm product.', err);
        }
    },
    getFarmProductById: (req, res) => {
        try {
            FarmProduct.findById(req.params.id)
                .populate('product_type')
                .populate('product_status')
                .exec((err, farmProduct) => {
                    if (err || !farmProduct){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm product by id.', err);
                    } else if (validate.resultFound(farmProduct, res)) {
                        const data = validate.formatData(farmProduct);
                        respHandler.sendSuccess(res, 200, 'Farm product fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm product.', err);
        }
    },
    putFarmProduct: (req, res) => {
        try {
            console.log('This is body ', req.body);
            FarmProduct.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, farmProduct) => {
                if (err || !farmProduct){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update farm product.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm product updated successfully!', farmProduct);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update farm product');
        }
    },
    deleteFarmProduct: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                FarmProduct.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete farm product.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Farm product deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete farm product by id');
        }
    },
};

module.exports = FarmProductController;

function getSearchQuery(req) {
    const { product_type, product_status } = req.body;
    const query = FarmProduct.find({});
    const queryTerm = [ ];

    if (product_type.length) {
        for(let i of product_type){
            if(product_status.length) {
                for(let j of product_status){
                    // queryTerm.push({ product_status: j });
                    queryTerm.push({ $and: [{product_type: i}, { product_status: j }] });

                }
            } else {
                queryTerm.push({ product_type: i });
            }
        }
    } else if(product_status.length && !product_type.length) {
        for(let i of product_status){
            queryTerm.push({ product_status: i });
        }
    } else {

    }

    /*
if (product_status.length) {
   for(let i of product_status){
       queryTerm.push({ product_status: i });
   }
}*/

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}