const FarmStatus = require('../models/FarmStatus');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const FarmStatusController = {
    getFarmStatus: (req, res) => {
        try {
            FarmStatus.find({})
                .exec((err, farmStatus) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm status.', err);
                    } else if (validate.resultFound(farmStatus, res)) {
                        const data = validate.formatData(farmStatus);
                        respHandler.sendSuccess(res, 200, 'Farm status listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm status.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm status.', err);
        }
    },
    getActiveFarmStatus: (req, res) => {
        try {
            FarmStatus.find({status: true})
                .exec((err, farmStatus) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm status.', err);
                    } else if (validate.resultFound(farmStatus, res)) {
                        const data = validate.formatData(farmStatus);
                        respHandler.sendSuccess(res, 200, 'Farm status listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm status.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm status.', err);
        }
    },
    createFarmStatus: (req, res) => {
        try {
            FarmStatus.create(req.body, (error, farmStatus) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm status.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm status created successfully', farmStatus);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new farm status.', err);
        }
    },
    getFarmStatusById: (req, res) => {
        try {
            FarmStatus.findById(req.params.id).exec((err, farmStatus) => {
                if (err || !farmStatus){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm status by id.', err);
                } else if (validate.resultFound(farmStatus, res)) {
                    const data = validate.formatData(farmStatus);
                    respHandler.sendSuccess(res, 200, 'Farm status fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm status.', err);
        }
    },
    putFarmStatus: (req, res) => {
        try {
            console.log('This is body ', req.body);
            FarmStatus.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, farmStatus) => {
                if (err || !farmStatus){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update farm status.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm status updated successfully!', farmStatus);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update farm status');
        }
    },
    deleteFarmStatus: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                FarmStatus.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete farm status.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Farm status deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete farm status by id');
        }
    },
};

module.exports = FarmStatusController;