const EducationLike = require('../models/EducationLikes');
const Education = require('../models/Education');
const respHandler = require('../services/responseHandler');
const eachAsync = require('each-async');
const EducationLikeController = {
    likePost: (req, res) => {
        try {
            const {userId, educationId, actionType, userIdentity} = req.body;
            EducationLike.find({ $and: [{userId: userId}, {educationId: educationId}]}, (err, likes) => {
                if(likes && likes.length) {
                    eachAsync(likes, (like, i, done) => {
                        Education.findById(educationId, (e, education) => {
                            let total_likes = education.total_likes || 0;
                            let total_dislikes = education.total_dislikes || 0;
                            let update = {};
                            if(like.actionType === 'LIKE') {
                                total_likes -= 1;
                                update = {total_likes: total_likes < 0 ? '0' : total_likes};
                            }
                            if(like.actionType === 'DISLIKE') {
                                total_dislikes -= 1;
                                update = {total_dislikes: total_dislikes < 0 ? '0' : total_dislikes};
                            }
                            Education.findByIdAndUpdate(education, {$set: update}, {new: true}, () => {  done();   })
                        })
                    }, () => {
                        EducationLike.findOneAndDelete({$and: [{userId: userId}, {educationId: educationId}]}).exec();
                        proceedToFeeling(req, res);
                    });
                } else {
                    proceedToFeeling(req, res);
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to express feeling for this post!', err);
        }
    },
    checkFeelingOnPost: (req, res) => {
        const {userId, educationId} = req.body;
        try {
            EducationLike.findOne({ $and: [{userId: userId}, {educationId: educationId}]}, (err, like) => {
                if(like){
                    respHandler.sendSuccess(res, 200, 'Feelings status retrieved', {
                        liked: (like.actionType === 'LIKE' ),
                        disliked: (like.actionType === 'DISLIKE' )
                    });
                }
            });
            } catch(error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to check user feelings status', error);
        }

    }
};

module.exports = EducationLikeController;

function proceedToFeeling(req, res) {
    const {userId, educationId,actionType, userIdentity} = req.body;
    EducationLike.create({userId, educationId,actionType, userIdentity}, (error, feeling) => {
        if(error) {
            respHandler.sendError(res, 404, 'FAILURE', 'Feeling expressed successfully!', error);
        } else {
            proceedToUpdate(educationId, actionType);
            respHandler.sendSuccess(res, 200, 'Unable to save feelings for this post.', feeling);
        }
    })
}
function proceedToUpdate(education, actionType) {
 Education.findById(education, (e, education) => {
     let likes = education.total_likes || 0;
     let dislikes = education.total_dislikes || 0;
     let update = {};
     if(actionType === 'LIKE') {
         likes += 1;
         update = {total_likes: likes};
     }
     if(actionType === 'DISLIKE') {
         dislikes += 1;
         update = {total_dislikes: dislikes};
     }
     Education.findByIdAndUpdate(education, {$set: update}, {new: true}, () => {     })
 })
}