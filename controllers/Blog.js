const Blog = require('../models/Blog');
const Comment = require('../models/BlogComment');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const pushNotify = require('../services/pushnotificationService');
const eachAsync = require('each-async');
const BlogController = {
    getBlogs: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9100;
            const totalCount = await Blog.countDocuments({}).exec();
            Blog.find({})
                .populate('blog_category')
                .exec((err, blogs) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
                    } else if (validate.resultFound(blogs, res)) {
                        eachAsync(blogs, (blog, i, done) => {
                            blog.comments_count = 0;
                            Comment.countDocuments({blogId: blog._id}, (e, count) => {
                                blog.comments_count = count;
                                done();
                            })
                        }, () => {
                            const data = {
                                data: validate.formatData(blogs),
                                page: currentPage += 1,
                                limit: currentLimit,
                                total: totalCount
                            };
                            respHandler.sendSuccess(res, 200, 'Blogs listed successfully', data);
                        });


                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
        }
    },
    getNormalBlogs: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await Blog.countDocuments({}).exec();
            Blog.find({$and: [{top_post: false}, {publish: true}]})
                .populate('blog_category')
                .exec((err, blogs) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
                    } else if (validate.resultFound(blogs, res)) {
                        eachAsync(blogs, (blog, i, done) => {
                            blog.comments_count = 0;
                            Comment.countDocuments({blogId: blog._id}, (e, count) => {
                                blog.comments_count = count;
                                done();
                            })
                        }, () => {
                            const data = {
                                data: validate.formatData(blogs),
                                page: currentPage += 1,
                                limit: currentLimit,
                                total: totalCount
                            };
                            respHandler.sendSuccess(res, 200, 'Blogs listed successfully', data);
                        });


                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blogs.', err);
        }
    },
    createBlog: (req, res) => {
        try {
            const newBlog = req.body;
            if(newBlog && !newBlog.title) {
                respHandler.sendError(res, 400, 'FAILURE', 'Title field is required!.', {});
            } else if(newBlog && !newBlog.message_body) {
                respHandler.sendError(res, 400, 'FAILURE', 'Message body is required!.', {});
            } else if(newBlog && !newBlog.blog_category) {
                respHandler.sendError(res, 400, 'FAILURE', 'Category field is required!.', {});
            } else {
                Blog.create(newBlog, (error, blog) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create blog.', error);
                    } else {
                        pushNotify.send('New Blog Post', blog.title);
                        respHandler.sendSuccess(res, 200, 'Blog created successfully', blog);
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create blog.', err);
        }
    },
    searchBlog: async (req, res) => {
        try {
            const { title } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await Blog.countDocuments({$and: [{title: new RegExp(`^${title}.*`, "si")}, {publish: true}]}).exec();
            Blog.find({$and: [{title: new RegExp(`^${title}.*`, "si")}, {publish: true}]})
                .populate('blog_category')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, blog) => {
                    if (err || !blog.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No blog matches that title!', err);
                    } else if (validate.resultFound(blog, res)) {
                        // const data = validate.formatData(blog);
                        const data = {
                            data: validate.formatData(blog),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Blog retrieved successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search blog by name.', err);
        }
    },
    filterBlog: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const blogQuery = getSearchQuery(req);
            blogQuery
                .populate('blog_category')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .exec((err, blog) => {
                    if (err || !blog.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No blogs matches this filters!', err);
                    } else if (validate.resultFound(blog, res)) {
                        const data = {
                            data: validate.formatData(blog),
                            page: currentPage += 1,
                            limit: currentLimit
                        };
                        respHandler.sendSuccess(res, 200, 'Blog filtered successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter blog.', err);
        }
    },
    getBlogById: (req, res) => {
        try {
            Blog.findById(req.params.id)
                .populate('blog_category')
                .exec((err, blog) => {
                    if (err || !blog){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog by id.', err);
                    } else if (validate.resultFound(blog, res)) {
                        Comment.countDocuments({blogId: blog._id}, (e, count) => {
                            blog.comments_count = count;
                            const data = validate.formatData(blog);
                            respHandler.sendSuccess(res, 200, 'Blog fetched successfully', data);
                        });
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog.', err);
        }
    },
    getTopPost: (req, res) => {
        try {
            Blog.findOne({$and: [{top_post: true}, {publish: true}]})
                .populate('blog_category')
                .sort({natural: -1})
                .exec((err, blog) => {
                    if (err || !blog){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get top blog post', err);
                    } else if (validate.resultFound(blog, res)) {
                        Comment.countDocuments({blogId: blog._id}, (e, count) => {
                            blog.comments_count = count;
                            const data = validate.formatData(blog);
                            respHandler.sendSuccess(res, 200, 'Blog fetched successfully', data);
                        });
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog.', err);
        }
    },
    makeTopPost: (req, res) => {
        try {
            Blog.updateMany({}, {$set: {top_post: false}}, {multi: true}, (err, blog) => {
                if (err){
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to set top blog post', err);
                } else {
                   Blog.findByIdAndUpdate(req.body.id, {$set: {top_post: true}}, {new: true}, (e, top) => {
                       if(e) {
                           respHandler.sendError(res, 400, 'FAILURE', 'Unable to set top blog post', err);
                       } else {
                           respHandler.sendSuccess(res, 200, 'Top blog set successfully', top);
                       }
                   });
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog.', err);
        }
    },
    putBlog: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Blog.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, blog) => {
                if (err || !blog){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update blog.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Blog updated successfully!', blog);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update blog');
        }
    },
    deleteBlog: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Blog.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete blog.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Blog deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete blog by id');
        }
    },
};

module.exports = BlogController;

function getSearchQuery(req) {
    const { blog_category } = req.body;
    const query = Blog.find({publish: true});
    const queryTerm = [ ];

    if (blog_category.length) {
        for(let i of blog_category){
            queryTerm.push({ blog_category: i });
        }
    }

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}