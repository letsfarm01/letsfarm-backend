const CompletedSponsor = require('../models/CompletedSponsor');
const User = require('../models/Users');
const Sponsor = require('../models/Sponsorship');
const Archive = require('../models/ArchivedSponsor');
const Wallet = require('../models/Wallet');
const WalletTransaction = require('../models/WalletTransaction');
const Transaction = require('../models/Transaction');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const jsEncryptService = require('../services/jsEncryptService');
const winston = require('../config/winston');
const moment = require('moment');
const mailer = require('../mailing/mailSystem');
const nanoid = require('nanoid');

const CompletedSponsorController = {
    getCompletedSponsors: (req, res) => {
        try {
            const { page, limit, status } = req.query;
            const {month, year} = req.body;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 900000;
            let cond = {};
            if(status && status !== 'ALL') {
                cond = {status}
            }
            CompletedSponsor.find(cond)
                .populate({path: 'sponsorId', populate: {path: 'farmId', select: 'farm_name'}})
                .populate('archiveId')
                .populate('userId', 'first_name last_name email full_name phone_number')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .sort('roi_date')
                .exec((err, sponsorships) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list completed sponsorships.', err);
                    } else if (validate.resultFound(sponsorships, res)) {
                        let data;
                        if(req.body && req.body.month) {
                            const matchedFilters = sponsorships.filter(sponsor => {
                                const date = moment(sponsor.expected_end_date);
                                const _month = date.month();
                                // console.log('Year ', date.year(), year, _month, month-1);
                                return ((_month === (month - 1)) && parseInt(year) === date.year());
                            });
                            data = validate.formatData(matchedFilters);
                        } else {
                            data = validate.formatData(sponsorships);
                        }
                        respHandler.sendSuccess(res, 200, 'Sponsorship listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list  completed sponsorships.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list completed sponsorships.', err);
        }
    },
    getCompletedSponsor: (req, res) => {
        try {
            CompletedSponsor.findById(req.params.id)
                .populate('sponsorId')
                .populate('archiveId')
                .populate('userId', 'first_name last_name email full_name phone_number')
                .exec((err, sponsorship) => {
                    if (err || !sponsorship){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get completed sponsorship by id.', err);
                    } else if (validate.resultFound(sponsorship, res)) {
                        const data = validate.formatData(sponsorship);
                        respHandler.sendSuccess(res, 200, 'Completed Sponsor fetched successfully', data);
                    } else {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get completed sponsorship.', err);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get completed sponsorship.', err);
        }
    },
    putCompletedSponsor: (req, res) => {
        try {
            const status = req.body.status;
            CompletedSponsor.findByIdAndUpdate(req.params.id, {$set: {status: status}}, { new: true }, (err, sponsorship) => {
                if (err || !sponsorship){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update completed sponsorship status.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Completed Sponsor updated successfully!', sponsorship);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update completed sponsorship');
        }
    },
    deleteCompletedSponsor: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                CompletedSponsor.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete sponsorship.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Completed Sponsor deleted successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete completed sponsorship by id');
        }
    },
    sendOneToWallet: (req, res, next) => {
       try {
           const decryptedData =  jsEncryptService.decryptData(req.body.token) || null;
           if(decryptedData && decryptedData.valid && decryptedData.sponsor) {
               CompletedSponsor.findOne({$and: [{_id: decryptedData.sponsor}, {status: 'PENDING'}]}, (error, sponsor) => {
                   if(error || !sponsor) {
                       respHandler.sendError(res, 401, 'FAILURE', 'Unable to carry out this action, invalid request');
                   } else {
                       User.findOne({_id: sponsor.userId}).exec((userErr, user) => {
                           if(!user || userErr) {
                               respHandler.sendError(res, 400, 'FAILURE', 'Unable to locate sponsor user!');
                           } else {
                               CompletedSponsorController.fundUserWallet(req, res, user, sponsor);
                           }
                       });
                   }
               });
           } else {
               respHandler.sendError(res, 401, 'FAILURE', 'Unable to carry out this action');
           }
       } catch (error) {
           respHandler.sendError(res, 400, 'FAILURE', 'Unable to carry out this action');
       }
    },
    fundUserWallet: (req, res, user, sponsor) => {
        if(!user || !sponsor) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to detect user and(or) sponsor details');
        } else {
            Wallet.findOne({userId: user._id}, (error, wallet) => {
                if(error || !wallet) {
                    return respHandler.sendError(res, 401, 'FAILURE', 'This user does not have a wallet!', error);
                } else {
                    const newBalance = parseFloat(wallet.wallet_balance) + parseFloat(sponsor.roi_amount);
                    console.log('BALANCE ', newBalance, parseFloat(wallet.wallet_balance), parseFloat(sponsor.roi_amount));
                    Wallet.findOneAndUpdate({userId: user._id}, {$set: {wallet_balance: newBalance, last_fund_date: moment.now()}},
                        {new: true}, (e, walletSuc) => {
                            if(e) {
                                CompletedSponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'UNABLE_TO_UPDATE_WALLET'}}, {new: true}, (err, completedSponsor) => {});
                                winston.logger.log('error', `Error while updating wallet balance - ${JSON.stringify(e)} ---Wallet transaction`);
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet balance', e);
                            } else {

                                Transaction.create({orderReference: `Wallet-Funding-${controllerService.getRandomInt(999999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`,
                                    total_price: sponsor.roi_amount,
                                    payment_channel: process.env.NODE_ENV === 'production' ? '5edbfc6cd04e430017f89318' : '5edc0b6bc9567500173af058',
                                    userId: user._id,
                                    narration: 'Funding of wallet for sponsor - ' + sponsor._id
                                } , (err, transaction) => {
                                    if(err || !transaction) {
                                        winston.logger.log('error', `unable to create transaction - ${JSON.stringify(e)} ---Wallet transaction`);
                                    } else {
                                        WalletTransaction.create({
                                            narration: 'Funding of wallet from sponsor - ' + sponsor._id,
                                            type: 'CREDIT',
                                            new_wallet_balance: newBalance,
                                            old_wallet_balance: wallet.wallet_balance,
                                            amount: sponsor.roi_amount,
                                            transactionId: transaction._id,
                                            userId: user._id
                                        }, (err_, walletTrans) => {
                                            if(err_) {
                                                console.log('Error ', err_);
                                                CompletedSponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'UNABLE_TO_CREATE_WALLET_TRANSACTION'}}, {new: true}, (err, completedSponsor) => {});
                                                winston.logger.log('error', `Error: unable to update wallet transaction table - ${JSON.stringify(err_)} ---Wallet transaction table`);
                                                return respHandler.sendError(res, 400, 'FAILURE', 'Unable to save wallet transaction', err_);
                                            } else {
                                                CompletedSponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'PAID'}}, {new: true}, (err, completedSponsor) => {
                                                    mailer.sendHtml({email: user.email, full_name: user.full_name},
                                                        'Wallet Credit Alert', 'WalletAlert', {
                                                            full_name: user.full_name, title: 'Wallet Credit Alert',amount: 'NGN ' + sponsor.roi_amount });
                                                    respHandler.sendSuccess(res, 200, user.full_name + ' wallet successfully funded!', {});
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                }
            });
        }
    }
};


module.exports = CompletedSponsorController;