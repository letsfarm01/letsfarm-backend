const BlogComment = require('../models/BlogComment');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const BlogCommentController = {
    getBlogComments: (req, res) => {
        try {
            BlogComment.find({})
                .exec((err, blogComments) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog comments.', err);
                    } else if (validate.resultFound(blogComments, res)) {
                        const data = validate.formatData(blogComments);
                        respHandler.sendSuccess(res, 200, 'Blog Comments listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog comments.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog comments.', err);
        }
    },
    createBlogComment: (req, res) => {
        try {
            BlogComment.create(req.body, (error, blogComment) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to save blog comment.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Blog Comment saved successfully', blogComment);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create save blog comment.', err);
        }
    },
    getBlogCommentById: (req, res) => {
        try {
            BlogComment.findById(req.params.id).exec((err, blogComment) => {
                if (err || !blogComment){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog comment by id.', err);
                } else if (validate.resultFound(blogComment, res)) {
                    const data = validate.formatData(blogComment);
                    respHandler.sendSuccess(res, 200, 'Blog comment fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog comment.', err);
        }
    },
    deleteBlogComment: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                BlogComment.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete blog comment.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Blog Comment deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete blog comment by id');
        }
    },
};

module.exports = BlogCommentController;