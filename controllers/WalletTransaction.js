const WalletTransaction = require('../models/WalletTransaction');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const WalletTransactionController = {
    getWalletTransactions: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await WalletTransaction.countDocuments({}).exec();
            WalletTransaction.find({})
                .populate('transactionId')
                .populate('userId')
                .sort('-createdAt')
                .exec((err, transaction) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallet transactions.', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Wallet transactions listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallet transactions.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallet transactions.', err);
        }
    },
    getUserWalletTransactions: (req, res) => {
        try {
            WalletTransaction.find({userId: req.params.userId})
                .populate('transactionId')
                .sort('-createdAt')
                .exec((err, transactions) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user\'s wallet transactions.', err);
                    } else if (validate.resultFound(transactions, res)) {
                        const data = validate.formatData(transactions);
                        respHandler.sendSuccess(res, 200, 'User\'s wallet transactions fetched successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallet transactions.', err);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get wallet transactions.', err);
        }
    },

    searchWalletTransaction: async (req, res) => {
        try {
            const { narration } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await WalletTransaction.countDocuments({narration: new RegExp(`^${narration}.*`, "i")}).exec();

            WalletTransaction.find({narration: new RegExp(`^${narration}.*`, "i")})
                .populate('userId')
                .populate('transactionId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, transaction) => {
                    if (err || !transaction.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No wallet transaction matches that narration!', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Wallet transactions retrieved successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search wallet transaction.', err);
        }
    },
    filterWalletTransaction: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const transactionQuery = getSearchQuery(req);
            transactionQuery
                .populate('transactionId')
                .populate('userId')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .exec((err, transaction) => {
                    if (err || !transaction.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No wallet transactions matches this criteria!', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit
                        };
                        respHandler.sendSuccess(res, 200, 'Wallet transaction filtered successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter wallet transaction.', err);
        }
    },
    getWalletTransaction: (req, res) => {
        try {
            WalletTransaction.findById(req.params.id)
                .populate('userId')
                .populate('transactionId')
                .exec((err, transaction) => {
                    if (err || !transaction){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get wallet transaction by id.', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = validate.formatData(transaction);
                        respHandler.sendSuccess(res, 200, 'Wallet transaction fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get wallet transaction.', err);
        }
    },
    putWalletTransaction: (req, res) => {
        try {
            const {narration} = req.body;
            WalletTransaction.findByIdAndUpdate(req.params.id, {$set:
                {narration}}, { new: true }, (err, transaction) => {
                if (err || !transaction){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update wallet transaction.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Wallet transaction updated successfully!', transaction);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet transaction');
        }
    },
    deleteWalletTransaction: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                WalletTransaction.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete wallet transaction.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Wallet transaction soft deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete wallet transaction by id');
        }
    }
};

module.exports = WalletTransactionController;

function getSearchQuery(req) {
    const { narration, type } = req.body;
    const query = WalletTransaction.find({});
    const queryTerm = [ ];

    if (narration) {
        queryTerm.push({ narration: new RegExp(`^${narration}.*`, "i") });
    }
    if (type) {
        queryTerm.push({ type: type });
    }

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}