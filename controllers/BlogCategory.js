const BlogCategory = require('../models/BlogCategory');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const BlogCategoryController = {
    getBlogCategories: (req, res) => {
        try {
            BlogCategory.find({})
                .exec((err, blogCategory) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog category.', err);
                    } else if (validate.resultFound(blogCategory, res)) {
                        const data = validate.formatData(blogCategory);
                        respHandler.sendSuccess(res, 200, 'Blog Category listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog category.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list blog categories.', err);
        }
    },
    createBlogCategory: (req, res) => {
        try {
            BlogCategory.create(req.body, (error, blogCategory) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create blog category.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Blog Category created successfully', blogCategory);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new blog category.', err);
        }
    },
    getBlogCategoryById: (req, res) => {
        try {
            BlogCategory.findById(req.params.id).exec((err, blogCategory) => {
                if (err || !blogCategory){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog category by id.', err);
                } else if (validate.resultFound(blogCategory, res)) {
                    const data = validate.formatData(blogCategory);
                    respHandler.sendSuccess(res, 200, 'Blog category fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get blog category.', err);
        }
    },
    putBlogCategory: (req, res) => {
        try {
            console.log('This is body ', req.body);
            BlogCategory.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, blogCategory) => {
                if (err || !blogCategory){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update blog category.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Blog Category updated successfully!', blogCategory);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update blog category');
        }
    },
    deleteBlogCategory: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                BlogCategory.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete blog category.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Blog Category deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete blog category by id');
        }
    },
};

module.exports = BlogCategoryController;