const Education = require('../models/Education');
const Comment = require('../models/EducationComment');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const pushNotify = require('../services/pushnotificationService');
const eachAsync = require('each-async');
const EducationController = {
    getEducations: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await Education.countDocuments({}).exec();
            Education.find({})
                .populate('edu_category')
                .exec((err, education) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
                    } else if (validate.resultFound(education, res)) {
                        // const data = validate.formatData(education);

                        eachAsync(education, (edu, i, done) => {
                            edu.comments_count = 0;
                            Comment.countDocuments({educationId: edu._id}, (e, count) => {
                                edu.comments_count = count;
                                done();
                            })
                        }, () => {
                            const data = {
                                data: validate.formatData(education),
                                page: currentPage += 1,
                                limit: currentLimit,
                                total: totalCount
                            };
                            respHandler.sendSuccess(res, 200, 'Educations listed successfully', data);
                        });
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
        }
    },
    getPublishedEducations: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await Education.countDocuments({publish: true}).exec();
            Education.find({publish: true})
                .populate('edu_category')
                .exec((err, education) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
                    } else if (validate.resultFound(education, res)) {
                        // const data = validate.formatData(education);

                        eachAsync(education, (edu, i, done) => {
                            edu.comments_count = 0;
                            Comment.countDocuments({educationId: edu._id}, (e, count) => {
                                edu.comments_count = count;
                                done();
                            })
                        }, () => {
                            const data = {
                                data: validate.formatData(education),
                                page: currentPage += 1,
                                limit: currentLimit,
                                total: totalCount
                            };
                            respHandler.sendSuccess(res, 200, 'Educations listed successfully', data);
                        });
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list educations.', err);
        }
    },
    createEducation: (req, res) => {
        try {
            const newEducation = req.body;
            if(newEducation && !newEducation.title) {
                respHandler.sendError(res, 400, 'FAILURE', 'Title field is required!.', {});
            } else if(newEducation && !newEducation.message_body) {
                respHandler.sendError(res, 400, 'FAILURE', 'Message body is required!.', {});
            } else if(newEducation && !newEducation.edu_category) {
                respHandler.sendError(res, 400, 'FAILURE', 'Category field is required!.', {});
            } else {
                Education.create(newEducation, (error, education) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create education.', error);
                    } else {
                        pushNotify.send('New Educational Post', education.title);
                        respHandler.sendSuccess(res, 200, 'Education created successfully', education);
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create education.', err);
        }
    },
    searchEducation: async (req, res) => {
        try {
            const { title } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            const totalCount = await Education.countDocuments({$and: [{title: new RegExp(`^${title}.*`, "si")}, {publish: true}]}).exec();
            Education.find({$and: [{title: new RegExp(`^${title}.*`, "si")}, {publish: true}]})
                .populate('edu_category')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, education) => {
                    if (err || !education.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No education matches that title!', err);
                    } else if (validate.resultFound(education, res)) {
                        // const data = validate.formatData(education);
                        const data = {
                            data: validate.formatData(education),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Education retrieved successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search education by name.', err);
        }
    },
    filterEducation: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 100;
            // const eduQuery = getSearchQuery(req);
            Education.find({$and: [{edu_category: req.body.edu_category}, {publish: true}]})
                .populate('edu_category')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .exec((err, education) => {
                    if (err || !education.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No educational post matches this category!', err);
                    } else if (validate.resultFound(education, res)) {
                        const data = {
                            data: validate.formatData(education),
                            page: currentPage += 1,
                            limit: currentLimit
                        };
                        respHandler.sendSuccess(res, 200, 'Education filtered successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter education.', err);
        }
    },
    getEducationById: (req, res) => {
        try {
            Education.findById(req.params.id)
                .populate('edu_category')
                .exec((err, education) => {
                    if (err || !education){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education by id.', err);
                    } else if (validate.resultFound(education, res)) {
                        const data = validate.formatData(education);
                        respHandler.sendSuccess(res, 200, 'Education fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education.', err);
        }
    },
    putEducation: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Education.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, education) => {
                if (err || !education){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update education.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Education updated successfully!', education);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update education');
        }
    },
    deleteEducation: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Education.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete education.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Education deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete education by id');
        }
    },
};

module.exports = EducationController;

function getSearchQuery(req) {
    const { edu_category } = req.body;
    const query = Education.find({});
    const queryTerm = [ ];

    if (edu_category.length) {
        for(let i of edu_category){
            queryTerm.push({ edu_category: i });
        }
    }

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}