const AccountManager = require('../models/AccountManager');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const mailer = require('../mailing/mailSystem');

const AccountManagerController = {
    getAccountManagers: (req, res) => {
        try {
            AccountManager.find({resolved: {$ne: true}})
                .populate('userId', 'email first_name last_name full_name')
                .exec((err, accountManagers) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list messages.', err);
                    } else if (validate.resultFound(accountManagers, res)) {
                        const data = validate.formatData(accountManagers);
                        respHandler.sendSuccess(res, 200, 'Messages listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list messages.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list messages.', err);
        }
    },
    resolveMessage: (req, res) => {
        try {
            AccountManager.findByIdAndUpdate(req.params.id, {$set: {resolved: true}}, {new: true}, (error, account) => {
                if(error) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to resolve message.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Message resolved successfully', account);
                }
            })
        } catch(err) {
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to resolve message.', err);
        }
    },
    createMessage: (req, res) => {
        try {
            AccountManager.create(req.body, (error, accountManager) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to send message.', error);
                } else {
                    AccountManager.findById(accountManager._id).populate('userId').exec((e, acctManager) => {
                       if(acctManager) {
                           mailer.sendHtml({email: process.env.MAILJET_ADMIN_EMAIL, full_name: `Letsfarm Admin`},
                               `Message for Account Manager`, 'AccountManager',
                               {user_name: `${acctManager.userId.full_name}`, message: acctManager.message, topic: acctManager.topic}, acctManager.userId.email);
                       }
                        respHandler.sendSuccess(res, 200, 'Message sent successfully', accountManager);
                    });
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to send message.', err);
        }
    }
};

module.exports = AccountManagerController;