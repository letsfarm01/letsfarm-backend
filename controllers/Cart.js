const Cart = require('../models/Cart');
const FarmShop = require('../models/FarmShop');
const FarmProduct = require('../models/FarmProduct');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');

const CartController = {
    getCarts: (req, res) => {
        try {
            // const { limit } = req.query;
            Cart.find({})
                .populate('userId', 'first_name last_name email full_name phone_number')
                .populate({
                    path: "farmId",
                    populate: { path: "farm_status" }
                })
                .exec((err, carts) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    } else if (validate.resultFound(carts, res)) {
                        const data = validate.formatData(carts);
                        respHandler.sendSuccess(res, 200, 'Cart listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
        }
    },
    getUserCarts: (req, res) => {
        try {
            Cart.find({$and: [{userId: req.params.userId}, {checkout_status: false}, {cart_type: 'FARM_SHOP'}]})
                .populate('userId', 'first_name last_name email full_name phone_number')
                .populate({
                    path: "farmId",
                    populate: { path: "farm_status farm_type" },
                })
                .exec((err, carts) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    } else if (validate.resultFound(carts, res)) {
                        const data = validate.formatData(carts);
                        respHandler.sendSuccess(res, 200, 'Cart listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
        }
    },
    getUserProductCarts: (req, res) => {
        try {
            Cart.find({$and: [{userId: req.params.userId}, {checkout_status: false}, {cart_type: 'FARM_PRODUCT'}]})
                .populate('userId', 'first_name last_name email full_name phone_number')
                .populate({
                    path: "productId",
                    populate: { path: "product_status product_type" },
                })
                .exec((err, carts) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    } else if (validate.resultFound(carts, res)) {
                        const data = validate.formatData(carts);
                        respHandler.sendSuccess(res, 200, 'Cart listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
        }
    },
    getUserCartCount: (req, res) => {
        try {
            Cart.countDocuments({$and: [{userId: req.params.userId}, {checkout_status: false}]})
                .exec((err, count) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to count carts.', err);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Cart listed successfully', count);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list carts.', err);
        }
    },
    createCart: (req, res) => {
        try {
            Cart.findOne({$and: [{userId: req.body.userId}, {farmId: req.body.farmId}, {checkout_status: false}]}, (err, cart) => {
                if(cart) {
                    let qty = parseInt(cart.quantity, 10);
                    let total_price = parseFloat(cart.total_price);
                    FarmShop.findById(req.body.farmId, (error, farmShop) => {
                        if(error) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', error);
                        }
                        total_price += parseFloat(farmShop.amount_to_invest);
                        qty += 1;
                        Cart.findByIdAndUpdate(cart._id, {$set: {total_price: total_price, quantity: qty}}, {new: true}, (e, success) => {
                            if(e) {
                                respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', e);
                            }
                            respHandler.sendSuccess(res, 200, 'Cart updated successfully', success);
                        });
                    })
                } else {
                    Cart.create(req.body, (error, cart) => {
                        if(error) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', error);
                        }
                        respHandler.sendSuccess(res, 200, 'Added to cart successfully', cart);
                    })
                }
            });


        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', err);
        }
    },
    createProductCart: (req, res) => {
        try {
            Cart.findOne({$and: [{userId: req.body.userId}, {productId: req.body.productId}, {cart_type: 'FARM_PRODUCT'}, {checkout_status: false}]}, (err, cart) => {
                if(cart) {
                    let qty = parseInt(cart.quantity, 10);
                    let total_price = parseFloat(cart.total_price);
                    FarmProduct.findById(req.body.productId, (error, farmProduct) => {
                        if(error) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', error);
                        }
                        total_price += parseFloat(farmProduct.price);
                        qty += 1;
                        Cart.findByIdAndUpdate(cart._id, {$set: {total_price: total_price, quantity: qty}}, {new: true}, (e, success) => {
                            if(e) {
                                respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', e);
                            }
                            respHandler.sendSuccess(res, 200, 'Cart updated successfully', success);
                        });
                    })
                } else {
                    Cart.create(Object.assign({cart_type: 'FARM_PRODUCT'}, req.body), (error, cart) => {
                        if(error) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', error);
                        }
                        respHandler.sendSuccess(res, 200, 'Added to cart successfully', cart);
                    })
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', err);
        }
    },
    bulkProductToCart: (req, res) => {
        try {
            // [{userId, priductId, quantity, cart_type}]
            const carts = req.body.cart;
           eachAsync(carts, (_cart, i, done) => {
               const productId = _cart.productId;
               const quantitySet = _cart.quantity;
               Cart.findOne({$and: [{userId: req.body.userId}, {productId: productId}, {cart_type: 'FARM_PRODUCT'}, {checkout_status: false}]}, (err, cart) => {
                   if(cart) {
                       let qty = parseInt(cart.quantity, 10);
                       let total_price = parseFloat(cart.total_price);
                       FarmProduct.findById(productId, (error, farmProduct) => {
                           if(error || !farmProduct) {
                               done();
                               // respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', error);
                           } else {
                               total_price += parseFloat(farmProduct.price);
                               qty += quantitySet;
                               Cart.findByIdAndUpdate(cart._id, {$set: {total_price: total_price, quantity: qty}}, {new: true}, (e, success) => {
                                   if(e) {
                                       done();
                                       // respHandler.sendError(res, 404, 'FAILURE', 'Unable to update to cart.', e);
                                   } else {
                                       // respHandler.sendSuccess(res, 200, 'Cart updated successfully', success);
                                       done();
                                   }
                               });
                           }
                       })
                   } else {
                       Cart.create({cart_type: 'FARM_PRODUCT', userId: req.body.userId, productId: _cart.productId, quantity: _cart.quantity}, (error, cart) => {
                           if(error) {
                               // respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', error);
                               done();
                           } else {
                               done();
                               // respHandler.sendSuccess(res, 200, 'Added to cart successfully', cart);
                           }
                       })
                   }
               });
           }, () => {
               respHandler.sendSuccess(res, 200, 'Added to cart successfully', {});
           })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add to cart.', err);
        }
    },
    getCart: (req, res) => {
        try {
            Cart.findById(req.params.id)
                .populate('userId', 'first_name last_name email full_name phone_number')
                .populate({
                    path: "farmId",
                    populate: { path: "farm_status" }
                })
                .populate({
                    path: "productId",
                    populate: { path: "product_status" }
                })
                .exec((err, cart) => {
                if (err || !cart){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get cart by id.', err);
                } else if (validate.resultFound(cart, res)) {
                    const data = validate.formatData(cart);
                    respHandler.sendSuccess(res, 200, 'Cart fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get cart.', err);
        }
    },
    putCart: async (req, res) => {
        try {
            let total_price = 0.0;
            const {farmId, quantity, productId} = req.body;

           if(farmId) {
               FarmShop.findById(farmId).exec((err, farmShop) => {
                   if(err || !farmShop) {
                       respHandler.sendError(res, 404, 'FAILURE', 'Unable to update cart.', err);
                   } else {
                       total_price = (parseInt(quantity, 10) * parseFloat(farmShop.amount_to_invest));
                       Cart.findByIdAndUpdate(req.params.id, {$set: {quantity: quantity, total_price: total_price}}, { new: true }, (err, cart) => {
                           if (err || !cart){
                               respHandler.sendError(res, 404, 'FAILURE', 'Unable to update cart.', err);
                           } else {
                               respHandler.sendSuccess(res, 200, 'Cart updated successfully!', cart);
                           }
                       });
                   }
               });
           } else if(productId) {
               FarmProduct.findById(productId).exec((err, farmProduct) => {
                   if(err || !farmProduct) {
                       respHandler.sendError(res, 404, 'FAILURE', 'Unable to update cart.', err);
                   } else {
                       total_price = (parseInt(quantity, 10) * parseFloat(farmProduct.price));
                       Cart.findByIdAndUpdate(req.params.id, {$set: {quantity: quantity, total_price: total_price}}, { new: true }, (err, cart) => {
                           if (err || !cart){
                               respHandler.sendError(res, 404, 'FAILURE', 'Unable to update cart.', err);
                           } else {
                               respHandler.sendSuccess(res, 200, 'Cart updated successfully!', cart);
                           }
                       });
                   }
               });
           } else {
               respHandler.sendError(res, 400, 'FAILURE', 'Unable to update cart');
           }
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update cart');
        }
    },
    deleteCart: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', async (deletedBy) => {

                Cart.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete cart.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Cart deleted by id successfully!', {});
                });

            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete cart by id');
        }
    },
};

module.exports = CartController;