const Wallet = require('../models/Wallet');
const User = require('../models/Users');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const moment = require('moment');
const eachAsync = require('each-async');
const WalletController = {
    getWallets: (req, res) => {
        try {
            Wallet.find({})
                .exec((err, wallets) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallets.', err);
                    } else if (validate.resultFound(wallets, res)) {
                        const data = validate.formatData(wallets);
                        respHandler.sendSuccess(res, 200, 'Wallet listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallets.', err);
                    }
                });
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list wallets.', err);
        }
    },
    createWallet: (req, res) => {
        try {
            User.find({}, (e, users) => {
                eachAsync(users, (user, i, done) => {
                    Wallet.create({userId: user._id}, () => {
                        done();
                    })
                }, () => {
                    respHandler.sendSuccess(res, 200, 'Wallet created successfully', {});
                })
            });

        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create wallet.', err);
        }
    },
    withdrawWallet: (req, res) => {
        respHandler.sendSuccess(res, 200, 'Withdrawal from wallet is pending!', {});
    },
    getUserWallet: (req, res) => {
        try {
            Wallet.findOne({userId: req.params.userId}).exec((err, wallet) => {
                if (err || !wallet){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user wallet.', err);
                } else if (validate.resultFound(wallet, res)) {
                    const data = validate.formatData(wallet);
                    respHandler.sendSuccess(res, 200, 'User wallet fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user wallet.', err);
        }
    },
    getWallet: (req, res) => {
        try {
            Wallet.findById(req.params.id).exec((err, wallet) => {
                if (err || !wallet){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get wallet by id.', err);
                } else if (validate.resultFound(wallet, res)) {
                    const data = validate.formatData(wallet);
                    respHandler.sendSuccess(res, 200, 'Wallet fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get wallet.', err);
        }
    },
    putWallet: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Wallet.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, wallet) => {
                if (err || !wallet){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update wallet.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Wallet updated successfully!', wallet);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet');
        }
    },
    deleteWallet: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Wallet.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete wallet.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Wallet deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete wallet by id');
        }
    },
};

module.exports = WalletController;