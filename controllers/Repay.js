const Repay = require('../models/Repay');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const Sponsorship = require('../models/Sponsorship');
const User = require('../models/Users');
const moment = require('moment');
const eachAsync = require('each-async');
const RepayController = {
    getRepays: (req, res) => {
        try {
            Repay.find({year: req.params.year})
                .sort("month_number")
                .exec((err, repays) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repay for ' + req.params.year, err);
                    } else if (validate.resultFound(repays, res)) {
                        const data = validate.formatData(repays);
                        respHandler.sendSuccess(res, 200, 'Repay listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repays.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repays.', err);
        }
    },
    getCurrentYear: (req, res) => {
        respHandler.sendSuccess(res, 200, 'Current year retrieved', new Date().getFullYear());
    },
    autoGetRepay: (req, res) => {
        try {
            const autoYear = new Date().getFullYear();
            Repay.find({year: autoYear})
                .sort("month_number")
                .exec((err, repays) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repay for ' + req.params.year, err);
                    } else if (validate.resultFound(repays, res)) {
                        const data = validate.formatData(repays);
                        respHandler.sendSuccess(res, 200, 'Repay listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repays.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list repays.', err);
        }
    },
    createRepay: (req, res) => {
        try {
            const {year} = req.body;
            const currentYear = new Date().getFullYear();
            if(year < currentYear) {
                respHandler.sendError(res, 400, 'FAILURE', 'You are not allow to generate payout for past years', {});
            } else {
                Repay.countDocuments({year: year}, (err, counts) => {
                    if(counts > 0 || err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to generate repay for ' + year, error);
                    } else {
                        const months = [
                            {month: 'January', month_number: 1, year: year},
                            {month: 'February', month_number: 2, year: year},
                            {month: 'March', month_number: 3, year: year},
                            {month: 'April', month_number: 4, year: year},
                            {month: 'May', month_number: 5, year: year},
                            {month: 'June', month_number: 6, year: year},
                            {month: 'July', month_number: 7, year: year},
                            {month: 'August', month_number: 8, year: year},
                            {month: 'September', month_number: 9, year: year},
                            {month: 'October', month_number: 10, year: year},
                            {month: 'November', month_number: 11, year: year},
                            {month: 'December', month_number: 12, year: year},
                        ];
                        Repay.create(months, (error, repay) => {
                            if(error) {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to generate repay '+ year, error);
                            } else {
                                respHandler.sendSuccess(res, 200, 'Repay generated successfully for ' + year, repay);
                            }
                        });
                    }
                });
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new repay.', err);
        }
    },
    putRepay: (req, res) => {
        try {
            let update = {};
            if(req.body.status || req.body.commencement_date) {
                if(req.body.status.toUpperCase() === 'PENDING' && !req.body.commencement_date) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Commencement date is required for pending months', {});
                }
                update = {
                    status: req.body.status.toUpperCase(),
                    commencement_date: req.body.commencement_date
                };
                Repay.findByIdAndUpdate(req.params.id, {$set: update}, { new: true }, (err, repay) => {
                    if (err || !repay){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to update repay.', err);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Repay updated successfully!', repay);
                    }
                });


            } else {
                respHandler.sendError(res, 401, 'FAILURE', 'You can only update status or commencement date', {});
            }
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update repay');
        }
    },
    getRepaySponsorship: (req, res) => {
        try {
            const autoYear = new Date().getFullYear();
            const {firstName, email} = req.body;
            Repay.find({$and: [{status: 'ONGOING'}, {year: autoYear}]}, "month _id month_number", (repayError, repayMonths) => {
                if(repayError || !repayMonths.length) {
                    respHandler.sendError(res, 400, 'FAILURE', 'No ongoing payout at the moment', repayError);
                } else {
                    const months = [];
                    for(let mon of repayMonths){
                        months.push(mon.month);
                    }
                    const _month = months.join(', ');

                    // $and: [{first_name: firstName}, {email: email}]
                    User.findOne({email: email}, (uErr, user) => {
                        if(uErr || !user) {
                            respHandler.sendError(res, 400, 'FAILURE', _month + ' payments only.', uErr);
                        } else {
                            let payouts = [];
                            eachAsync(repayMonths, (repayMonth, i, done) => {
                                Sponsorship.find({$and: [{userId: user._id}, {status: {$in: ['STARTED', 'COMPLETED'] } }]})
                                    .populate('orderId')
                                    .populate('farmId', 'farm_name')
                                    .populate('userId', 'first_name last_name email full_name phone_number')
                                    .sort('expected_end_date')
                                    .exec((err, sponsorships) => {
                                        if (err) {
                                            done();
                                        } else {
                                            const matchedFilters = sponsorships.filter(sponsor => {
                                                const date = moment(sponsor.expected_end_date);
                                                const __month = date.month();
                                                return (((__month + 1) == (repayMonth.month_number)) && parseInt(autoYear) === date.year());
                                            });
                                            payouts = payouts.concat(matchedFilters);
                                            done();
                                        }
                                    })
                            }, () => {
                                console.log('Payout ', payouts);
                                if(payouts.length) {
                                    respHandler.sendSuccess(res, 200, 'Your payouts fetched successfully', payouts);
                                } else {
                                    respHandler.sendError(res, 400, 'FAILURE', _month + ' payouts only.', {});
                                }
                            });
                        }
                    });
                }
            });
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Error fetching payouts.', err);
        }
    },
};

module.exports = RepayController;