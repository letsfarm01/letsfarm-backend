const Update = require('../models/Update');
const Push = require('../models/PushTo');
const Sponsor = require('../models/Sponsorship');
const FarmShop = require('../models/FarmShop');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const UpdateController = {
    getUpdates: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9100;
            const totalCount = await Update.countDocuments({}).exec();
            Update.find({})
                .exec((err, updates) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list updates.', err);
                    } else if (validate.resultFound(updates, res)) {
                        const data = {
                            data: validate.formatData(updates),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Updates listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list updates.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list updates.', err);
        }
    },
    getFarmUpdates: async (req, res) => {
        try {
            const allNotifications = await Push.find({pushTo: 'ALL'})
                .populate('updateId')
                .sort("-createdAt")
                .exec();
            controllerService.getLoginUser(req, 'userId', (userId) => {
                const specialUpdates = [];
                FarmShop.find({}, (farmError, farmShops) => {
                    if(farmError) {
                        sendPushMessage(res, allNotifications, []);
                    } else {
                        eachAsync(farmShops, (farmShop, i, done) => {
                            Sponsor.countDocuments({$and: [{farmId: farmShop._id}, {userId: userId}]}, (countError, sponsorCount) => {
                                if(sponsorCount) {
                                    Push.find({pushTo: farmShop._id})
                                        .populate('updateId')
                                        .populate('farm_name', '_id farm_name')
                                        .sort("-createdAt")
                                        .exec((pushError, pushFound) => {
                                        if(pushError) {
                                            done();
                                        } else {
                                            specialUpdates.push(...pushFound);
                                            done();
                                        }
                                        });
                                } else {
                                    done();
                                }
                            });
                        }, () => {
                            const sortedUpdates = specialUpdates.sort(function(a, b){
                                return new Date(b.createdAt) - new Date(a.createdAt);
                            });
                            sendPushMessage(res, allNotifications, sortedUpdates);
                        })
                    }
                });
            });
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list updates.', err);
        }
    },
    createUpdate: (req, res) => {
        try {
            const {title, message_body, img_link} = req.body;
            if(!title) {
                respHandler.sendError(res, 400, 'FAILURE', 'Title field is required!.', {});
            } else if(!message_body) {
                respHandler.sendError(res, 400, 'FAILURE', 'Message body is required!.', {});
            } else {
                Update.create({title, message_body, img_link}, (error, update) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm update.', error);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Farm update created successfully', update);
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm update.', err);
        }
    },
    pushUpdates: (req, res) => {
        try {
            const {pushTo, updateId} = req.body;
            Push.create({pushTo, updateId, farm_name: (pushTo === 'ALL') ? null: pushTo}, (error, update) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to push farm update.', error);
                } else {
                    Update.findByIdAndUpdate(updateId, {$set: {publish: true}}, {new: true}, () => {
                        respHandler.sendSuccess(res, 200, 'Farm Update pushed successfully', update);
                    });
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to push farm update.', err);
        }
    },
    getUpdateById: (req, res) => {
        try {
            Update.findById(req.params.id)
                .exec((err, update) => {
                    if (err || !update){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get update by id.', err);
                    } else if (validate.resultFound(update, res)) {
                        respHandler.sendSuccess(res, 200, 'Update fetched successfully', update);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get update.', err);
        }
    },
    putUpdate: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Update.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, update) => {
                if (err || !update){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update farm update.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm update updated successfully!', update);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update farm update');
        }
    },
    deleteUpdate: (req, res) => {
        try {
            Update.findByIdAndDelete(req.params.id, (err, update) => {
                if (err){
                    return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete update.', err);
                }
                respHandler.sendSuccess(res, 200, 'Update deleted by id successfully!', {});
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete update by id');
        }
    },
};

module.exports = UpdateController;

function sendPushMessage(res, all, special) {
    respHandler.sendSuccess(res, 200, 'Farm Updates listed successfully', {all: all, updates: special});
}