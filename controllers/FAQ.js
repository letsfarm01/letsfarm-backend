const FAQ = require('../models/FAQ');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const FAQController = {
    getFAQs: (req, res) => {
        try {
            const { category, limit } = req.query;
            FAQ.find({active: true, category: category || 'GENERAL' })
                .limit(parseInt(limit) || 40)
                .exec((err, faqs) => {
                if (err) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list faqs.', err);
                } else if (validate.resultFound(faqs, res)) {
                    const data = validate.formatData(faqs);
                    respHandler.sendSuccess(res, 200, 'FAQ listed successfully', data);
                } else {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list faqs.', err);
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list faqs.', err);
        }
    },
    createFAQ: (req, res) => {
        try {
            FAQ.create(req.body, (error, faq) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new faq.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'FAQ added successfully', faq);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new faq.', err);
        }
    },
    getFAQ: (req, res) => {
        try {
            FAQ.findById(req.params.id).exec((err, faq) => {
                if (err || !faq){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get faq by id.', err);
                } else if (validate.resultFound(faq, res)) {
                    const data = validate.formatData(faq);
                    respHandler.sendSuccess(res, 200, 'FAQ fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get faq.', err);
        }
    },
    putFAQ: (req, res) => {
        try {
            console.log('This is body ', req.body);
            FAQ.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, faq) => {
                if (err || !faq){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update faq.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'FAQ updated successfully!', faq);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update faq');
        }
    },
    deleteFAQ: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                FAQ.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete faq.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'FAQ deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete faq by id');
        }
    },
};

module.exports = FAQController;