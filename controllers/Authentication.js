const Users = require('../models/Users');
const PasswordReset = require('../models/PasswordReset');
const SignupToken = require('../models/SignupToken');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
// const pushNotify = require('../services/pushnotificationService');
const jwt    = require('jsonwebtoken');
const config = require('../config/constants');
const bcrypt = require('bcrypt');
const Tokens = require('../models/Tokens');
const ArchiveSponsor = require('../models/ArchivedSponsor');
const Wallet = require('../models/Wallet');
const Referral = require('../models/Refarral');
const DeviceId = require('../models/DevicesId');
const Sponsor = require('../models/Sponsorship');
const Cart = require('../models/Cart');
const eachAsync = require('each-async');
const nanoid = require('nanoid');

const moment = require('moment');
const request = require('request');
const encrypt = require('../services/jsEncryptService');
const jwtService = require('../services/jwtService');

const pushNotiy = require('../services/pushnotificationService');
const jwtEncrypt = require('jwt-token-encrypt');
const mailer = require('../mailing/mailSystem');
const mongoose = require('mongoose');


const google = require('googleapis').google;
const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2();
const AuthenticationController = {
    returnMessage: (req, res, next) => {
        /*Cart.updateMany({}, {$set: {cart_type: 'FARM_SHOP'}}, {multi: true}, () => {
            respHandler.sendSuccess(res, 200, 'Please specify the endpoint75678!', {});
        });*/

        /*const rearrange = "01/03/2021".split('/');
        const newDate  = new Date(`${parseInt(rearrange[1])}/${parseInt(rearrange[0])}/${parseInt(rearrange[2])}`) || `${parseInt(rearrange[1])}/${parseInt(rearrange[0])}/${parseInt(rearrange[2])}`;
        console.log('CHECK IF DATE IS VALID === ');
        ArchiveSponsor.findOneAndUpdate({_id: "5efb72711c6e2300174c024e"}, {$set: {order_status: 'PROCESSING', formatted_roi_date: newDate}}, {new: true}, (e, s) => {
            console.log('WE ', e, s);
            respHandler.sendSuccess(res, 200, 'Completed with update!', {});
        });*/
       /* ArchiveSponsor.find({}, (error, sponsors) => {
                eachAsync(sponsors, (sponsor, i, done) => {
                    // console.log('SPONSOE ', sponsor._id);5efb72711c6e2300174c024e
                     Users.findOne({email: sponsor.email}, (e, user) => {
                         // console.log('USER  === ',sponsor.farm_name,  user.full_name, user._id);
                         if(sponsor.roi_date) {
                            const rearrange = sponsor.roi_date.split('/');
                            const newDate  = new Date(`${parseInt(rearrange[1])}/${parseInt(rearrange[0])}/${parseInt(rearrange[2])}`) || `${parseInt(rearrange[1])}/${parseInt(rearrange[0])}/${parseInt(rearrange[2])}`;
                            // console.log('CHECK IF DATE IS VALID === ', e, user);
                            if(e || !user) {
                                // console.log('USER ERROR ');
                                ArchiveSponsor.findOneAndUpdate({_id: sponsor._id}, {$set: {order_status: 'PROCESSING', formatted_roi_date: newDate}}, {new: true}, (e, s) => {
                                    // console.log('2345 WE ', e, s);
                                    done();
                                });
                            } else {
                                // console.log('NO USER ERROR', `p${sponsor._id.toString()}p`);
                                ArchiveSponsor.findOneAndUpdate({_id: sponsor._id}, {$set: {order_status: 'PROCESSING', formatted_roi_date: newDate, userId: user._id || null}}, {new: true}, (e, s) => {
                                    // console.log('WE ', e, s);
                                    done();
                                });
                            }
                        } else {
                            done();
                        }
                    });
                }, () => {
                    respHandler.sendSuccess(res, 200, 'Completed with update!', {});
                });
        });*/


                        // respHandler.sendSuccess(res, 200, 'Please specify the endpoint75678!', {});


      /*  eachAsync([{
            first_name: 'Chidiebere',
            last_name: 'Nwamadi',
            email: 'nwamadichidiebere@gmail.com',
            referral_code: `${'Chidiebere'.toString().slice(0, 5).toUpperCase()}-${nanoid(5).toString().toUpperCase()}-${controllerService.getRandomInt(9999)}`,
            phone_number: '08064227503',
            is_verified: true,
            password_: nanoid(12),
            password: null,
            referral_email: 'arokoyuolalekan@gmail.com',
            migrated: true
        }], (_user, i, done) => {
            bcrypt.hash(_user.password_, 10,  (err, hash) => {
                if(err) {
                    // console.log('Error ', err);
                    done();
                } else {
                    _user.password = hash;
                    // console.log('USER ', _user);
                    Users.create(_user, (e, suc) => {
                        if(e) {
                            done();
                        } else {
                            Wallet.create({userId: suc._id, wallet_balance: 0.0, last_fund_date: 'nil'},
                                () => {
                                    mailer.sendHtml({email: suc.email, full_name: `${suc.first_name} ${suc.last_name}`},
                                        'Password update', 'UserPasswordUpdate', {first_name: suc.first_name,
                                            password: _user.password_,
                                            title: 'Password update'});
                                    done();
                                });
                        }
                    })
                }
            });
        }, () => {
            ArchiveSponsor.create({farm_name: 'Rice Processing', quantity: 2,
                email: 'nwamadichidiebere@gmail.com', first_name: 'Chidiebere', last_name: 'Nwamadi',
                order_number: '19142', order_total: '100000', order_status: 'Completed',
                date_of_sponsor: '27/5/2020', roi_date: '28/2/2020', roi_amount: '135000'}, () => {
                respHandler.sendSuccess(res, 200, 'Done with migration!', {});
            });

        });
*/


        /*  try {
            /!*  Referral.find({$and: [{has_joined: true}, {has_sponsor: true}]}, (error, referrals) => {
                  if(error ) {
                      // do nothing about error
                      respHandler.sendError(res, 400, 'FAILURE', 'Error with staring refferra--' + JSON.stringify(referrals) + 'Error ' + JSON.stringify(error), error);
                  } else {
                      eachAsync(referrals, (referral, i, done) => {
                          console.log('REFERRAL ------ ', referral);
                          Users.findOne({$and: [{_id: referral.referree}, {referral_done: true}]}, (error, userFound) => {
                              if(error || !userFound) {
                                  // console.log('USERFOUND ', userFound, error);
                                  done(); // they have paid their dues
                              } else {
                                  // console.log('USERFOUND 34', userFound, referral.referree);
                                  // they have not pay the user who refer them
                                  Sponsor.findOne({userId: userFound._id})
                                      .populate('orderId', 'total_price')
                                      .exec((sError, sponsor) => {
                                          if(sError || !sponsor) {
                                              console.log('SPONSOR FOUND ', sponsor, sError);
                                              done(); // they are yet to sponsor a farm
                                          } else {
                                              console.log('SPONSOR FOUND 2', referral, referral.percentage);
                                              // referree has sponsor a farm, time to gift to referral
                                              const percentage = parseFloat(referral.percentage || 0);
                                              const bonus = (parseFloat(percentage / 100) * parseFloat(sponsor.orderId.total_price)) || 0;
                                              console.log('PERCENTAGE FOUND 2', percentage, bonus);
                                              let is_sponsor_valid = true;
                                              if(parseFloat(sponsor.orderId.total_price) < 50000 ) {
                                                  is_sponsor_valid =  false;
                                              }
                                              console.log('PERCENTAGE FOUND 5', is_sponsor_valid, bonus);
                                              Referral.findByIdAndUpdate(referral._id, {$set: {
                                                  has_sponsor: true,
                                                  amount_sponsor: sponsor.orderId.total_price,
                                                  sponsorId: sponsor._id,
                                                  expected_bonus: (is_sponsor_valid) ? bonus : 0,
                                                  is_sponsor_valid: is_sponsor_valid
                                              }}, {new: true}, (uError, referralUpdate) => {
                                                  if(uError) {
                                                      done();
                                                  } else {
                                                      // update User referral_done
                                                      done();

                                                  }
                                              })
                                          }
                                      });
                              }
                          });
                      }, () => {
                          // winston.logger.log('info', `Cron job for referral manager completed @ ---${moment.now()}`);
                          respHandler.sendSuccess(res, 200, `Cron job for referral manager completed @ ---${moment.now()}`, {});

                      });
                  }
              });*!/
          } catch(e){
              // winston.logger.log('error', `Unable to manage pending referral`);
              respHandler.sendError(res, 401, 'FAILURE', 'Error with staring refferra', e);

          }
*/



        // respHandler.sendSuccess(res, 200, 'Please specify the endpoint!', {});
/*
        Users.find({referral_code: null}, (er, users) => {
            eachAsync(users, (user, i, done) => {
                const referralCode = `${user.first_name.toString().slice(0, 5).toUpperCase()}-${nanoid(5).toString().toUpperCase()}-${controllerService.getRandomInt(9999)}`;
                Users.findByIdAndUpdate(user._id, {$set: {referral_code: referralCode}}, {new: true}, () => {
                    done();
                });
            }, () => {
                respHandler.sendSuccess(res, 200, 'Please specify the endpoint!', {});
            });
        })*/
    },
    recaptchaProcess: (req, res) => {
        try {
            request({
                url: `https://www.google.com/recaptcha/api/siteverify`,
                method: "POST",
                json: true,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: `secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${req.body.token}`
            }, (error, resp) => {
                console.log('RECAPTCHA', resp.body, error);
                if (error) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify request', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Verification successful', resp.body);
                }
            });
        } catch (error) {
            respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify request', error);
        }
    },
    logoutUser: (req, res, next) => {
        systemLogout(req, res, next);
    },
    register: (req, res) => {
        try {
            const userObject = req.body;
            const referralCode = `${userObject.first_name.toString().slice(0, 5).toUpperCase()}-${nanoid(5).toString().toUpperCase()}-${controllerService.getRandomInt(9999)}`;
            console.log('USER => ', userObject);
            if(userObject && !validate.isEmptyObject(userObject)){
                if(!userObject.password) {
                    respHandler.sendError(res, 406, 'FAILURE', 'No password provided!');
                } else if(!userObject.email) {
                    respHandler.sendError(res, 406, 'FAILURE', 'Provide user email!');
                } else if(userObject.referral_email && userObject.email === userObject.referral_email) {
                    respHandler.sendError(res, 401, 'FAILURE', 'You can\'t user yourself as referral!');
                } else {
                    userObject.referral_code = referralCode;
                    bcrypt.hash(userObject.password, 10,  (err, hash) => {
                        userObject.password = hash;
                        Users.create(userObject, (err, user) => {
                            if (err) {
                                console.log('err.name' , err);
                                switch (err.name) {
                                    case 'ValidationError': {
                                        if (err.message.includes('`email` to be unique')){
                                            respHandler.sendError(res, 406, 'FAILURE', 'Email already exist!');
                                        } else if(err.message.includes('`email` is invalid')) {
                                            respHandler.sendError(res, 401, 'FAILURE', 'Invalid email address!');
                                        } else {
                                            respHandler.sendError(res, 401, 'FAILURE', err.message, err);
                                        }
                                        break;
                                    }
                                    default: {
                                        respHandler.sendError(res, 400, 'FAILURE', err.message, err);
                                        break;
                                    }
                                }
                            }
                            else if(validate.resultFound(user, res)) {
                                const data = validate.formatData(user);
                                const tokenGen = `Q${nanoid()}1O9A${controllerService.getRandomInt(99999)}${nanoid()}${controllerService.getRandomInt(989999)}`;
                                SignupToken.create({token: tokenGen, userId: user._id}, () => {});
                                mailer.sendHtml({email: user.email, full_name: `${user.first_name} ${user.last_name}`},
                                    'Sign up completed, verify email', 'ActivateAccount', {first_name: user.first_name,
                                        title: 'Verify email address', url: `${process.env.FRONT_END_URL}verify-user-email/${user._id}/${tokenGen}?userId=${controllerService.getRandomInt(989999)}`});
                                if(userObject.referral_email) {
                                    handleReferral(userObject, user);
                                }
                                respHandler.sendSuccess(res, 200, 'We have sent a verification mail to you at ' + user.email+ ' please click the link to verify account.', data);
                            }
                        });
                    });
                }
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Provide user details');
            }

        } catch (err){
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to register user');
        }
    },
    socialRegister: (req, res, next) => {
        try {
            const { accessToken, auth_type } = req.body;
            if(auth_type === 'GOOGLE') {
                oauth2Client.setCredentials({access_token: accessToken});
                const oauth2 = google.oauth2({
                    auth: oauth2Client,
                    version: 'v2'
                });
                oauth2.userinfo.get((err, response) => {
                    if (err) {
                        // console.log('GOOGLE ERROR', err);
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to signup using google!');
                    } else {
                        // console.log('GOOGLE SUCCESS', response.data);
                        signupSocial({
                            first_name: req.body.first_name,
                            user_image_url: response.data.picture,
                            last_name: req.body.last_name,
                            social_id: response.data.id,
                            auth_type: 'GOOGLE',
                            password: null,
                            phone_number: req.body.phone_number,
                            email: req.body.email,
                            is_verified: true,
                            is_auth_sign_up: true,
                            role: 'USER',
                            referral_email: req.body.referral_email,
                            channel_notice: req.body.channel_notice,
                            address: req.body.address,
                        }, req, res, next);
                    }
                });
            } else if(auth_type === 'FACEBOOK') {
                request({
                    url: `https://graph.facebook.com/me`,
                    method: "GET",
                    json: true,
                    qs: {
                        fields: ['id', 'email', 'first_name', 'last_name', 'picture'].join(','),
                        access_token: accessToken,
                    },
                }, (error, response) => {
                    if (error) {
                        // console.log('FACEBOOK ERRROr', error);
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get facebook user', error);
                    } else {
                        const fbSuccess = JSON.parse(JSON.stringify(response.body));
                        // console.log('FACEBOOK SUCCESS', fbSuccess.id, fbSuccess.picture.data.url);
                        signupSocial({
                            first_name: req.body.first_name,
                            user_image_url: fbSuccess.picture.data.url,
                            last_name: req.body.last_name,
                            social_id: fbSuccess.id,
                            auth_type: 'FACEBOOK',
                            password: null,
                            phone_number: req.body.phone_number,
                            email: req.body.email,
                            is_verified: true,
                            is_auth_sign_up: true,
                            role: 'USER',
                            referral_email: req.body.referral_email,
                            channel_notice: req.body.channel_notice,
                            address: req.body.address,
                        }, req, res, next);
                    }
                });
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Endpoint not available');
            }
        } catch (err){
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to register user');
        }
    },
    resendVerificationLink: (req, res) => {
        const email = req.body.email;
        try {
            Users.findOne({email: email}, (err, user) => {
                if(err || !user) {
                    respHandler.sendError(res, 401, 'FAILURE', email + ' does not exist on our system!');
                } else if(user && user.is_verified) {
                    respHandler.sendError(res, 401, 'FAILURE', 'User already verified, proceed to login');
                } else {
                    const tokenGen = `Q${nanoid()}1O9A${controllerService.getRandomInt(99999)}${nanoid()}${controllerService.getRandomInt(989999)}`;
                    SignupToken.create({token: tokenGen, userId: user._id}, () => {});
                    mailer.sendHtml({email: user.email, full_name: `${user.first_name} ${user.last_name}`},
                        'Verify account', 'ActivateAccount', {first_name: user.first_name,
                            title: 'Activate Account', url: `${process.env.FRONT_END_URL}verify-user-email/${user._id}/${tokenGen}?userId=${controllerService.getRandomInt(989999)}`});
                    respHandler.sendSuccess(res, 200, 'We have sent a verification mail to you at ' + user.email+ ', please click the link to verify account.', {});
                }
            });
        } catch (error) {
            console.log('Error ', error);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to resend verification link to '+ email);
        }
      },
    resetPassword: (req, res) => {
        try {
            const { email, url } = req.body;
            if(!email) {
                return respHandler.sendError(res, 400, 'FAILURE', 'No email provided for this action.');
            }
            Users.findOne({email: email }, (err, user) => {
                if(err || !user) {
                    respHandler.sendError(res, 400, 'FAILURE', 'User email not recognise on our system!');
                } else if(user && user.auth_type !== 'NO_AUTH') {
                    respHandler.sendError(res, 401, 'FAILURE', 'Password cannot be reset, account uses social authentication');
                } else if(user && !validate.isEmptyObject(user)){
                    const code = nanoid(8);
                    const tokenGen = `Q${nanoid()}1O9A${controllerService.getRandomInt(99999)}${nanoid()}${controllerService.getRandomInt(989999)}`;
                    console.log('Mobile Version ', code, tokenGen );
                    if(!url) {
                        PasswordReset.create({token: code, userId: user._id}, () => {});
                        mailer.sendHtml({email: user.email, full_name: `${user.first_name} ${user.last_name}`},
                              'Reset password', 'MobilePasswordReset', {title: 'Reset Password', code});
                      } else {
                        PasswordReset.create({token: tokenGen, userId: user._id}, () => {});
                          const urlGen = `${url}/reset-password/token/special/${user._id}/${tokenGen}`;
                          mailer.sendHtml({email: user.email, full_name: `${user.first_name} ${user.last_name}`},
                              'Reset password', 'PasswordReset', {title: 'Reset Password', url: urlGen});
                      }
                    respHandler.sendSuccess(res, 200, `Password reset ${url ? 'link':'code'} sent to email successfully!`, {});
                }
            });
        } catch(err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to send password reset link');
        }
    },
    forgotPassword: (req, res) => {
        try {
            const { password, confirmPassword, token } = req.body;
            if(!password || !confirmPassword || !token) {
                return respHandler.sendError(res, 400, 'FAILURE', 'Incomplete password reset credentials.');
            }
            if(password !== confirmPassword) {
                respHandler.sendError(res, 400, 'FAILURE', 'Password must match!');
            } else {
                PasswordReset.findOne({token: token}, (tokenErr, tokenRes) => {
                    if (tokenErr || null === tokenRes) {
                        return respHandler.sendError(res, 401, 'FAILURE', 'Invalid password reset token!', tokenErr);
                    } else {
                        const exp = moment(tokenRes.expiredIn);
                        if (moment(tokenRes.created).isSameOrBefore(exp)) {
                            Users.findById(tokenRes.userId, (err, user) => {
                                if (err) {
                                    respHandler.sendError(res, 400, 'FAILURE', 'User not recognised!');
                                }
                                if (user && !validate.isEmptyObject(user)) {
                                    bcrypt.hash(password, 10, (err, hash) => {
                                        Users.findByIdAndUpdate(user._id, {$set: {password: hash}}, {new: true}, () => {
                                            PasswordReset.findOneAndRemove({userId: user._id}, () => {});
                                            respHandler.sendSuccess(res, 200, 'Password reset successful!', {});
                                        });
                                    })
                                }
                            });
                        } else {
                            return respHandler.sendSuccess(res, 401, 'FAILURE', 'Password reset token has expired!', tokenErr);
                        }
                    }
                });


            }
        } catch(err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to reset password!');
        }
    },
    verifySignUp: (req, res) => {
        try {
            const { userId, token } = req.body;
            if(!token) {
                return respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify email!')
            }
            Users.findById(userId, (err, user) => {
                if(err || !user) {
                    return respHandler.sendError(res, 400, 'FAILURE', 'Invalid validation link!');
                }
                if(user && !user.is_verified){
                    SignupToken.findOne({token: token}, (tokenErr, tokenRes) => {
                        if (tokenErr || null === tokenRes) {
                            return respHandler.sendSuccess(res, 200, 'Invalid sign up token!', tokenErr);
                        } else {
                            const exp = moment(tokenRes.expiredIn);
                            if (moment(tokenRes.created).isSameOrBefore(exp)) {
                                Users.findByIdAndUpdate(user._id, {$set: { is_verified: true }}, {new: true}, (err, user_) => {
                                    if(err) {
                                        return respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify email!');
                                    } else {
                                        SignupToken.findOneAndRemove({userId: user._id}, () => {});
                                        Wallet.create({userId: user._id, wallet_balance: 0.0, last_fund_date: 'nil'},
                                            (er, success) => {
                                            respHandler.sendSuccess(res, 200, 'User verified successfully!', {});
                                        });
                                    }
                                });
                            } else {
                                return respHandler.sendSuccess(res, 200, 'Sign up token has expired!', tokenErr);
                            }
                        }
                    });
                } else {
                    respHandler.sendSuccess(res, 200, 'User already verified, proceed with login.', {});
                }
            });
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify email!', error);
        }
    },
    changePassword: (req, res) => {
        try {
            const { old_password, confirm_password, new_password, userId } = req.body;
            if(new_password !== confirm_password) {
                respHandler.sendError(res, 401, 'FAILURE', 'Password must match the confirm password!');
            } else {
                Users.findById(userId, (checkPasswordErr, userInfo) => {
                    if(checkPasswordErr || !userInfo) {
                        respHandler.sendError(res, 400, 'FAILURE', 'User not recognised on the system!');
                    } else if(userInfo) {
                        if(userInfo.auth_type === 'NO_AUTH') {
                            bcrypt.compare(old_password, userInfo.password, (err, response) => {
                                //  console.log(err, response);
                                if(err || !response) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Incorrect user password!', err);
                                } else {
                                    //  console.log('Password is correct');
                                    bcrypt.hash(new_password, 10,  (err, hash) => {
                                        Users.findByIdAndUpdate(userInfo._id, {$set: {password: hash}}, { new: true }, () => {
                                            respHandler.sendSuccess(res, 200, 'Password updated successfully!', {});
                                        })
                                    })
                                }
                            });
                        } else {
                            bcrypt.hash(new_password, 10, (err, hash) => {
                                Users.findByIdAndUpdate(userInfo._id, {$set: {password: hash, auth_type: 'NO_AUTH', social_id: null, is_auth_sign_up: false}}, { new: true }, () => {
                                    respHandler.sendSuccess(res, 200, 'Password updated successfully!', {});
                                })
                            })
                        }
                    } else {
                        respHandler.sendError(res, 401, 'FAILURE', 'User not recognised!', {});
                    }
                });
            }
        } catch(err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to change password!');
        }
    },

    authenticateUser: (req, res, next) => {
        detectUser(req, res, next);
    },
    socialLogin: (req, _res, next) => {
        const { accessToken, auth_type, deviceId } = req.body;
       if(auth_type === 'GOOGLE') {
           oauth2Client.setCredentials({access_token: accessToken});
           const oauth2 = google.oauth2({
               auth: oauth2Client,
               version: 'v2'
           });
           oauth2.userinfo.get((err, res) => {
                   if (err) {
                       console.log('GOOGLE ERROR', err);
                       respHandler.sendError(_res, 400, 'FAILURE', 'Unable to login with google');
                   } else {
                       console.log('GOOGLE SUCCESS', res.data);
                       loginUser(req, _res, next, res.data.email, null, deviceId, 'GOOGLE', res.data.id);
                   }
               });
       } else if(auth_type === 'FACEBOOK') {
           request({
               url: `https://graph.facebook.com/me`,
               method: "GET",
               json: true,
               qs: {
                   fields: ['id', 'email', 'first_name', 'last_name'].join(','),
                   access_token: accessToken,
               },
           }, (error, response) => {
               console.log('FACEBOOK ', response.body, error);
               if (error) {
                   console.log('FACEBOOK ERROR', error);
                   respHandler.sendError(_res, 400, 'FAILURE', 'Unable to login with facebook');
               } else {
                   console.log('FACEBOOK SUCCESS', response.body);
                   loginUser(req, _res, next, response.body.email, null, deviceId, 'FACEBOOK', response.body.id);
               }
           });
       } else {
           respHandler.sendError(_res, 400, 'FAILURE', 'Endpoint not available');
       }
    }
};

module.exports = AuthenticationController;
function signupSocial(_data, req, res, next) {
    const userObject = _data;
    const referralCode = `${userObject.first_name.toString().slice(0, 5).toUpperCase()}-${nanoid(5).toString().toUpperCase()}-${controllerService.getRandomInt(9999)}`;
    console.log('USER => ', userObject);
    if(userObject && !validate.isEmptyObject(userObject)){
        if(!userObject.email) {
            respHandler.sendError(res, 406, 'FAILURE', 'Provide user email!');
        } else if(userObject.referral_email && userObject.email === userObject.referral_email) {
            respHandler.sendError(res, 401, 'FAILURE', 'You can\'t user yourself as referral!');
        } else {
            userObject.referral_code = referralCode;
            Users.create(userObject, (err, user) => {
                if (err) {
                    console.log('err.name' , err);
                    switch (err.name) {
                        case 'ValidationError': {
                            if (err.message.includes('`email` to be unique')){
                                return respHandler.sendError(res, 406, 'FAILURE', 'Email already exist in our system, please login!');
                            } else if(err.message.includes('`email` is invalid')) {
                                return respHandler.sendError(res, 401, 'FAILURE', 'Invalid email address!');
                            } else {
                                return respHandler.sendError(res, 401, 'FAILURE', err.message, err);
                            }

                        }
                        default: {
                            return respHandler.sendError(res, 400, 'FAILURE', err.message, err);
                        }
                    }
                }
                else if(validate.resultFound(user, res)) {
                    const data = validate.formatData(user);
                    mailer.sendHtml({email: user.email, full_name: `${user.first_name} ${user.last_name}`},
                        'Sign up completed', 'WelcomeMessage', {first_name: user.first_name, method: userObject.auth_type,
                            title: 'Welcome to Letsfarm'});
                    if(userObject.referral_email) {
                        handleReferral(userObject, user);
                    }
                    Wallet.create({userId: user._id, wallet_balance: 0.0, last_fund_date: 'nil'},
                        (er, success) => {
                            //todo: log user in maybe onthe frontend
                            loginUser(req, res, next, user.email, null, null, userObject.auth_type, userObject.social_id);
                        });
                    // respHandler.sendSuccess(res, 200, 'Signup successful, please login', data);
                }
            });
        }
    } else {
        respHandler.sendError(res, 400, 'FAILURE', 'Provide user details');
    }
}
function handleReferral(userObject, user) {
    Users.findOne({$or: [{email: userObject.referral_email},
            {referral_code: userObject.referral_email.toUpperCase()}]},
        (er, referral) => { // get referral person
            if(!er && referral) {
                Referral.findOneAndUpdate({$and: [
                        {referral_email: userObject.referral_email},
                        {referree_email: user.email},
                        {has_joined: false}]},
                    {$set: {
                        referral: referral._id,
                        has_joined: true, referree: user._id
                    }
                    }, {new: true}, (uErr, refUpdate) => {
                        // delete other after signup
                        //     pushNotify.send('Hurray!!', `${user.first_name} ${user.last_name} has signed up to Letsfarm using your referral code!`);
                        if(refUpdate) {
                            Referral.deleteMany({$and: [
                                {referree_email: user.email},
                                {has_joined: false}]})
                        } else {
                            Referral.create({
                                referral: referral._id,
                                referral_email: referral.email,
                                has_joined: true,
                                referree: user._id,
                                referree_email: user.email
                            }, () => {});
                        }
                    });
            }
        });
}
function generateJWT(user, secret, req, res, next) {
    try {
        const jsonUser = user; // .toJSON();
        jsonUser.secretToken = secret;
        jsonUser.accessKey = process.env.ACTION_KEY;
        const genToken = jwtService.jwt.signEncryptJWT(jsonUser);
        genToken.then(function (result) {
            sendTokenToUser(result, user, req, res);
        })
    } catch (error) {
        console.log('Error 3 ', error);
    }
}
function sendTokenToUser(userToken, user, req, res){
    respHandler.sendSuccess(res, 200, 'User authentication successful', {token: userToken, user: user});
}
function saveDeviceId(id, email) {
    DeviceId.create({deviceId: id, email}, (e, r) => {
        console.log('ERROR OR RESPONSE while creating DEVICE ID ', e, r);
    });
}
function detectUser(req, res, next) {
    const {email, password, deviceId} = req.body;
    try {
        loginUser(req, res, next, email, password, deviceId);
    } catch (err) {
        console.log('Error ', err);
        respHandler.sendError(res, 400, 'FAILURE', 'Error while authenticating user.');
    }
}


function loginUser(req, res, next, email, password, deviceId = null, auth_type = 'NO_AUTH', social_id = null) {
    console.log('LOGIN HERE ');
    if(deviceId) {
        saveDeviceId(deviceId, email);
    }
    let query =  Users.findOne({email: email});
    if(!email && auth_type === 'FACEBOOK' && social_id) {
        query = Users.findOne({social_id: social_id});
    }
    query.exec((err, user) => {
            if (err || !user){
                return respHandler.sendError(res, 400, 'FAILURE', 'User not found on the system');
            } else if(user && !user.is_verified) {
                respHandler.sendError(res, 401, 'USER_NOT_VERIFIED', 'User account not verified yet!');
            } else if (user && user.auth_type !== auth_type) {
                console.log('AUTH HEREEEEEEEEEEE ');
                respHandler.sendError(res, 400, 'FAILURE', 'Sign in using the correct social authentication type!');
            } else if(user){
                console.log('USER found HERE ');
                if(user.auth_type === 'NO_AUTH') {
                    bcrypt.compare(password, user.password, (err, response) => {
                        //  console.log(err, response);
                        if(!response) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Incorrect user password!', err);
                        } else {
                            //  console.log('Password is correct');
                            const jsonUser = user.toJSON();
                            let wallet = 0.0;
                            Wallet.findOne({userId: user._id}, (walletError, walletSuccess) => {
                                if(walletError || !walletSuccess) {
                                    wallet = 0.0;
                                } else {
                                    wallet = walletSuccess.wallet_balance || 0.0;
                                }
                                jsonUser.password = null;
                                jsonUser.wallet_balance = wallet;
                                delete jsonUser.password;
                                // console.log('USER ', jsonUser, walletSuccess);
                                proceedToGenerateToken(jsonUser, req, res, next);
                            });
                        }
                    });
                } else {
                    //  console.log('Password is correct');
                    const jsonUser = user.toJSON();
                    let wallet = 0.0;
                    Wallet.findOne({userId: user._id}, (walletError, walletSuccess) => {
                        if(walletError || !walletSuccess) {
                            wallet = 0.0;
                        } else {
                            wallet = walletSuccess.wallet_balance || 0.0;
                        }
                        jsonUser.wallet_balance = wallet;
                        console.log('WALLET SET ');
                        proceedToGenerateToken(jsonUser, req, res, next);
                    });
                }
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to authenticate user!');
            }
        });
}

function proceedToGenerateToken(user, req, res, next) {
    // get JWT implementation here
    try {
        const secretToken = nanoid() + '__-__' + nanoid(); // random string to be save to db as auth procedure
        saveSecretToken(secretToken, user, req, res, next);
    } catch (error) {
        console.log('Error Occurred: ', error);
    }

}

function saveSecretToken(token_, user, req, res, next) {
    try {
        console.log('Code is here', user, token_);
        const secretToken = {
            token: token_,
            role: user.role,
            userId: user._id
        };
        createNewToken(secretToken, user, token_, req, res, next);
    } catch (error) {
        console.log('Error 2____', error);
    }
    finally {
        // do nothing
    }
}

function createNewToken(secretToken, user, token_, req, res, next) {
//console.log('createNewToken createNewToken', secretToken);
    try {
        Tokens.create(secretToken, (err, token) => {
            //console.log('Token created', err, token);
            if (err) {
                // do nothing
                respHandler.sendError(res, 400, 'FAILURE', 'User authentication failed, please retry');
            }
            if (token) {
                //  console.log('Is it token');
                generateJWT(user, token_, req, res, next);
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'User authentication failed, please retry');
            }
        });
    } catch (error){
        console.log('Error creating new Token');
        respHandler.sendError(res, 400, 'FAILURE', 'User authentication failed, please retry');
    }
}

function systemLogout(req, res, next) {
    const logout = req.headers.authorization || {};
    // console.log('LOGOUT ', logout);
    user = jwtService.jwt.decodeJWT(logout);
    user.then((result) => {
        // console.log('RESULT ', result);
    logoutUser(req, res, next, result.data);
    }).catch((e) => {
        respHandler.sendError(res, 400, 'FAILURE', 'Unable to log user out', e);
    })

}

function logoutUser(req, res, next, logout) {
    try {
        console.log('Token ', logout.secretToken, logout.userId);
        Tokens.deleteMany( {$or: [{token: logout.secretToken}, {userId: logout.userId}]}, {multi: true}, (err, token) => {
            if(token) {
                respHandler.sendSuccess(res, 200, 'User logout successful', token);
            } else {
                respHandler.sendSuccess(res, 200, 'User logout successful with error 0', token);
            }
        });
    } catch (err){
        respHandler.sendError(res, 400, 'FAILURE', 'Unable to log user out');
    }
    finally {
        // DELETE ALL EXPIRED TOKENS IN THE SYSTEM
        Tokens.find({}, function (err, tokens) {
            if(tokens) {
                tokens.forEach((token) => {
                    if((moment(token.lastUsed)).isAfter(moment(token.expiredIn))) {
                        Tokens.findByIdAndRemove(token._id, (err, token) => {
                            console.log('Removed expired tokens');
                        });
                    }
                });
            }
        });
    }
}