const Bank = require('../models/Bank');
const User = require('../models/Users');
const PaymentProof = require('../models/BankPaymentProof');
const Transaction = require('../models/Transaction');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const paystackService = require('../services/paystackServices');
const mailer = require('../mailing/mailSystem');
const request = require('request');
const BankController = {
    getBanks: (req, res) => {
        try {
            paystackService.listBanks((error, body) => {
                const response = JSON.parse(body);
                if (error) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list banks.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Bank listed successfully', response.data);
                }
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list banks.', err);
        }
    },
    createBank: (req, res) => {
        try {



            const banks = [
                {
                    bank_name: "Access Bank",
                    bank_code: 36
                },
                {
                    bank_name: "Citibank",
                    bank_code: 19
                },
                {
                    bank_name: "Diamond Bank",
                    bank_code: 51
                },
                {
                    bank_name: "Ecobank",
                    bank_code: 40
                },
                {
                    bank_name: "Enterprise Bank Limited",
                    bank_code: 998
                },
                {
                    bank_name: "Fidelity Bank",
                    bank_code: 56
                },
                {
                    bank_name: "First City Monument Bank",
                    bank_code: 214
                },
                {
                    bank_name: "Guaranty Trust Bank",
                    bank_code: 58
                },
                {
                    bank_name: "Heritage Bank",
                    bank_code: 24
                },
                {
                    bank_name: "Keystone Bank",
                    bank_code: 81
                },
                {
                    bank_name: "Mainstreet Bank",
                    bank_code: 12
                },
                {
                    bank_name: "Access Bank(Diamond)",
                    bank_code: 36
                },
                {
                    bank_name: "Savannah Bank",
                    bank_code: 999
                },
                {
                    bank_name: "Polaris Bank",
                    bank_code: 62
                },
                {
                    bank_name: "Stanbic IBTC Bank",
                    bank_code: 221
                },
                {
                    bank_name: "Standard Chartered Bank",
                    bank_code: 68
                },
                {
                    bank_name: "Sterling Bank",
                    bank_code: 232
                },
                {
                    bank_name: "Union Bank",
                    bank_code: 26
                },
                {
                    bank_name: "United Bank for Africa",
                    bank_code: 27
                },
                {
                    bank_name: "Unity Bank",
                    bank_code: 215
                },
                {
                    bank_name: "Wema Bank",
                    bank_code: 29
                },
                {
                    bank_name: "Zenith Bank",
                    bank_code: 47
                },
                {
                    bank_name: "Spring Bank",
                    bank_code: 84
                },
                {
                    bank_name: "JAIZ Bank",
                    bank_code: 301
                },
                {
                    bank_name: "PROVIDOUS Bank",
                    bank_code: 101
                },
                {
                    bank_name: "CBN",
                    bank_code: 1
                }
            ];
            Bank.create(banks, (error, bank) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new bank.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Bank added successfully', bank);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add new bank.', err);
        }
    },
    saveProof: (req, res) => {
        try {
            console.log({BODY: req.body});
            const { userId, transactionId, transactionRef, message, bank_name, account_name, amount, documents } =  req.body;
            if(!documents.length || !documents[0].url.includes('https://firebasestorage.googleapis.com')) {
                respHandler.sendError(res, 400, 'FAILURE', 'No document proof provided!.', {});
            } else {
                PaymentProof.create({userId, transactionId, transactionRef, message, bank_name, account_name, amount, documents},
                    (error, paymentProof) => {
                    if(error) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to save payment proof.', error);
                    } else {
                        console.log('{AYMENT PROOF');
                        Transaction.findByIdAndUpdate(transactionId, {$set: {payment_proof: paymentProof._id}}, {new: true}, () => {
                            User.find({role: 'SUPER'}, (e, users) => {
                                let recipients = [];
                                for(const user of users) {
                                    recipients.push({
                                        "Email": user.email,
                                        "Name": user.full_name
                                    });
                                }
                                mailer.sendHtmlManyTo(recipients,
                                    'New bank payment alert', 'BankPaymentConfirm', {full_name: `Letsfarm Admin`,
                                        title: 'Action required: Bank Payment Alert'});
                            });
                            // controllerService.sendToAdmins()
                            respHandler.sendSuccess(res, 200, 'Payment proof added successfully', paymentProof);
                        });
                    }
                })
            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to add payment proof.', err);
        }
    },
    getOldPaymentProofs: (req, res) => {
        try {
            const { page, limit, role } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 1950;
            PaymentProof.find({is_confirmed: true})
                .populate('transactionId')
                .populate('userId', 'first_name last_name full_name email')
                .skip((currentLimit * currentPage) - currentLimit)
                .sort("-createdAt")
                .limit(currentLimit)
                .exec((err, paymentProofs) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
                    } else if (validate.resultFound(paymentProofs, res)) {
                        const data = validate.formatData(paymentProofs);
                        respHandler.sendSuccess(res, 200, 'Payment proofs listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
        }
    },
    getPaymentProofs: (req, res) => {
        try {
            const { page, limit, role } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 1950;
            PaymentProof.find({is_confirmed: false})
                .populate('transactionId')
                .populate('userId', 'first_name last_name full_name email')
                .skip((currentLimit * currentPage) - currentLimit)
                .sort("-createdAt")
                .limit(currentLimit)
                .exec((err, paymentProofs) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
                    } else if (validate.resultFound(paymentProofs, res)) {
                        const data = validate.formatData(paymentProofs);
                        respHandler.sendSuccess(res, 200, 'Payment proofs listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list payment proofs.', err);
        }
    },
};

module.exports = BankController;

/*
function listBanks(cb) {
    const options = {
        url : 'https://api.paystack.co/bank',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }
    };
    const callback = (error, response, body) => {
        return cb(error, body, response.statusCode);
    };
    request.get(options, callback);
}*/
