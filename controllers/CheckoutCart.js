const PaymentChannel = require('../models/PaymentChannel');
const Cart = require('../models/Cart');
const Order = require('../models/Order');
const ProductOrder = require('../models/ProductOrder');
const Sponsorship = require('../models/Sponsorship');
const PaystackReference = require('../models/PaystackReference');
const BankPaymentProof = require('../models/BankPaymentProof');
const User = require('../models/Users');
const WalletTransaction = require('../models/WalletTransaction');
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const FarmShop = require('../models/FarmShop');
const FaultyTransaction = require('../models/FaultyTransaction');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const eachAsync = require('each-async');
const controllerService = require('../services/controllerServices');
const nanoid = require('nanoid');
const request = require('request');
const winston = require('../config/winston');
const crypto = require('crypto');
const moment = require('moment');
const mailer = require('../mailing/mailSystem');

const CheckoutCartController = {
    createCheckoutCart: (req, res) => {
        try {
             const {payment_gateway} = req.body;
             controllerService.getLoginUser(req, 'userId', (userId) => {
                 console.log('USER ', userId, payment_gateway);
                 if(!payment_gateway) {
                     respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout', {});
                 } else {
                     PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                         if(gatewayError) {
                             respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                         } else if(!paymentGateway.is_active) {
                             respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                         } else {
                             Cart.find({$and: [{userId: userId}, {checkout_status: false}, {cart_type: 'FARM_SHOP'}]})
                                 .populate('userId', 'first_name last_name email full_name phone_number')
                                 .populate({
                                 path: "farmId",
                                 populate: { path: "farm_status" }
                             }).exec((cartError, cartData) => {
                                 if(cartError || !cartData.length) {
                                     respHandler.sendError(res, 401, 'FAILURE', 'Unable to fetch cart Items!', {});
                                 } else {
                                     let cartConcluded = [];
                                     eachAsync(cartData, (cart, i, done) => {
                                         console.log('CART DATA ', cart.farmId.farm_status);
                                         if(!cart.farmId.farm_status.can_sponsor || !cart.farmId.farm_status.status
                                             || cart.farmId.remaining_in_stock <= 0 || cart.quantity > cart.farmId.remaining_in_stock) {
                                             done("One of the Items in your cart cannot be sponsored, please consider removing it!");
                                         } else {
                                             cartConcluded.push(cart);
                                             done();
                                         }
                                     }, (response) => {
                                         if(response) {
                                             respHandler.sendError(res, 401, 'FAILURE', response, {});
                                         } else {
                                             CheckoutCartController.proceedToPlaceOrder(cartConcluded, paymentGateway, req, res);
                                         }
                                     });
                                 }
                             })
                         }
                     });
                 }
             });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to checkout cart.', err);
        }
    },
    checkoutWithExitingCard: (req, res) => {
        try {
             const { payment_gateway, userId, card_id } = req.body;
             controllerService.getLoginUser(req, 'userId', (_userId) => {
                 if(userId !== _userId) {
                     respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout, invalid credentials', {});
                 } else if(!payment_gateway) {
                     respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout', {});
                 } else {
                     PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                         if(gatewayError) {
                             respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                         } else if(!paymentGateway.is_active) {
                             respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                         } else if(paymentGateway.name.toLowerCase() !== 'paystack') {
                             respHandler.sendError(res, 401, 'FAILURE', 'Only paystack is allowed!', {});
                         } else if(!card_id) {
                             respHandler.sendError(res, 401, 'FAILURE', 'Card Id not provided!', {});
                         } else {
                             const date = new Date();
                             const year = date.getFullYear();
                             // const month = date.getMonth() + 1;
                             PaystackReference.findOne({$and: [{_id: card_id}, {reusable: true}, {exp_year: {$gte: year}}]},
                                 (cardErr, cardSuc) => {
                                     if (cardErr || !cardSuc) {
                                         respHandler.sendError(res, 401, 'FAILURE', 'Invalid payment card selected!', cardErr);
                                     } else {
                                         Cart.find({$and: [{userId: userId}, {checkout_status: false}, {cart_type: 'FARM_SHOP'}]})
                                             .populate('userId', 'first_name last_name email full_name phone_number')
                                             .populate({
                                                 path: "farmId",
                                                 populate: { path: "farm_status" }
                                             }).exec((cartError, cartData) => {
                                             if(cartError || !cartData.length) {
                                                 respHandler.sendError(res, 401, 'FAILURE', 'Unable to fetch cart Items!', {});
                                             } else {
                                                 let cartConcluded = [];
                                                 eachAsync(cartData, (cart, i, done) => {
                                                     console.log('CART DATA ', cart.farmId.farm_status);
                                                     if(!cart.farmId.farm_status.can_sponsor || !cart.farmId.farm_status.status
                                                         || cart.farmId.remaining_in_stock <= 0 || cart.quantity > cart.farmId.remaining_in_stock) {
                                                         done("One of the Items in your cart cannot be sponsored, please consider removing it!");
                                                     } else {
                                                         cartConcluded.push(cart);
                                                         done();
                                                     }
                                                 }, (response) => {
                                                     if(response) {
                                                         respHandler.sendError(res, 401, 'FAILURE', response, {});
                                                     } else {
                                                         CheckoutCartController.proceedToPlaceOrder(cartConcluded, paymentGateway, req, res);
                                                     }
                                                 });
                                             }
                                         })

                                     }
                                 });
                         }
                     });
                 }
             });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to checkout cart.', err);
        }
    },
    proceedToPlaceOrder: (carts, paymentGateway, req, res) => {
        const generateRef = `${controllerService.getRandomInt(9999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`;
        eachAsync(carts, (cart, i, done) => {
            console.log('Cart ', cart);
            let remainingItem = parseInt(cart.farmId.remaining_in_stock, 10);
            let cartQty = parseInt(cart.quantity, 10);
            let total_price = cartQty * parseFloat(cart.farmId.amount_to_invest);
            let price_per_unit = parseFloat(cart.farmId.amount_to_invest);
            let roi = parseInt(cart.farmId.percentage_to_gain, 10);
            const duration_type = cart.farmId.duration_type;
            const duration = cart.farmId.duration;
            const farm_name = cart.farmId.farm_name;
            const currency = cart.farmId.currency;
            const cartId = cart._id;
            const farmId = cart.farmId;
            const userId = cart.userId._id;
            const itemRemain = remainingItem - cartQty;
            Order.create({userId, farmId, cartId, farm_name, currency, duration, orderReference: generateRef,
                duration_type, roi, total_price, price_per_unit,
                quantity: cartQty,
                payment_channel: paymentGateway._id,
                qty_remaining_before_order: remainingItem }, (orderErr, orderSuc) => {
                if(orderErr) {
                    done('Unable to place order at the moment');
                } else {
                    if(orderSuc) {
                        done();
                    } else {
                        done('Unable to place order at the moment-2');
                    }
                }
            });
        }, (response) => {
            if(response) {
                respHandler.sendError(res, 401, 'FAILURE', response, {});
            } else {
                CheckoutCartController.proceedToPaymentInit(req, res, generateRef, paymentGateway);
            }
        });
    },
    proceedToPaymentInit: (req, res, orderRef, paymentChannel) => {
        let total_invoice = 0.0;
        let userId = '';
        let narrate = '';
        Order.find({orderReference: orderRef}, (error, orders) => {
           if(orders.length === 1) {
               narrate = `Transaction for ${orders[0].farm_name} via ${paymentChannel.name}`;
           } else {
               narrate = `Transaction for ${orders.length} farm shop(s) via ${paymentChannel.name}`;
           }
            eachAsync(orders, (order, i, done) => {
                total_invoice += parseFloat(order.total_price);
                userId = order.userId;
                done();
            }, () => {
                Transaction.create({orderReference: orderRef,
                    total_price: total_invoice,
                    payment_channel: paymentChannel._id,
                    userId: userId, narration: narrate
                } , (err, transaction) => {
                    if(err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to generate payment link', err);
                    } else if(transaction) {
                        // todo: decide on payment URL
                        // switch channel
                        CheckoutCartController.decideOnPayment(req, res, transaction, paymentChannel);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to save transaction record', err);
                    }
                });
            })
        });
    },
    decideOnPayment: (req, res, transaction, paymentChannel) => {
        const { card_id } = req.body;
        switch (paymentChannel.name.toLowerCase()) {
            case 'paystack': {
                if(card_id) {
                    useAutoCharge(req, res, transaction, paymentChannel)
                } else {
                    usePayStack(req, res, transaction, paymentChannel);
                }
                break;
            }
            case 'bank transfer': {
                transferToBankAccount(req, res, transaction, paymentChannel);
                break;
            }
            case 'e-wallet': {
                useWallet(req, res, transaction, paymentChannel);
                break;
            }
            case 'flutterwave': {
                useFlutter(req, res, transaction, paymentChannel);
                break;
            }
            default: {
                usePayStack(req, res, transaction, paymentChannel);
                break;
            }
        }
    },
    verifyPayment: (req, res) => {
        try {
            const transId = req.body.transactionId;
            if(transId) {
                Transaction.findById(transId, (err, transaction) => {
                    if(err) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Transaction does not exist on our system!', err);
                    } else {
                        verifyPayment(transaction.paymentReference, (erro, body, statusCode) => {
                            if(erro) {
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', erro);
                            }  else if(statusCode !== 200){
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', erro);
                            } else {
                                const response = JSON.parse(body);
                                if(response.data.status === 'success') {
                                    const authorization = response.data.authorization;
                                    // console.log('RESPONSIE ', response);
                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {
                                        if(err) {
                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                        }
                                        // todo: Start the SPONSORSHIP
                                        CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                                            PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                respHandler.sendSuccess(res, 200, response.message + ', sponsorship started!', {
                                                    body: response,
                                                    sponsorship
                                                });
                                            });
                                        });
                                    });
                                } else {
                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {paystack_status: response.data.status}}, {new: true}, (err, update) => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', response);

                                }
                            }
                        });
                    }
                });
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', {});
            }
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', error);
        }
    },
    startSponsorship: (req, res, transactionId, cb) => {
        Transaction.findById(transactionId, (errTr, transaction) => {
            if(errTr) {
                winston.logger.log('error', `Error: Unable to fetch transaction by ID`);
            } else {
                console.log('TRANSACTION => ', transaction);
                Order.find({orderReference: transaction.orderReference})
                    // .populate('farmId')
                    .populate({
                        path: "farmId",
                        populate: { path: "farm_type" }
                    })
                    .exec((orderError, orders) => {
                    if(orderError) {
                        winston.logger.log('error', `Error: Unable to list order for this transaction ---- ${transaction}`);
                    } else {
                        const sponsors = [];
                        eachAsync(orders, (order, i, done) => {
                            let remainingItem = parseInt(order.farmId.remaining_in_stock, 10);
                            let orderQty = parseInt(order.quantity, 10);
                            const itemRemain = remainingItem - orderQty;
                            console.log('ORDER ', order);
                            if(itemRemain < 0) {
                                done('Out of stock stop sponsorship' + order);
                            } else {
                                // todo: if farm type name matches food trading
                                // todo: calculate all roi into array
                                // todo: set advanceType to advance
                                // todo: on dashboard use advance type to filter// simmple
                                let advanceType = 'default';
                                let expectedReturn = parseFloat(order.total_price || 0) + ((parseInt(order.roi || 0, 10) * parseFloat(order.total_price || 0)) / 100);
                                let roiData = [];
                                let _total = 0;
                                const roi_interval = order.farmId.roi_interval;
                                const duration = parseInt(order.duration, 10) * 12;
                                let interval_count = parseInt((duration / roi_interval).toFixed(0), 10);
                                // const interval = parseInt((duration / roi_interval).toFixed(0), 10);
                                const now_ = moment();
                                if(order.farmId.farm_type.name.toLowerCase() === 'food trading'
                                    || order.farmId.farm_type.name.toLowerCase() === 'food stuff trading') {
                                    advanceType = 'advance';
                                    while(interval_count > 0 ){
                                        const int_duration = parseInt(interval_count, 10) * parseInt(roi_interval, 10);
                                        const roi = ((parseInt(order.roi || 0, 10) * parseFloat(order.total_price || 0)) / 100);
                                        const endDate = moment().add(int_duration, 'months').add(4, 'days');
                                        const roi_data = {
                                            roi: roi,
                                            status: 'STARTED',
                                            out_date: endDate
                                        };
                                        roiData.push(roi_data);
                                        interval_count -= 1;
                                        _total += roi;
                                    }
                                    const _expectedReturn = parseFloat(order.total_price || 0) + ((parseInt(order.roi || 0, 10) * parseFloat(order.total_price || 0)) / 100);
                                    roiData[0].roi = _expectedReturn;
                                    expectedReturn = parseFloat(_total) + parseFloat(order.total_price || 0);
                                    roiData = roiData.reverse();
                                }
                                sponsors.push({
                                    farmId: order.farmId,
                                    itemRemaining: itemRemain,
                                    orderId: order._id,
                                    roi_interval,
                                    advanceType,
                                    roi_data: roiData,
                                    userId: order.userId,
                                    expected_return: expectedReturn,
                                    start_date: moment.now(),
                                    expected_end_date: moment().add(order.duration, order.duration_type.toLowerCase()).add(7, 'days')
                                });
                                done();
                            }
                        }, (_response) => {
                            if(_response) {
                                controllerService.sendToAdmins('A user paid to sponsor a farm late, system could not start the sponsorship  because the item is out of stock - Here is the transaction ID ' + transactionId, 'Action required: Faulty sponsor', 'Sponsorship Issue')
                                FaultyTransaction.create({
                                    transactionId: transactionId,
                                    orderReference: transaction.orderReference,
                                    narration: 'User paid to sponsor a farm late, system could not start the sponsorship' +
                                    ' because the item is out of stock - Here is the transaction ID ' + transactionId,
                                    expected_start_date: moment.now(),
                                    expected_end_date: moment().add(order.duration, order.duration_type.toLowerCase()).add(7, 'days'),
                                    userId: order.userId}, () => {
                                    respHandler.sendError(res, 400, 'FAILURE', 'We can not start your sponsorship even though you have paid', {transactionId});
                                });
                            } else {
                                eachAsync(sponsors, (sponsor, i, done) => {
                                    FarmShop.findByIdAndUpdate(sponsor.farmId, {$set: {remaining_in_stock: sponsor.itemRemaining}}, {new: true}, () => {
                                        done();
                                    });
                                }, () => {
                                    Sponsorship.create(sponsors,  (err, sponsorships) => {
                                        if(err) {
                                            winston.logger.log('error', `Error: Unable to add farm to sponsorShip table ---- ${err}`);
                                            respHandler.sendError(res, 400, 'FAILURE', 'Unable to start sponsorship', err);
                                        } else {
                                            eachAsync(sponsorships, (sponsor, i, done) => {
                                                Sponsorship.findById(sponsor._id)
                                                    .populate('farmId', 'farm_name')
                                                    .populate('userId', 'email first_name last_name full_name phone_number')
                                                    .populate('orderId', 'roi duration quantity currency price_per_unit duration_type total_price')
                                                    .exec((ee, sponsored) => {
                                                    if(sponsored) {
                                                        const roi = sponsored.orderId.roi;
                                                        const quantity = sponsored.orderId.quantity;
                                                        const price_per_unit = sponsored.orderId.price_per_unit;
                                                        const currency = sponsored.orderId.currency;
                                                        const farm_name = sponsored.farmId.farm_name;
                                                        const duration = sponsored.orderId.duration;
                                                        const duration_type = sponsored.orderId.duration_type;
                                                        const total_return = sponsored.expected_return;
                                                        const total_paid = sponsored.orderId.total_price;
                                                        const interest = sponsored.expected_return - sponsored.orderId.total_price;
                                                        const end_date = moment(sponsored.expected_end_date).format('DD-MM-YYYY');
                                                        const start_date = moment(sponsored.start_date).format('DD-MM-YYYY');
                                                        console.log('SPONSORSHIP EMAIL ');
                                                        if(sponsored.advanceType === 'advance') {
                                                            const roi_interval = sponsored.roi_interval;
                                                            const interest_per_roi = sponsored.roi_data[0].roi;
                                                            const finalReturn = sponsored.roi_data[sponsored.roi_data.length - 1].roi;
                                                            mailer.sendHtml({email: sponsored.userId.email, full_name: sponsored.userId.full_name},
                                                                farm_name + ' Sponsorship Details', 'FoodTradingSponsor', {
                                                                    full_name: sponsored.userId.full_name, title: farm_name + ' Sponsorship Notification', quantity,
                                                                    farm_name: farm_name, roi, duration, duration_type,
                                                                    interval: roi_interval,
                                                                    interest_per_roi: interest_per_roi,
                                                                    finalReturn: finalReturn,
                                                                    total_return, total_paid, price_per_unit, currency, interest, end_date, start_date });
                                                        } else {
                                                            mailer.sendHtml({email: sponsored.userId.email, full_name: sponsored.userId.full_name},
                                                                farm_name + ' Sponsorship Details', 'SponsorShip', {
                                                                    full_name: sponsored.userId.full_name, title: farm_name + ' Sponsorship Notification', quantity,
                                                                    farm_name: farm_name, roi, duration, duration_type,
                                                                    total_return, total_paid, price_per_unit, currency, interest, end_date, start_date });
                                                        }

                                                        winston.logger.log('info', `Sponsorship Mail sent - --- ${sponsored.userId.email}`);
                                                    }
                                                    done();
                                                })
                                            }, () => {});
                                            winston.logger.log('info', `Sponsorship started - --- ${sponsorships}`);
                                            cb(sponsorships);
                                        }
                                    });
                                });
                            }

                        });
                    }
                });
            }
        });
    },
    paystackWebhook: (req, res) => {
        //validate event
        try {
            console.log('PAYSTACK WEBHOOK ', req.body);
            const hash = crypto.createHmac('sha512', process.env.PAYSTACK_SKT).update(JSON.stringify(req.body)).digest('hex');
            console.log('PAYSTACK WEBHOOK HASH', hash);
            if (hash === req.headers['x-paystack-signature']) {
                // Retrieve the request's body
                const event = JSON.parse(req.body);
                console.log('PAYSTACK WEBHOOK EVENT', event);
                const transId = event.metadata.orderReference;
                if(transId) {
                    Transaction.findOne({orderReference: transId}, (err, transaction) => {
                        if(err) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Transaction does not exist on our system!', err);
                        } else {
                            verifyPayment(transaction.paymentReference, (erro, body, statusCode) => {
                                if(erro) {
                                    // Transaction.findOneAndUpdate({orderReference: transId}, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', erro);
                                } else if(statusCode !== 200){
                                    // Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', erro);
                                } else {
                                    const response = JSON.parse(body);
                                    if(response.data.status === "success") {
                                        const authorization = response.data.authorization;
                                        // console.log('RESPONSIE ', response);
                                        Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED', paystack_status: response.data.status}}, {new: true}, (err, update) => {
                                            if(err) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                            }
                                            if(transaction.transactionType === 'WALLET'){
                                                const authorization = response.data.authorization;
                                                approveWalletFunding(res, transaction, (walletTransaction) => {
                                                    PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                        respHandler.sendSuccess(res, 200, response.message + ', wallet credited!', {
                                                            body: response,
                                                            walletTransaction
                                                        });
                                                    });
                                                });
                                            } else if(transaction.transactionType === 'FARM_PRODUCT'){
                                                const authorization = response.data.authorization;
                                                ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PAID'}}, {multi: true}, () => {
                                                    PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                        respHandler.sendSuccess(res, 200, 'Product order successful, payment confirmed', {
                                                            body: response
                                                        });
                                                    });
                                                });
                                            } else {
                                                CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                                                    PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                        respHandler.sendSuccess(res, 200, response.message + ', sponsorship started!', {
                                                            body: response,
                                                            sponsorship
                                                        });
                                                    });
                                                });
                                            }
                                        });

                                    } else {
                                        Transaction.findByIdAndUpdate(transaction._id, {$set: {paystack_status: response.data.status}}, {new: true}, (err, update) => {});
                                        respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', response);
                                    }

                                }
                            });
                        }
                    });
                } else if(event.transfer_code.includes('TRF_')) {
                    console.log('THIS IS A TRANSFER WEBHOOK RESPONSE', event);
                } else {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', {});
                }
                respHandler.sendSuccess(res, 200, 'Webhook event received successfully!', {});
            }
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', error);
        }
    },
    paystackConfirmedTransactionManually: (req, res) => {
        try {
                const transId = req.body.transactionId;
                if(transId) {
                    Transaction.findOne({$and: [{_id: transId}, {status: 'INITIATED'}]},
                        (err, transaction) => {
                        if(err || !transaction) {
                            respHandler.sendError(res, 404, 'FAILURE', 'Transaction does not exist on our system!', err);
                        } else {
                            verifyPayment(transaction.paymentReference, (erro, body, statusCode) => {
                                // console.log(erro, body, statusCode);
                                if(erro) {
                                    // Transaction.findOneAndUpdate({orderReference: transId}, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', erro);
                                } else if(statusCode !== 200){
                                    // Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', erro);
                                } else {
                                    const response = JSON.parse(body);

                                    if(response.data.status === "success") {
                                        const authorization = response.data.authorization;
                                        // console.log('RESPONSIE ', response);
                                        Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED', paystack_status: response.data.status}}, {new: true}, (err, update) => {
                                            if(err) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                            }
                                            if(transaction.transactionType === 'WALLET'){
                                                const authorization = response.data.authorization;
                                                approveWalletFunding(res, transaction, (walletTransaction) => {
                                                    PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                        respHandler.sendSuccess(res, 200, response.message + ', wallet credited!', {
                                                            body: response,
                                                            walletTransaction
                                                        });
                                                    });
                                                });
                                            } else {
                                                CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                                                    PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                        respHandler.sendSuccess(res, 200, response.message + ', sponsorship started!', {
                                                            body: response,
                                                            sponsorship
                                                        });
                                                    });
                                                });
                                            }
                                        });

                                    } else {
                                        Transaction.findByIdAndUpdate(transaction._id, {$set: {paystack_status: response.data.status}}, {new: true}, (err, update) => {});
                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', response);
                                    }
                                }
                            });
                        }
                    });
                } else {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', {});
                }
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', error);
        }
    },
    confirmBankTransfer: (req, res) => {
        const { transactionId } = req.body;
        Transaction.findByIdAndUpdate(transactionId, {$set: {status: 'CONFIRMED'}}, {new: true}, (error, transaction) => {
            if(error) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to confirm transaction', error);
            } else {
                if(transaction.transactionType === 'FARM_PRODUCT'){
                    ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PAID'}}, {multi: true}, () => {
                        BankPaymentProof.findOneAndUpdate({transactionId: transaction._id}, {$set: { is_confirmed: true }}, {new: true}, (err, bankProofSus) => {});
                        respHandler.sendSuccess(res, 200, 'Transaction confirmed successfully!', {transaction});
                    });
                } else {
                    CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                        BankPaymentProof.findOneAndUpdate({transactionId: transaction._id}, {$set: { is_confirmed: true }}, {new: true}, (err, bankProofSus) => {});
                        respHandler.sendSuccess(res, 200, 'Transaction confirmed successfully!', {transaction, sponsorship});
                    });
                }
            }
        })
    },
    calculateCharge: (req, res) => {
        const amount = req.body.amount;
        controllerService.calculateTransactionCharge(amount, (charge, subTotal, amountToPay) => {
            respHandler.sendSuccess(res, 200, 'Transaction charge calculated successfully!', {
                charge, total: amountToPay, subTotal
            })
        });

        }
};

module.exports = CheckoutCartController;

function usePayStack(req, res, transaction, paymentChannel) {
    User.findById(transaction.userId, (err, user) => {
        if(err) {
         // do nothing
            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
        } else {
            controllerService.calculateTransactionCharge(transaction.total_price, (charge, subTotal, amountToPay) => {
                initializePayment(
                    {email: user.email, full_name: user.full_name,
                        first_name: user.first_name, last_name: user.last_name,
                        amount: amountToPay,
                        transaction: transaction.toJSON()}, (error, body, statusCode) => {
                        if(error){
                            //handle errors
                            console.log('PAYSTACK ERROR', error);
                            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', error);
                        } else {
                            const response = JSON.parse(body);
                            if(response.status === false) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                            } else if(statusCode !== 200) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                            } else {
                                Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: response.data.reference}}, {new: true},
                                    (errorTransaction, sucTransaction) => {
                                        if(errorTransaction) {
                                            // log error
                                            winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                        } else {
                                            Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_SHOP'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {});
                                            respHandler.sendSuccess(res, 200, 'Transaction successfully created!', {
                                                authorization_url: response.data.authorization_url,
                                                details: {
                                                    charge: charge,
                                                    subTotal: subTotal,
                                                    total: amountToPay,
                                                    transactionId: transaction._id,
                                                    full_name: user.full_name,
                                                    email: user.email,
                                                    ref: response.data.reference,
                                                    access_code: response.data.access_code
                                                }
                                            });
                                        }
                                    });
                            }
                        }
                    });
            });
        }
    });
}
function transferToBankAccount(req, res, transaction, paymentChannel) {
    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: null}}, {new: true},
        (errorTransaction, sucTransaction) => {
            if(errorTransaction) {
                // log error
                winston.logger.log('error', `Error: while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}--bank transfer`);
                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete checkout using bank transfer', errorTransaction);
            } else {
                Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_SHOP'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {});
                respHandler.sendSuccess(res, 200, 'Transaction initiated with bank transfer, proceed to payment!', {
                    transactionRef: transaction.orderReference,
                    transactionId: transaction._id,
                    amount: transaction.total_price
                });
            }
        });
}
function useWallet(req, res, transaction, paymentChannel) {
    Wallet.findOne({userId: transaction.userId}, (error, wallet) => {
        if(error || !wallet) {
           return respHandler.sendError(res, 401, 'FAILURE', 'Unable to process payment with e-Wallet', error);
        } else if(!wallet.wallet_balance || wallet.wallet_balance < transaction.total_price) {
           return respHandler.sendError(res, 401, 'FAILURE', 'Unable to process payment with e-Wallet, insufficient balance', error);
        } else {
            Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED', paymentReference: null}}, {new: true},
                (errorTransaction, sucTransaction) => {
                    if(errorTransaction) {
                        // log error
                        winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                        return respHandler.sendError(res, 400, 'FAILURE', 'Unable to update transaction status', errorTransaction);
                    } else {
                        const newBalance = parseFloat(wallet.wallet_balance) - parseFloat(transaction.total_price);
                        console.log('BALANCE ', newBalance, parseFloat(wallet.wallet_balance), parseFloat(transaction.total_price));
                        Wallet.findOneAndUpdate({userId: transaction.userId}, {$set: {wallet_balance: newBalance}},
                            {new: true}, (e, walletSuc) => {
                            if(e) {
                                winston.logger.log('error', `Error while updating wallet balance - ${JSON.stringify(e)} ---Wallet transaction`);
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet balance', e);
                            } else {
                                WalletTransaction.create({
                                    narration: 'Sponsor farm with wallet',
                                    type: 'DEBIT',
                                    new_wallet_balance: newBalance,
                                    old_wallet_balance: wallet.wallet_balance,
                                    amount: transaction.total_price,
                                    transactionId: transaction._id,
                                    userId: transaction.userId
                                }, (err_, walletTrans) => {
                                    if(err_) {
                                        console.log('Error ', err_);
                                        winston.logger.log('error', `Error: unable to update wallet transaction table - ${JSON.stringify(err_)} ---Wallet transaction table`);
                                       return respHandler.sendError(res, 400, 'FAILURE', 'Unable to save wallet transaction', err_);
                                    } else {
                                        // todo: Start the SPONSORSHIP
                                        Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_SHOP'}]}, {$set: {checkout_status: true}}, { multi: true }, () => {});
                                        CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                                            respHandler.sendSuccess(res, 200, 'Transaction successful with e-wallet!', {walletTrans, sponsorship});
                                        });
                                    }
                                });
                            }
                            });
                    }
                });
        }
    });
}
function useFlutter(req, res, transaction, paymentChannel) {
    respHandler.sendSuccess(res, 200, 'Flutter wave is not setup for payment, please consider using paystack', {});
}
function useAutoCharge(req, res, transaction, paymentChannel) {
    const card_id = req.body.card_id;
    User.findById(transaction.userId, (err, user) => {
        if(err || !user) {
            // do nothing
            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
        } else {
            const date = new Date();
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            PaystackReference.findOne({$and: [{_id: card_id}, {reusable: true}, {exp_year: {$gte: year}}, {exp_month: {$gte: month}}]},
                (cardErr, cardSuc) => {
                    if(cardErr || !cardSuc) {
                        respHandler.sendError(res, 401, 'FAILURE', 'Invalid payment card selected!', cardErr);
                    } else {
                        checkPaymentAuth({
                            email: user.email,
                            authorization_code: cardSuc.authorization_code,
                            amount: transaction.total_price
                        }, (error, body, statusCode) => {
                            const response = JSON.parse(body);
                            if(error){
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', error);
                            } else if(response.status === false) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                            } else if(statusCode !== 200) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                            } else {
                                controllerService.calculateTransactionCharge(transaction.total_price, (charge, subTotal, amountToPay) => {
                                    recurrentPayment({email: user.email, full_name: user.full_name,
                                        first_name: user.first_name, last_name: user.last_name,
                                        authorization_code: cardSuc.authorization_code,
                                        amount: amountToPay,
                                        transaction: transaction.toJSON()}, (_error, _body, _statusCode) => {
                                        if(_error){
                                            //handle errors
                                            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', _error);
                                        } else {
                                            const _response = JSON.parse(_body);
                                            if(_response.status === false) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', {response: _response});
                                            } else if(_statusCode !== 200) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', {response: _response});
                                            } else {
                                                Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: _response.data.reference}}, {new: true},
                                                    (errorTransaction, sucTransaction) => {
                                                        if(errorTransaction || !sucTransaction ) {
                                                            // log error
                                                            winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', errorTransaction);
                                                        } else {
                                                            Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_SHOP'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {});
                                                            verifyPayment(sucTransaction.paymentReference, (erro, __body, __statusCode) => {
                                                                if(erro) {
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', erro);
                                                                }  else if(__statusCode !== 200){
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction, please retry!', erro);
                                                                } else {
                                                                    const response = JSON.parse(__body);
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {
                                                                        if(err) {
                                                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                                                        } else {
                                                                            CheckoutCartController.startSponsorship(req, res, transaction._id, (sponsorship) => {
                                                                                respHandler.sendSuccess(res, 200, response.message + ', sponsorship started!', {
                                                                                    sponsorship
                                                                                });
                                                                            });
                                                                        }
                                                                    });

                                                                }
                                                            });

                                                        }
                                                    });
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });

        }
    });
}

function initializePayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/cart/${data.transaction._id}/farm_transaction`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/initialize',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        // console.log('Error ', error, response);
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function recurrentPayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        authorization_code: data.authorization_code,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/wallet-history/${data.transaction._id}`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/charge_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}
function checkPaymentAuth(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        authorization_code: data.authorization_code
    };
    const options = {
        url : 'https://api.paystack.co/transaction/check_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function verifyPayment(ref, cb) {
    const options = {
        url : 'https://api.paystack.co/transaction/verify/' + encodeURIComponent(ref),
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }
    };
    const callback = (error, response, body) => {
        return cb(error, body, response.statusCode);
    };
    request.get(options, callback);
}

function approveWalletFunding(res, transaction, cb=null) {
    Wallet.findOne({userId: transaction.userId}, (walletError, walletSuccess) => {
        if(walletError || !walletSuccess) {
            winston.logger.log('error', `Error: Unable to find wallet data ---- ${JSON.stringify(transaction)}`);
            respHandler.sendError(res, 401, 'FAILURE', 'Unable to credit wallet balance!', walletError);
        } else {
            let wallet_balance = parseFloat(walletSuccess.wallet_balance) || 0;
            let amountToInput  = parseFloat(transaction.total_price) || 0;
            let new_wallet_balance = wallet_balance + amountToInput;
            Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {});
            WalletTransaction.create({
                narration: transaction.narration,
                old_wallet_balance: wallet_balance,
                type: 'CREDIT',
                new_wallet_balance: new_wallet_balance,
                amount: amountToInput,
                userId: transaction.userId,
                transactionId: transaction._id
            }, (wtErr, walletTransaction) => {
                if(wtErr) {
                    winston.logger.log('error', `Error: Unable to save wallet-transaction record ---- ${JSON.stringify(wtErr)} --- ${JSON.stringify(walletTransaction)}`);
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to save wallet-transaction record!', wtErr);
                } else {
                    Wallet.findByIdAndUpdate(walletSuccess._id, {$set: {wallet_balance: new_wallet_balance, last_fund_date: moment.now()}}, {new: true}, (errUpdate, sucUpdate) => {
                        if(errUpdate) {
                            winston.logger.log('error', `Error: Unable to update wallet balance after funding ---- ${JSON.stringify(errUpdate)} --- ${JSON.stringify(walletSuccess)}`);
                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to update wallet balance after funding!', errUpdate);
                        } else {
                            if(cb) {
                                cb(walletTransaction);
                            } else {
                                respHandler.sendSuccess(res, 200, 'Transaction completed, wallet credited!')
                            }
                        }
                    });
                }
            });
        }
    })
}
