const Order = require('../models/Order');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const OrderController = {
    getOrders: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await Order.countDocuments({}).exec();
            Order.find({})
                .populate('payment_channel')
                .populate('userId')
                .populate('farmId')
                .exec((err, order) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list orders.', err);
                    } else if (validate.resultFound(order, res)) {
                        const data = {
                            data: validate.formatData(order),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Orders listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list orders.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list orders.', err);
        }
    },
    getUserOrders: (req, res) => {
        try {
            Order.find({userId: req.params.userId})
                .populate('payment_channel')
                .populate('farmId')
                .exec((err, orders) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user\'s orders.', err);
                    } else if (validate.resultFound(orders, res)) {
                        const data = validate.formatData(orders);
                        respHandler.sendSuccess(res, 200, 'User\'s orders fetched successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list orders.', err);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get orders.', err);
        }
    },

    getOrder: (req, res) => {
        try {
            Order.findById(req.params.id)
                .populate('userId')
                .populate('payment_channel')
                .populate('farmId')
                .exec((err, order) => {
                    if (err || !order){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get order by id.', err);
                    } else if (validate.resultFound(order, res)) {
                        const data = validate.formatData(order);
                        respHandler.sendSuccess(res, 200, 'Order fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get order.', err);
        }
    },
    getOrderByRef: (req, res) => {
        try {
            Order.findOne({orderReference: req.params.ref})
                .populate('userId')
                .populate('farmId')
                .exec((err, order) => {
                    if (err){
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get order by order reference.', err);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Order fetched successfully', order);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get orders.', err);
        }
    }
};

module.exports = OrderController;
