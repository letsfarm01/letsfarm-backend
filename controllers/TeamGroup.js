const TeamGroup = require('../models/TeamGroup');
const TeamMember = require('../models/TeamMember');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const TeamGroupController = {
    getTeamGroups: (req, res) => {
        try {
            TeamGroup.find({is_active: true})
                .sort({'sort_order': 1})
                .exec((err, teamGroups) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list team groups.', err);
                    } else if (validate.resultFound(teamGroups, res)) {
                        const data = validate.formatData(teamGroups);
                        respHandler.sendSuccess(res, 200, 'Team group listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list team groups.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list team groups.', err);
        }
    },
    createTeamGroup: (req, res) => {
        try {
            const {name, description, sort_order} = req.body;
            TeamGroup.create({name, description, sort_order}, (error, teamGroup) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create team group.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Team group created successfully', teamGroup);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create team group.', err);
        }
    },
    getTeamGroup: (req, res) => {
        try {
            TeamGroup.findById(req.params.id).exec((err, teamGroup) => {
                if (err || !teamGroup){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get team group by id.', err);
                } else if (validate.resultFound(teamGroup, res)) {
                    const data = validate.formatData(teamGroup);
                    respHandler.sendSuccess(res, 200, 'Team group fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get team group.', err);
        }
    },
    putTeamGroup: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const {is_active, description, name, sort_order} = req.body;
            TeamGroup.findByIdAndUpdate(req.params.id, {$set: {is_active, description, name, sort_order}}, { new: true }, (err, teamGroup) => {
                if (err || !teamGroup){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update team group.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Team group updated successfully!', teamGroup);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update team group');
        }
    },
    deleteTeamGroup: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                TeamMember.countDocuments({teamGroup: req.params.id}, (error, counts) => {
                    if(error || counts > 0) {
                        return respHandler.sendError(res, 401, 'FAILURE', 'Unable to delete team group, there might be members in that group.', error);
                    } else {
                        TeamGroup.delete({_id: req.params.id}, deletedBy, (err) => {
                            if (err){
                                return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete team group.', err);
                            }
                            respHandler.sendSuccess(res, 200, 'Team group deleted by id successfully!', {});
                        });
                    }
                })
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete team group by id');
        }
    },
};

module.exports = TeamGroupController;