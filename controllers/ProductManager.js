const PaymentChannel = require('../models/PaymentChannel');
const Cart = require('../models/Cart');
const Order = require('../models/Order');
const ProductOrder = require('../models/ProductOrder');
const Sponsorship = require('../models/Sponsorship');
const PaystackReference = require('../models/PaystackReference');
const BankPaymentProof = require('../models/BankPaymentProof');
const User = require('../models/Users');
const WalletTransaction = require('../models/WalletTransaction');
const Wallet = require('../models/Wallet');
const Transaction = require('../models/Transaction');
const FarmShop = require('../models/FarmShop');
const FaultyTransaction = require('../models/FaultyTransaction');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const eachAsync = require('each-async');
const controllerService = require('../services/controllerServices');
const nanoid = require('nanoid');
const request = require('request');
const winston = require('../config/winston');
const crypto = require('crypto');
const moment = require('moment');
const mailer = require('../mailing/mailSystem');

const ProductManagementController = {
    checkoutProductCart: (req, res) => {
        try {
            const {payment_gateway, shipping_address} = req.body;
            controllerService.getLoginUser(req, 'userId', (userId) => {
                console.log('USER ', userId, payment_gateway);
                if(!payment_gateway) {
                   return  respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout', {});
                } else if(!shipping_address) {
                   return  respHandler.sendError(res, 400, 'FAILURE', 'Customer shipping address is required', {});
                } else {
                    PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                        if(gatewayError) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                        } else if(!paymentGateway.is_active) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                        } else {
                            Cart.find({$and: [{userId: userId}, {checkout_status: false}, {cart_type: 'FARM_PRODUCT'}]})
                                .populate('userId', 'first_name last_name email full_name phone_number')
                                .populate({
                                    path: "productId",
                                    populate: { path: "product_status" }
                                }).exec((cartError, cartData) => {
                                if(cartError || !cartData.length) {
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to fetch cart Items!', {});
                                } else {
                                    // todo: extract farm_product to another METHOD
                                    // let cartConcluded = [];
                                    let farmProductCartConcluded = [];
                                    eachAsync(cartData, (cart, i, done) => {
                                        console.log('CART DATA ', cart.productId.product_status);
                                        if(!cart.productId.product_status.status
                                            || cart.productId.remaining_in_stock <= 0 || cart.quantity > cart.productId.remaining_in_stock) {
                                            done("One of the Items in your cart cannot be sponsored, please consider removing it!");
                                        } else {
                                            farmProductCartConcluded.push(cart);
                                            done();
                                        }
                                    }, (response) => {
                                        if(response) {
                                            respHandler.sendError(res, 401, 'FAILURE', response, {});
                                        } else {
                                            if(farmProductCartConcluded.length) {
                                                ProductManagementController.proceedToPlaceProductOrder(farmProductCartConcluded, paymentGateway, shipping_address, req, res);
                                            } else {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Invalid cart information, cart is empty', {});
                                            }
                                        }
                                    });
                                }
                            })
                        }
                    });
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to checkout cart.', err);
        }
    },
    checkoutWithExitingCard: (req, res) => {
        try {
            const { payment_gateway, userId, card_id, shipping_address } = req.body;
            controllerService.getLoginUser(req, 'userId', (_userId) => {
                if(userId !== _userId) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout, invalid credentials', {});
                } else if(!payment_gateway) {
                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to proceed with checkout', {});
                } else {
                    PaymentChannel.findById(payment_gateway, (gatewayError, paymentGateway) => {
                        if(gatewayError) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to determine payment gateway', {});
                        } else if(!paymentGateway.is_active) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Payment gateway is not allow!', {});
                        } else if(paymentGateway.name.toLowerCase() !== 'paystack') {
                            respHandler.sendError(res, 401, 'FAILURE', 'Only paystack is allowed!', {});
                        } else if(!card_id) {
                            respHandler.sendError(res, 401, 'FAILURE', 'Card Id not provided!', {});
                        } else {
                            const date = new Date();
                            const year = date.getFullYear();
                            // const month = date.getMonth() + 1;
                            PaystackReference.findOne({$and: [{_id: card_id}, {reusable: true}, {exp_year: {$gte: year}}]},
                                (cardErr, cardSuc) => {
                                    if (cardErr || !cardSuc) {
                                        respHandler.sendError(res, 401, 'FAILURE', 'Invalid payment card selected!', cardErr);
                                    } else {

                                        Cart.find({$and: [{userId: userId}, {checkout_status: false}, {cart_type: 'FARM_PRODUCT'}]})
                                            .populate('userId', 'first_name last_name email full_name phone_number')
                                            .populate({
                                                path: "productId",
                                                populate: { path: "product_status" }
                                            }).exec((cartError, cartData) => {
                                            if(cartError || !cartData.length) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to fetch cart Items!', {});
                                            } else {
                                                // todo: extract farm_product to another METHOD
                                                // let cartConcluded = [];
                                                let farmProductCartConcluded = [];
                                                eachAsync(cartData, (cart, i, done) => {
                                                    console.log('CART DATA ', cart.productId.product_status);
                                                    if(!cart.productId.product_status.status
                                                        || cart.productId.remaining_in_stock <= 0 || cart.quantity > cart.productId.remaining_in_stock) {
                                                        done("One of the Items in your cart cannot be sponsored, please consider removing it!");
                                                    } else {
                                                        farmProductCartConcluded.push(cart);
                                                        done();
                                                    }
                                                }, (response) => {
                                                    if(response) {
                                                        respHandler.sendError(res, 401, 'FAILURE', response, {});
                                                    } else {
                                                        if(farmProductCartConcluded.length) {
                                                            ProductManagementController.proceedToPlaceProductOrder(farmProductCartConcluded, paymentGateway, shipping_address, req, res);
                                                        } else {
                                                            respHandler.sendError(res, 401, 'FAILURE', 'Invalid cart information, cart is empty', {});
                                                        }
                                                    }
                                                });
                                            }
                                        })

                                    }
                                });
                        }
                    });
                }
            });
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to checkout cart.', err);
        }
    },

    proceedToPlaceProductOrder: (carts, paymentGateway, shipping_address, req, res) => {
        const generateRef = `P-${controllerService.getRandomInt(9999999)}-${nanoid(6)}-${controllerService.getRandomInt(999)}`;
        eachAsync(carts, (cart, i, done) => {
            console.log('Cart ', cart);
            let cartType = cart.cart_type;
            let remainingItem = parseInt(cart.productId.remaining_in_stock, 10);
            let cartQty = parseInt(cart.quantity, 10);
            let total_price = cartQty * parseFloat(cart.productId.price);
            let price_per_unit = parseFloat(cart.productId.price);
            // let roi = parseInt(cart.farmId.percentage_to_gain, 10);
            // const duration_type = cart.farmId.duration_type;
            // const duration = cart.farmId.duration;
            const product_name = cart.productId.product_name;
            const currency = cart.productId.currency;
            const cartId = cart._id;
            const productId = cart.productId;
            const userId = cart.userId._id;
            const itemRemain = remainingItem - cartQty;
            ProductOrder.create({
                userId,
                productId,
                cartId,
                product_name,
                currency,
                shipping_address,
                cartType,
                orderReference: generateRef,
                total_price,
                price_per_unit,
                quantity: cartQty,
                payment_channel: paymentGateway._id,
                qty_remaining_before_order: remainingItem
            }, (orderErr, orderSuc) => {
                if(orderErr) {
                    done('Unable to place order at the moment');
                } else {
                    if(orderSuc) {
                        done();
                    } else {
                        done('Unable to place order at the moment');
                    }
                }
            });
        }, (response) => {
            if(response) {
                respHandler.sendError(res, 401, 'FAILURE', response, {});
            } else {
                ProductManagementController.proceedToPaymentInit(req, res, generateRef, paymentGateway);
            }
        });
    },
    proceedToPaymentInit: (req, res, orderRef, paymentChannel) => {
        // todo: if cartype === farm product...create different NARRATE
        let total_invoice = 0.0;
        let userId = '';
        let narrate = '';
        ProductOrder.find({orderReference: orderRef}, (error, orders) => {
            if(orders.length === 1) {
                narrate = `Transaction for ${orders[0].product_name} via ${paymentChannel.name}`;
            } else {
                narrate = `Transaction for ${orders.length} farm product(s) via ${paymentChannel.name}`;
            }
            eachAsync(orders, (order, i, done) => {
                total_invoice += parseFloat(order.total_price);
                userId = order.userId;
                done();
            }, () => {
                Transaction.create({orderReference: orderRef,
                    total_price: total_invoice,
                    transactionType: 'FARM_PRODUCT',
                    payment_channel: paymentChannel._id,
                    userId: userId, narration: narrate
                } , (err, transaction) => {
                    if(err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to generate payment link', err);
                    } else if(transaction) {
                        // todo: decide on payment URL
                        // switch channel
                        ProductManagementController.decideOnPayment(req, res, transaction, paymentChannel);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to save transaction record', err);
                    }
                });
            })
        });
    },
    decideOnPayment: (req, res, transaction, paymentChannel) => {
        const { card_id } = req.body;
        switch (paymentChannel.name.toLowerCase()) {
            case 'paystack': {
                if(card_id) {
                    useAutoCharge(req, res, transaction, paymentChannel)
                } else {
                    usePayStack(req, res, transaction, paymentChannel);
                }
                break;
            }
            case 'bank transfer': {
                transferToBankAccount(req, res, transaction, paymentChannel);
                break;
            }
            case 'e-wallet': {
                useWallet(req, res, transaction, paymentChannel);
                break;
            }
            case 'flutterwave': {
                useFlutter(req, res, transaction, paymentChannel);
                break;
            }
            default: {
                usePayStack(req, res, transaction, paymentChannel);
                break;
            }
        }
    },
    verifyPayment: (req, res) => {
        try {
            const transId = req.body.transactionId;
            if(transId) {
                Transaction.findById(transId, (err, transaction) => {
                    if(err) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Transaction does not exist on our system!', err);
                    } else {
                        verifyPayment(transaction.paymentReference, (erro, body, statusCode) => {
                            if(erro) {
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', erro);
                            }  else if(statusCode !== 200){
                                Transaction.findByIdAndUpdate(transId, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', erro);
                            } else {
                                const response = JSON.parse(body);
                                if(response.data.status === 'success') {
                                    const authorization = response.data.authorization;
                                    // console.log('RESPONSIE ', response);
                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {
                                        if(err) {
                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                        }
                                        ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PAID'}}, {multi: true}, () => {
                                            PaystackReference.create({...authorization, userId: transaction.userId}, () => {
                                                respHandler.sendSuccess(res, 200, 'Product order successful, payment confirmed!', {
                                                    body: response
                                                });
                                            });
                                        });
                                    });
                                } else {
                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {paystack_status: response.data.status}}, {new: true}, (err, update) => {});
                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to verify paystack payment, please retry!', response);

                                }
                            }
                        });
                    }
                });
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', {});
            }
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to verify payment.', error);
        }
    }
};

module.exports = ProductManagementController;

function usePayStack(req, res, transaction, paymentChannel) {
    User.findById(transaction.userId, (err, user) => {
        if(err) {
            // do nothing
            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
        } else {
            controllerService.calculateTransactionCharge(transaction.total_price, (charge, subTotal, amountToPay) => {
                initializePayment(
                    {email: user.email, full_name: user.full_name,
                        first_name: user.first_name, last_name: user.last_name,
                        amount: amountToPay,
                        transaction: transaction.toJSON()}, (error, body, statusCode) => {
                        if(error){
                            //handle errors
                            console.log('PAYSTACK ERROR', error);
                            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', error);
                        } else {
                            const response = JSON.parse(body);
                            if(response.status === false) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                            } else if(statusCode !== 200) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Error with payment gateway!', response);
                            } else {
                                Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: response.data.reference}}, {new: true},
                                    (errorTransaction, sucTransaction) => {
                                        if(errorTransaction) {
                                            // log error
                                            winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                        } else {
                                            ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PENDING_PAYMENT'}}, {multi: true}, () => {
                                                Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_PRODUCT'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {});
                                                respHandler.sendSuccess(res, 200, 'Transaction successfully created!', {
                                                    authorization_url: response.data.authorization_url,
                                                    details: {
                                                        charge: charge,
                                                        subTotal: subTotal,
                                                        total: amountToPay,
                                                        transactionId: transaction._id,
                                                        full_name: user.full_name,
                                                        email: user.email,
                                                        ref: response.data.reference,
                                                        access_code: response.data.access_code
                                                    }
                                                });
                                            });
                                        }
                                    });
                            }
                        }
                    });
            });
        }
    });
}
function transferToBankAccount(req, res, transaction, paymentChannel) {
    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: null}}, {new: true},
        (errorTransaction, sucTransaction) => {
            if(errorTransaction) {
                // log error
                winston.logger.log('error', `Error: while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}--bank transfer`);
                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete checkout using bank transfer', errorTransaction);
            } else {
                ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PENDING_PAYMENT'}}, {multi: true}, () => {
                    Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_PRODUCT'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {
                        respHandler.sendSuccess(res, 200, 'Transaction initiated with bank transfer, proceed to payment!', {
                            transactionRef: transaction.orderReference,
                            transactionId: transaction._id,
                            amount: transaction.total_price
                        });
                    });
                });
            }
        });
}
function useWallet(req, res, transaction, paymentChannel) {
    Wallet.findOne({userId: transaction.userId}, (error, wallet) => {
        if(error || !wallet) {
            return respHandler.sendError(res, 401, 'FAILURE', 'Unable to process payment with e-Wallet', error);
        } else if(!wallet.wallet_balance || wallet.wallet_balance < transaction.total_price) {
            return respHandler.sendError(res, 401, 'FAILURE', 'Unable to process payment with e-Wallet, insufficient balance', error);
        } else {
            const newBalance = parseFloat(wallet.wallet_balance) - parseFloat(transaction.total_price);
            console.log('BALANCE ', newBalance, parseFloat(wallet.wallet_balance), parseFloat(transaction.total_price));
            Wallet.findOneAndUpdate({userId: transaction.userId}, {$set: {wallet_balance: newBalance}},
                {new: true}, (e, walletSuc) => {
                    if(e) {
                        winston.logger.log('error', `Error while updating wallet balance - ${JSON.stringify(e)} ---Wallet transaction`);
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update wallet balance', e);
                    } else {
                        WalletTransaction.create({
                            narration: 'Purchase farm products with wallet',
                            type: 'DEBIT',
                            new_wallet_balance: newBalance,
                            old_wallet_balance: wallet.wallet_balance,
                            amount: transaction.total_price,
                            transactionId: transaction._id,
                            userId: transaction.userId
                        }, (err_, walletTrans) => {
                            if(err_) {
                                console.log('Error ', err_);
                                winston.logger.log('error', `Error: unable to update wallet transaction table - ${JSON.stringify(err_)} ---Wallet transaction table`);
                                return respHandler.sendError(res, 400, 'FAILURE', 'Unable to save wallet transaction', err_);
                            } else {
                                ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PAID'}}, {multi: true}, () => {
                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED', paymentReference: null}}, {new: true},
                                        (errorTransaction, sucTransaction) => {
                                            if(errorTransaction) {
                                                winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                            }
                                        });
                                    Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_PRODUCT'}]}, {$set: {checkout_status: true}}, { multi: true }, () => {
                                        respHandler.sendSuccess(res, 200, 'Transaction successful with e-wallet!', {});
                                    });
                                });
                            }
                        });
                    }
                });
        }
    });
}
function useFlutter(req, res, transaction, paymentChannel) {
    respHandler.sendSuccess(res, 200, 'Flutter wave is not setup for payment, please consider using paystack', {});
}
function useAutoCharge(req, res, transaction, paymentChannel) {
    const card_id = req.body.card_id;
    User.findById(transaction.userId, (err, user) => {
        if(err || !user) {
            // do nothing
            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', err);
        } else {
            const date = new Date();
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            PaystackReference.findOne({$and: [{_id: card_id}, {reusable: true}, {exp_year: {$gte: year}}, {exp_month: {$gte: month}}]},
                (cardErr, cardSuc) => {
                    if(cardErr || !cardSuc) {
                        respHandler.sendError(res, 401, 'FAILURE', 'Invalid payment card selected!', cardErr);
                    } else {
                        checkPaymentAuth({
                            email: user.email,
                            authorization_code: cardSuc.authorization_code,
                            amount: transaction.total_price
                        }, (error, body, statusCode) => {
                            const response = JSON.parse(body);
                            if(error){
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', error);
                            } else if(response.status === false) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                            } else if(statusCode !== 200) {
                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to charge card!', response);
                            } else {
                                controllerService.calculateTransactionCharge(transaction.total_price, (charge, subTotal, amountToPay) => {
                                    recurrentPayment({email: user.email, full_name: user.full_name,
                                        first_name: user.first_name, last_name: user.last_name,
                                        authorization_code: cardSuc.authorization_code,
                                        amount: amountToPay,
                                        transaction: transaction.toJSON()}, (_error, _body, _statusCode) => {
                                        if(_error){
                                            //handle errors
                                            respHandler.sendError(res, 401, 'FAILURE', 'Error while processing payment!', _error);
                                        } else {
                                            const _response = JSON.parse(_body);
                                            if(_response.status === false) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', {response: _response});
                                            } else if(_statusCode !== 200) {
                                                respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', {response: _response});
                                            } else {
                                                Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'INITIATED', paymentReference: _response.data.reference}}, {new: true},
                                                    (errorTransaction, sucTransaction) => {
                                                        if(errorTransaction || !sucTransaction ) {
                                                            // log error
                                                            winston.logger.log('error', `Error while updating transaction - ${JSON.stringify(errorTransaction)} --- ---${JSON.stringify(transaction)}`);
                                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', errorTransaction);
                                                        } else {
                                                            Cart.updateMany({$and: [{userId: transaction.userId}, {cart_type: 'FARM_PRODUCT'}]}, {$set: {checkout_status: true}}, {multi: true}, () => {});
                                                            verifyPayment(sucTransaction.paymentReference, (erro, __body, __statusCode) => {
                                                                if(erro) {
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction!', erro);
                                                                }  else if(__statusCode !== 200){
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'FAILED'}}, {new: true}, () => {});
                                                                    respHandler.sendError(res, 401, 'FAILURE', 'Unable to complete transaction, please retry!', erro);
                                                                } else {
                                                                    const response = JSON.parse(__body);
                                                                    Transaction.findByIdAndUpdate(transaction._id, {$set: {status: 'CONFIRMED'}}, {new: true}, (err, update) => {
                                                                        if(err) {
                                                                            respHandler.sendError(res, 401, 'FAILURE', 'Unable to confirm payment!', err);
                                                                        } else {
                                                                            ProductOrder.updateMany({orderReference: transaction.orderReference}, {$set: {status: 'PAID'}}, {multi: true}, () => {
                                                                                respHandler.sendSuccess(res, 200, 'Product ordering successful, payment confirmed!', {});
                                                                            });
                                                                        }
                                                                    });

                                                                }
                                                            });

                                                        }
                                                    });
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });

        }
    });
}

function initializePayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/cart/${data.transaction._id}/product_transaction`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/initialize',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        // console.log('Error ', error, response);
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function recurrentPayment(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        full_name: data.full_name,
        authorization_code: data.authorization_code,
        first_name: data.first_name,
        last_name: data.last_name,
        callback_url: `${process.env.FRONT_END_URL}user/wallet-history/${data.transaction._id}`,
        metadata: {
            transactionId: data.transaction._id,
            full_name: data.full_name,
            first_name: data.first_name,
            last_name: data.last_name,
            orderReference: data.transaction.orderReference,
            total_price: data.transaction.total_price,
            payment_channel: data.transaction.payment_channel,
            userId: data.transaction.userId,
            narration: data.transaction.narration,

        }
    };
    const options = {
        url : 'https://api.paystack.co/transaction/charge_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}
function checkPaymentAuth(data, cb) {
    const form = {
        email: data.email,
        amount: parseFloat(data.amount) * 100,
        authorization_code: data.authorization_code
    };
    const options = {
        url : 'https://api.paystack.co/transaction/check_authorization',
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        },
        form
    };
    const callback = (error, response, body)=>{
        return cb(error, body, response.statusCode);
    };
    request.post(options, callback);
}

function verifyPayment(ref, cb) {
    const options = {
        url : 'https://api.paystack.co/transaction/verify/' + encodeURIComponent(ref),
        headers : {
            authorization: `Bearer ${process.env.PAYSTACK_SKT}`,
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }
    };
    const callback = (error, response, body) => {
        return cb(error, body, response.statusCode);
    };
    request.get(options, callback);
}
