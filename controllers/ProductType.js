const ProductType = require('../models/ProductType');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const ProductTypeController = {
    getProductTypes: (req, res) => {
        try {
            ProductType.find({})
                .exec((err, productType) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product types.', err);
                    } else if (validate.resultFound(productType, res)) {
                        const data = validate.formatData(productType);
                        respHandler.sendSuccess(res, 200, 'Product types listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product types.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list product types.', err);
        }
    },
    getActiveProductType: (req, res) => {
        try {
            ProductType.find({status: true})
                .exec((err, productType) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product type.', err);
                    } else if (validate.resultFound(productType, res)) {
                        const data = validate.formatData(productType);
                        respHandler.sendSuccess(res, 200, 'Product type listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product type.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active product type.', err);
        }
    },
    createProductType: (req, res) => {
        try {
            ProductType.create(req.body, (error, productType) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create product type.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Product type created successfully', productType);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new product type.', err);
        }
    },
    getProductTypeById: (req, res) => {
        try {
            ProductType.findById(req.params.id).exec((err, productType) => {
                if (err || !productType){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get product type by id.', err);
                } else if (validate.resultFound(productType, res)) {
                    const data = validate.formatData(productType);
                    respHandler.sendSuccess(res, 200, 'Product type fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get product type.', err);
        }
    },
    putProductType: (req, res) => {
        try {
            console.log('This is body ', req.body);
            ProductType.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, productType) => {
                if (err || !productType){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update product type.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Product type updated successfully!', productType);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update product type');
        }
    },
    deleteProductType: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                ProductType.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete product type.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Product type deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete product type by id');
        }
    },
};

module.exports = ProductTypeController;