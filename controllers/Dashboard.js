const respHandler = require('../services/responseHandler');
const controllerService = require('../services/controllerServices');
const Wallet = require('../models/Wallet');
const Sponsorship = require('../models/Sponsorship');
const FarmShop = require('../models/FarmShop');
const Version = require('../models/Version');
const AccountManager = require('../models/AccountManager');
const Cart = require('../models/Cart');
const Order = require('../models/Order');
const Transaction = require('../models/Transaction');
const Users = require('../models/Users');
const Newsletter = require('../models/Newsletter');
const Referral = require('../models/Refarral');
const WalletTransaction = require('../models/WalletTransaction');
const moment = require('moment');
const eachAsync = require('each-async');
const DashboardController = {
    getDashboard: async (req, res) => {
        try {
            const dashboard = {
                wallet: {
                    wallet_balance: 0.0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                withdraw: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                active_sponsorship: {
                    total: 0,
                    interest: 0,
                    name: null,
                    roi: 0,
                    duration: null,
                    duration_type: null,
                    currency: 'NGN',
                    date: moment.now(),
                    end_date: null,
                    start_date: null,
                    spike: []
                },
                total_investment: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                total_returns: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                expected_returns: {
                    total: 0,
                    names: [],
                    number_of_farms: 0,
                    spike: [],
                    description: [],
                    returns: [],
                    invested: [],
                    interests: [],
                    currency: 'NGN',
                    date: moment.now()
                }


            };
            controllerService.getLoginUser(req, 'userId', (userId) => {
                WalletTransaction.find({userId: userId}, 'new_wallet_balance').sort('-date').limit(10).exec((error, walletTransactions) => {
                    Wallet.findOne({userId: userId}, (err, wallet) => {
                        if(wallet) {
                            dashboard.wallet.wallet_balance = wallet.wallet_balance || 0.0;
                        }
                        if(walletTransactions.length) {
                            dashboard.wallet.spike = [0];
                            walletTransactions.forEach((trans) => {
                                dashboard.wallet.spike.push(parseInt(trans.new_wallet_balance)/10000);
                            });
                        }

                        Sponsorship.find({$and: [{userId: userId}, {status: 'STARTED'}]})
                            .populate('farmId', 'farm_name currency')
                            .populate('orderId', 'roi duration duration_type total_price')
                            .limit(5)
                            .sort('-date').exec((errorror, sponsorships) => {
                            dashboard.expected_returns.total = 0;
                            eachAsync(sponsorships, (spon, i, done) => {
                                    dashboard.expected_returns.names.push(spon.farmId.farm_name);
                                    dashboard.expected_returns.description.push(`${spon.farmId.farm_name}-${spon.orderId.roi}% ROI`);
                                    dashboard.expected_returns.invested.push(spon.orderId.total_price);
                                    dashboard.expected_returns.returns.push(spon.expected_return);
                                    dashboard.expected_returns.interests.push(spon.expected_return - spon.orderId.total_price);
                                    dashboard.expected_returns.spike.push(spon.orderId.roi);
                                    done();
                                },
                                () => {
                                        Sponsorship.find({$and: [{userId: userId}, {$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}]}]})
                                            .populate('farmId', 'farm_name currency')
                                            .populate('orderId', 'roi duration duration_type total_price')
                                            .sort('-date').exec((e, sponsorships) => {
                                            dashboard.total_returns.total = 0;
                                            eachAsync(sponsorships, (spon, i, doneNow) => {
                                                dashboard.total_returns.spike.push(spon.orderId.roi);
                                                dashboard.total_returns.total += spon.expected_return || 0;
                                                doneNow();
                                            }, () => {
                                                Sponsorship.find({$and: [{userId: userId}, {$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}, {status: 'STARTED'}]}]})
                                                    .populate('farmId', 'farm_name currency')
                                                    .populate('orderId', 'roi duration duration_type total_price')
                                                    .sort('-date').exec((e, sponsorships) => {
                                                    dashboard.total_investment.total = 0;
                                                    eachAsync(sponsorships, (spon, i, done) => {
                                                        dashboard.total_investment.spike.push(spon.orderId.roi);
                                                        dashboard.total_investment.total += spon.orderId.total_price || 0;
                                                        done();
                                                    }, () => {
                                                        Sponsorship.findOne({$and: [{userId: userId}, {status: 'STARTED'}]})
                                                            .populate('farmId', 'farm_name currency')
                                                            .populate('orderId', 'roi duration duration_type total_price')
                                                            .sort('-date').exec((e, sponsorship) => {
                                                            if(sponsorship) {
                                                                dashboard.active_sponsorship.name = sponsorship.farmId.farm_name;
                                                                dashboard.active_sponsorship.roi = sponsorship.orderId.roi;
                                                                dashboard.active_sponsorship.duration = sponsorship.orderId.duration;
                                                                dashboard.active_sponsorship.duration_type = sponsorship.orderId.duration_type;
                                                                dashboard.active_sponsorship.total = sponsorship.expected_return;
                                                                dashboard.active_sponsorship.interest = sponsorship.expected_return - sponsorship.orderId.total_price;
                                                                dashboard.active_sponsorship.end_date = sponsorship.expected_end_date;
                                                                dashboard.active_sponsorship.start_date = sponsorship.start_date;
                                                                for(let i = 0; i < 10; i++) {
                                                                    dashboard.active_sponsorship.spike.push(controllerService.getRandomInt(15));
                                                                }
                                                            }
                                                            Sponsorship.find({$and: [{userId: userId}, {status: 'STARTED'}]} , (_ee, activeSponsorships) => {
                                                                dashboard.expected_returns.number_of_farms = activeSponsorships.length;
                                                                eachAsync(activeSponsorships, (_spon, i, don) => {
                                                                    dashboard.expected_returns.total += _spon.expected_return || 0;
                                                                    don();
                                                                }, () => {
                                                                    respHandler.sendSuccess(res, 200, 'Dashboard fetched successfully for user', dashboard);
                                                                });
                                                            });
                                                        });
                                                    });

                                                });
                                            });

                                        });


                                });
                        });
                    });
                });
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get user dashboard!', err);
        }
    },
    adminGetUserDashboard: async (req, res) => {
        try {
            const dashboard = {
                wallet: {
                    wallet_balance: 0.0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                withdraw: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                active_sponsorship: {
                    total: 0,
                    interest: 0,
                    name: null,
                    roi: 0,
                    duration: null,
                    duration_type: null,
                    currency: 'NGN',
                    date: moment.now(),
                    end_date: null,
                    start_date: null,
                    spike: []
                },
                total_investment: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                total_returns: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                expected_returns: {
                    total: 0,
                    names: [],
                    number_of_farms: 0,
                    spike: [],
                    description: [],
                    returns: [],
                    invested: [],
                    interests: [],
                    currency: 'NGN',
                    date: moment.now()
                }


            };
            const userId = req.params.userId;
            WalletTransaction.find({userId: userId}, 'new_wallet_balance').sort('-date').limit(10).exec((error, walletTransactions) => {
                Wallet.findOne({userId: userId}, (err, wallet) => {
                    if(wallet) {
                        dashboard.wallet.wallet_balance = wallet.wallet_balance || 0.0;
                    }
                    if(walletTransactions.length) {
                        dashboard.wallet.spike = [0];
                        walletTransactions.forEach((trans) => {
                            dashboard.wallet.spike.push(parseInt(trans.new_wallet_balance)/10000);
                        });
                    }

                    Sponsorship.find({$and: [{userId: userId}, {status: 'STARTED'}]})
                        .populate('farmId', 'farm_name currency')
                        .populate('orderId', 'roi duration duration_type total_price')
                        .limit(5)
                        .sort('-date').exec((errorror, sponsorships) => {
                        dashboard.expected_returns.total = 0;
                        eachAsync(sponsorships, (spon, i, done) => {
                                dashboard.expected_returns.names.push(spon.farmId.farm_name);
                                dashboard.expected_returns.description.push(`${spon.farmId.farm_name}-${spon.orderId.roi}% ROI`);
                                dashboard.expected_returns.invested.push(spon.orderId.total_price);
                                dashboard.expected_returns.returns.push(spon.expected_return);
                                dashboard.expected_returns.interests.push(spon.expected_return - spon.orderId.total_price);
                                dashboard.expected_returns.spike.push(spon.orderId.roi);
                                done();
                            },
                            () => {
                                Sponsorship.find({$and: [{userId: userId}, {$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}]}]})
                                    .populate('farmId', 'farm_name currency')
                                    .populate('orderId', 'roi duration duration_type total_price')
                                    .sort('-date').exec((e, sponsorships) => {
                                    dashboard.total_returns.total = 0;
                                    eachAsync(sponsorships, (spon, i, doneNow) => {
                                        dashboard.total_returns.spike.push(spon.orderId.roi);
                                        dashboard.total_returns.total += spon.expected_return || 0;
                                        doneNow();
                                    }, () => {
                                        Sponsorship.find({$and: [{userId: userId}, {$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}, {status: 'STARTED'}]}]})
                                            .populate('farmId', 'farm_name currency')
                                            .populate('orderId', 'roi duration duration_type total_price')
                                            .sort('-date').exec((e, sponsorships) => {
                                            dashboard.total_investment.total = 0;
                                            eachAsync(sponsorships, (spon, i, done) => {
                                                dashboard.total_investment.spike.push(spon.orderId.roi);
                                                dashboard.total_investment.total += spon.orderId.total_price || 0;
                                                done();
                                            }, () => {
                                                Sponsorship.findOne({$and: [{userId: userId}, {status: 'STARTED'}]})
                                                    .populate('farmId', 'farm_name currency')
                                                    .populate('orderId', 'roi duration duration_type total_price')
                                                    .sort('-date').exec((e, sponsorship) => {
                                                    if(sponsorship) {
                                                        dashboard.active_sponsorship.name = sponsorship.farmId.farm_name;
                                                        dashboard.active_sponsorship.roi = sponsorship.orderId.roi;
                                                        dashboard.active_sponsorship.duration = sponsorship.orderId.duration;
                                                        dashboard.active_sponsorship.duration_type = sponsorship.orderId.duration_type;
                                                        dashboard.active_sponsorship.total = sponsorship.expected_return;
                                                        dashboard.active_sponsorship.interest = sponsorship.expected_return - sponsorship.orderId.total_price;
                                                        dashboard.active_sponsorship.end_date = sponsorship.expected_end_date;
                                                        dashboard.active_sponsorship.start_date = sponsorship.start_date;
                                                        for(let i = 0; i < 10; i++) {
                                                            dashboard.active_sponsorship.spike.push(controllerService.getRandomInt(15));
                                                        }
                                                    }
                                                    Sponsorship.find({$and: [{userId: userId}, {status: 'STARTED'}]} , (_ee, activeSponsorships) => {
                                                        dashboard.expected_returns.number_of_farms = activeSponsorships.length;
                                                        eachAsync(activeSponsorships, (_spon, i, don) => {
                                                            dashboard.expected_returns.total += _spon.expected_return || 0;
                                                            don();
                                                        }, () => {
                                                            respHandler.sendSuccess(res, 200, 'Dashboard fetched successfully for user', dashboard);
                                                        });
                                                    });
                                                });
                                            });

                                        });
                                    });

                                });


                            });
                    });
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get user dashboard!', err);
        }
    },
    getAdminDashboard: async (req, res) => {
        try {
            const dashboard = {
                wallet: {
                    wallet_total: 0.0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                users: {
                    verified: 0,
                    unverified: 0,
                    migrated: 0,
                    total: 0,
                    referred: 0,
                    spike: [],
                    date: moment.now()
                },
                stats: {
                    account_manager: 0,
                    cart_count: 0,
                    orders: 0,
                    sponsorships: 0,
                    sponsorships_completed: 0,
                    transactions: 0,
                    newsletter: 0,
                    confirmed_transactions: 0,
                    wallet_transaction: 0,
                    date: moment.now()
                },
                withdraw: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                total_investment: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                total_returns: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                },
                expected_returns: {
                    total: 0,
                    currency: 'NGN',
                    spike: [],
                    date: moment.now()
                }

            };

            dashboard.stats.account_manager = await AccountManager.countDocuments({resolved: {$ne: true}}).exec();
            dashboard.stats.cart_count = await Cart.countDocuments({checkout_status: false}).exec();
            dashboard.stats.orders = await Order.countDocuments({}).exec();
            dashboard.stats.sponsorships = await Sponsorship.countDocuments({status: 'STARTED'}).exec();
            dashboard.stats.sponsorships_completed = await Sponsorship.countDocuments({status: 'COMPLETED'}).exec();
            dashboard.stats.transactions = await Transaction.countDocuments({status: 'INITIATED'}).exec();
            dashboard.stats.confirmed_transactions = await Transaction.countDocuments({status: 'CONFIRMED'}).exec();
            dashboard.stats.wallet_transaction = await WalletTransaction.countDocuments({}).exec();
            dashboard.stats.newsletter = await Newsletter.countDocuments({}).exec();

            dashboard.users.migrated = await Users.countDocuments({migrated: true}).exec();
            dashboard.users.verified = await Users.countDocuments({is_verified: true}).exec();
            dashboard.users.unverified = await Users.countDocuments({is_verified: false}).exec();
            dashboard.users.total = await Users.countDocuments({}).exec();
            dashboard.users.referred = await Referral.countDocuments({has_joined: true}).exec();
            Wallet.find({wallet_balance: {$gt: 0}}, 'wallet_balance').exec((error, wallets) => {
                if(error || !wallets.length) {
                    dashboard.wallet.wallet_total = 0;
                }
                if(wallets.length) {
                    dashboard.wallet.wallet_total = 0;
                    dashboard.wallet.spike = [0];
                    eachAsync(wallets, (wallet, i, done) => {
                        dashboard.wallet.spike.push(parseInt(wallet.wallet_balance)/10000);
                        dashboard.wallet.wallet_total += parseFloat(wallet.wallet_balance);
                        done();
                    }, () => {

                    });
                }

                Sponsorship.find({$and: [{$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}]}]})
                    .exec((e, sponsorships) => {
                    dashboard.total_returns.total = 0;
                        dashboard.total_returns.spike = [0];
                        eachAsync(sponsorships, (spon, i, doneNow) => {
                        dashboard.total_returns.spike.push(parseInt(spon.expected_return)/10000);
                        dashboard.total_returns.total += spon.expected_return || 0;
                        doneNow();
                    }, () => {
                        Sponsorship.find({$or: [{status: 'COMPLETED'}, {status: 'WITHDREW'}, {status: 'STARTED'}] })
                            .populate('orderId', 'total_price')
                            .exec((e, sponsorships) => {
                            dashboard.total_investment.total = 0;
                            dashboard.total_investment.spike = [0];
                            eachAsync(sponsorships, (_spon, i, done) => {
                                dashboard.total_investment.total += _spon.orderId.total_price || 0;
                                dashboard.total_investment.spike.push(parseInt(_spon.orderId.total_price)/10000);
                                done();
                            }, () => {
                                Sponsorship.find({status: 'STARTED'} , (_ee, activeSponsorships) => {
                                    dashboard.expected_returns.total = 0;
                                    dashboard.expected_returns.spike = [0];
                                    eachAsync(activeSponsorships, (__spon, i, don) => {
                                        dashboard.expected_returns.total += __spon.expected_return || 0;
                                        dashboard.expected_returns.spike.push(parseInt(__spon.expected_return)/10000);
                                        don();
                                    }, () => {
                                        Sponsorship.find({status: 'WITHDREW'}, (ee, withdrawals) => {
                                            dashboard.withdraw.total = 0;
                                            dashboard.withdraw.spike = [0];
                                            // if(!withdrawals.length)
                                            eachAsync(withdrawals, (withdraw, i, done_now) => {
                                                dashboard.withdraw.total += withdraw.expected_return || 0;
                                                dashboard.withdraw.spike.push(parseInt(withdraw.expected_return)/10000);
                                                done_now();
                                            }, () => {
                                                dashboard.users.spike = [0, dashboard.users.verified, dashboard.users.unverified, dashboard.users.referred, dashboard.users.migrated, dashboard.users.total];
                                                dashboard.wallet.spike = dashboard.wallet.spike.slice(0, 10);
                                                dashboard.users.spike = dashboard.users.spike.slice(0, 10);
                                                dashboard.total_investment.spike = dashboard.total_investment.spike.slice(0, 10);
                                                dashboard.expected_returns.spike = dashboard.expected_returns.spike.slice(0, 10);
                                                dashboard.total_returns.spike = dashboard.total_returns.spike.slice(0, 10);
                                                respHandler.sendSuccess(res, 200, 'Dashboard fetched successfully', dashboard);
                                            });
                                        });
                                    });
                                });
                            });

                        });
                    });

                });
            });
        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get dashboard!', err);
        }
    },
    getFarmShopStatus: (req, res) => {
        try {
            FarmShop.find({})
                .populate('farm_status')
                .exec((err, farmShops) => {
                let found = true;
                eachAsync(farmShops, (farmShop, i, next) => {
                // || farmShop.remaining_in_stock > 0
                    if(farmShop && ( farmShop.farm_status.name === 'Now Selling')) {
                        found = true;
                        next('done');
                    } else {
                        found = false;
                        next();
                    }
                }, (response) => {
                    respHandler.sendSuccess(res, 200, (found) ? 'FOUND' : 'NOT_FOUND', {});
                });
            });
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'FOUND', error);
        }
    },
    addVersion: (req, res) => {
        try {
            Version.create(req.body, (err, version) => {
                if(err) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to add latest version', err);
                }
                respHandler.sendSuccess(res, 200, 'Version added successfully', version);
            })
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to add version', error);
        }
    },
    getLatestVersion: (req, res) => {
        try {
            Version.findOne({})
                .sort({$natural: -1})
                .exec((err, version) => {
                if(err) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to get version', error);
                }
                 respHandler.sendSuccess(res, 200, 'Version fetched successfully', version);
                });
        } catch (error) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get version', error);
        }
    }
};

module.exports = DashboardController;