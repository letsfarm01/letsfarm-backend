const ContactUs = require('../models/ContactUs');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const mailer = require('../mailing/mailSystem');

const ContactUsController = {
    getContactUs: (req, res) => {
        try {
            ContactUs.find({}).exec((err, contactForms) => {
                if (err) {
                  return respHandler.sendError(res, 400, 'FAILURE', 'Unable to list contact us form data.', err);
                }
                if (validate.resultFound(contactForms, res)) {
                    const data = validate.formatData(contactForms);
                    respHandler.sendSuccess(res, 200, 'Contact us form data listed successfully', data);
                } else {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list contact us form data.', err);
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list contact us data.', err);
        }
    },
    createContactUs: (req, res) => {
        try {
            ContactUs.create(req.body, (error, contactForm) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to request for support.', error);
                } else {
                    mailer.sendHtml({email: process.env.MAILJET_USER_EMAIL, full_name: `Letsfarm Admin`},
                        `${contactForm.name} request for support`, 'ContactForm',
                        {user_name: `${contactForm.name}`, message: contactForm.message}, contactForm.email);
                    respHandler.sendSuccess(res, 200, 'Contact request sent!', contactForm);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to request for support.', err);
        }
    },
    getContactUsById: (req, res) => {
        try {
            ContactUs.findById(req.params.id).exec((err, contactForm) => {
                if (err || !contactForm){
                  return respHandler.sendError(res, 404, 'FAILURE', 'Unable to get contact us form data by id.', err);
                }
                if(validate.resultFound(contactForm, res)) {
                    const data = validate.formatData(contactForm);
                    respHandler.sendSuccess(res, 200, 'Contact us form data fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get contact us form data.', err);
        }
    },
    deleteContactUs: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                ContactUs.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                       return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete contact us form data.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Contact us form data deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete contact us form data by id');
        }
    },
};

module.exports = ContactUsController;