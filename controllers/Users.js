const Users = require('../models/Users');
const ArchiveSponsor = require('../models/ArchivedSponsor');
const Wallet = require('../models/Wallet');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const csv = require('fast-csv');
const moment = require('moment');
const nanoid = require('nanoid');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const mailer = require('../mailing/mailSystem');


const UsersController = {
    getUsers: async (req, res) => {
        try {
            const { page, limit, role } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9050;
            let cond = {};
            if(role === 'USER') {
                cond = {role: role.toUpperCase()}
            } else if(role === 'SUPER') {
                cond = {role: {$ne: 'USER'}}
            } else {
                cond = {};
            }
            const totalCount = await Users.countDocuments(cond).exec();
            Users.find(cond, '-password')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, users) => {
                if (err) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list users.', err);
                }
                // console.log('Users ', users);
                if (validate.resultFound(users, res)) {
                    // const data = validate.formatData(users);
                    const data = {
                        data: validate.formatData(users),
                        page: currentPage += 1,
                        limit: currentLimit,
                        total: totalCount
                    };
                    respHandler.sendSuccess(res, 200, 'Users listed successfully', data);
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list users.', err);
        }
    },
    createAdmin: (req, res) => {
        try {
            const userObject = req.body;
            console.log('USER => ', userObject);
            if(userObject && !validate.isEmptyObject(userObject)){
                if(!userObject.password) {
                    respHandler.sendError(res, 406, 'FAILURE', 'No password provided!');
                } else if(!userObject.email) {
                    respHandler.sendError(res, 406, 'FAILURE', 'Provide admin email!');
                }  else if(!userObject.role) {
                    respHandler.sendError(res, 406, 'FAILURE', 'Provide admin role!');
                } else {
                    bcrypt.hash(userObject.password, 10,  (err, hash) => {
                        userObject.password = hash;
                        Users.create(userObject, (err, user) => {
                            if (err) {
                                console.log('err.name' , err);
                                switch (err.name) {
                                    case 'ValidationError': {
                                        if (err.message.includes('`email` to be unique')){
                                            respHandler.sendError(res, 406, 'FAILURE', 'Email already exist!');
                                        } else if(err.message.includes('`email` is invalid')) {
                                            respHandler.sendError(res, 401, 'FAILURE', 'Invalid email address!');
                                        } else {
                                            respHandler.sendError(res, 401, 'FAILURE', err.message, err);
                                        }
                                        break;
                                    }
                                    default: {
                                        respHandler.sendError(res, 400, 'FAILURE', err.message, err);
                                        break;
                                    }
                                }
                            } else {
                                respHandler.sendSuccess(res, 200, 'Admin created successfully', user);
                            }
                        });
                    });
                }
            } else {
                respHandler.sendError(res, 400, 'FAILURE', 'Provide user details');
            }

        } catch (err){
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to create admin');
        }
    },

    migration: (req, res) => {
        try {
            console.log('REQ ', req.body, req.files);
            ArchiveSponsor.collection.drop();
            Users.deleteMany({migrated: true}, () => {            });
            if(!req.files){
                respHandler.sendError(res, 400, 'FAILURE', 'No file upload for migration!', {});
            } else
                {
                let migrationFile = req.files.file;
                let sponsors = [];
                let users = [];
                csv.fromString(migrationFile.data.toString(), {
                    // delimiter: '\t',
                    headers: true,
                    ignoreEmpty: true,
                    discardUnmappedColumns: true,
                })
                    .on("data", function(value){
                        const PASSWORD = nanoid(12);
                        const _id = new mongoose.Types.ObjectId();
                        value.phone_number = value.phone_number.toLocaleString();

                        // console.log('DATA KEYS UNIQUE', value.phone_number, _id, PASSWORD);
                        let user = {
                            _id: _id,
                            first_name: value.first_name,
                            last_name: value.last_name,
                            email: value.email.toLocaleString().toLowerCase(),
                            address: value.address,
                            password_: PASSWORD,
                            password: null,
                            city: value.city,
                            is_verified: true,
                            migrated: true,
                            state_province: value.state_province,
                            phone_number: value.phone_number
                        };
                        let sponsor = {
                            farm_name: value.FARM.toLocaleString(),
                            email: value.email.toLocaleString().toLowerCase(),
                            first_name: value.first_name,
                            last_name: value.last_name,
                            quantity: value.quantity,
                            order_number: value.order_number,
                            order_total: value.order_total,
                            order_status: value.order_status,
                            date_of_sponsor: value.date_of_sponsor,
                            roi_date: value.roi_date,
                            roi_amount: value.roi_amount
                        };
                        if(user.email && user.email != '' ) {
                            users.push(user);
                            sponsors.push(sponsor);
                        }

                    })
                    .on("error", function(data){
                        return false;
                    })
                    .on("end", function() {
                        // console.log('USER SET ', users.length);
                        respHandler.sendSuccess(res, 200, users.length + ' users read, processing in progress ' + sponsors.length, {});
                        eachAsync(users, (_user, i, done) => {
                            bcrypt.hash(_user.password_, 10,  (err, hash) => {
                                if(err) {
                                    // console.log('Error ', err);
                                    done();
                                } else {
                                    _user.password = hash;
                                    // console.log('USER ', _user);
                                    Users.create(_user, (e, suc) => {
                                        if(e) {
                                            done();
                                        } else {
                                            Wallet.create({userId: suc._id, wallet_balance: 0.0, last_fund_date: 'nil'},
                                                () => {
                                                    mailer.sendHtml({email: suc.email, full_name: `${suc.first_name} ${suc.last_name}`},
                                                        'Password update', 'UserPasswordUpdate', {first_name: suc.first_name,
                                                            password: _user.password_,
                                                            title: 'Password update'});
                                                    done();
                                                });
                                        }
                                    })
                                }
                            });
                        }, () => {
                            ArchiveSponsor.create(sponsors, (er, aS) => {
                                console.log('Done with Migration');
                            });
                        });

                    });

            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to import data for migration!', err);
        }
    },
    migration2: (req, res) => {
        try {
            console.log('REQ ', req.body, req.files);
            if(!req.files){
                respHandler.sendError(res, 400, 'FAILURE', 'No file upload for migration!', {});
            } else
                {
                let migrationFile = req.files.file;
                let sponsors = [];
                let users = [];
                csv.fromString(migrationFile.data.toString(), {
                    // delimiter: '\t',
                    headers: true,
                    ignoreEmpty: true,
                    discardUnmappedColumns: true,
                })
                    .on("data", function(value){
                        let sponsor = {
                            farm_name: value.FARM.toLocaleString(),
                            email: value.email.toLocaleString().toLowerCase(),
                            quantity: value.quantity,
                            order_number: value.order_number,
                            order_total: value.order_total,
                            order_status: value.order_status,
                            date_of_sponsor: value.date_of_sponsor,
                            roi_date: value.roi_date,
                            roi_amount: value.roi_amount
                        };
                        if(sponsor.order_number && sponsor.email ) {
                            sponsors.push(sponsor);
                        }

                    })
                    .on("error", function(data){
                        return false;
                    })
                    .on("end", function() {
                        ArchiveSponsor.create(sponsors, () => {
                            respHandler.sendSuccess(res, 200, sponsors.length + ' ---- users read, processing in progress ', {});
                        });
                       /* eachAsync(sponsors, (sponsor, i, done) => {
                            ArchiveSponsor.findOneAndUpdate({email: sponsor.email}, {$set: {
                                quantity: sponsor.quantity,
                                order_number: sponsor.order_number,
                                order_total: sponsor.order_total,
                                order_status: sponsor.order_status,
                                date_of_sponsor: sponsor.date_of_sponsor,
                                roi_date: sponsor.roi_date,
                                roi_amount: sponsor.roi_amount
                            }}, {new: true}, () => {
                                done();
                            })
                        }, () => {
                            respHandler.sendSuccess(res, 200, sponsors.length + ' users read, processing in progress ', {});
                        });
*/
                    });

            }
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to import data for migration!', err);
        }
    },
    getUser: (req, res) => {
        try {
            Users.findById(req.params.id, '-password').exec((err, user) => {
                if (err || !user){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user.', err);
                }
                if(validate.resultFound(user, res)) {
                    const data = validate.formatData(user);
                    respHandler.sendSuccess(res, 200, 'User fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user.', err);
        }
    },
    putUser: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const _user = req.body;
            if (_user.password) {
                _user.password = null;
                delete _user.password
            }
            Users.findByIdAndUpdate(req.params.id, {$set: _user}, { new: true }, (err, user) => {
                if (err || !user){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update user.', err);
                }
                if(user) {
                    user.password = null;
                    respHandler.sendSuccess(res, 200, 'User updated successfully!', user);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update user');
        }
    },
    patchUser: (req, res) => {
        try {
            console.log('This is body ', req.body);
            const _user = req.body;
            delete _user.profile_picture;
            if (_user.password) {
                _user.password = null;
                delete _user.password;
            }
            Users.findByIdAndUpdate(req.params.id, {$set: _user}, { new: true }, (err, user) => {
                if (err || !user){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to patch user.', err);
                }
                if(user) {
                    user.password = null;
                    respHandler.sendSuccess(res, 200, 'User patched successfully!', user);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to patch user');
        }
    },
    deleteUser: (req, res) => {
        try {
             controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Users.delete({_id: req.params.id}, deletedBy, (err, user) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete user.', err);
                    }
                    Users.findByIdAndUpdate(req.params.id, {$set: {email: user.email + '__' + moment.now() + '_' + nanoid(3)}}, {new: true}, () => {
                        respHandler.sendSuccess(res, 200, 'User deleted successfully!', {});
                    });
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete user');
        }
    },
};

module.exports = UsersController;
