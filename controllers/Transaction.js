const Transaction = require('../models/Transaction');
const PaymentChannel = require('../models/PaymentChannel');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const TransactionController = {
    getTransactions: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await Transaction.countDocuments({}).exec();
            Transaction.find({})
                .populate('payment_channel')
                .populate('userId', '-password')
                .populate('proof_payment')
                .sort('-createdAt')
                .exec((err, transaction) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Transactions listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
        }
    },
    getPaystackTransactions: (req, res) => {
        try {
            const { page, limit, status } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;

            PaymentChannel.findOne({name: 'Paystack'}, async (error, channel) => {
                if(error || !channel) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', error);
                } else {
                    let cond = {payment_channel: channel._id};
                    if(status) {
                        cond = {$and: [{payment_channel: channel._id}, {status}]};
                    } else {
                        cond = {payment_channel: channel._id};
                    }
                    const totalCount = await Transaction.countDocuments(cond).exec();
                    Transaction.find(cond)
                        .populate('payment_channel')
                        .populate('userId', '-password')
                        .populate('proof_payment')
                        .sort("-createdAt")
                        .exec((err, transaction) => {
                            if (err) {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                            } else if (validate.resultFound(transaction, res)) {
                                const data = {
                                    data: validate.formatData(transaction),
                                    page: currentPage += 1,
                                    limit: currentLimit,
                                    total: totalCount
                                };
                                respHandler.sendSuccess(res, 200, 'Transactions listed successfully', data);
                            } else {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                            }
                        })
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
        }
    },
    getBankTransactions: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await Transaction.countDocuments({}).exec();
            PaymentChannel.findOne({name: 'Bank Transfer'}, (error, channel) => {
                if(error || !channel) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', error);
                } else {
                    Transaction.find({payment_channel: channel._id})
                        .populate('payment_channel')
                        .populate('userId', '-password')
                        .populate('proof_payment')
                        .sort("-createdAt")
                        .exec((err, transaction) => {
                            if (err) {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                            } else if (validate.resultFound(transaction, res)) {
                                const data = {
                                    data: validate.formatData(transaction),
                                    page: currentPage += 1,
                                    limit: currentLimit,
                                    total: totalCount
                                };
                                respHandler.sendSuccess(res, 200, 'Transactions listed successfully', data);
                            } else {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                            }
                        })
                }
            })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
        }
    },
    getUserTransactions: (req, res) => {
        try {
            Transaction.find({$and: [{userId: req.params.userId}, {status: 'CONFIRMED'}]})
                .populate('payment_channel')
                .sort("-createdAt")
                .exec((err, transactions) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user\'s transactions.', err);
                    } else if (validate.resultFound(transactions, res)) {
                        const data = validate.formatData(transactions);
                        respHandler.sendSuccess(res, 200, 'User\'s transactions fetched successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list transactions.', err);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transactions.', err);
        }
    },

    searchTransaction: async (req, res) => {
        try {
            const { narration, orderReference, status, total_price } = req.body;
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await Transaction.countDocuments({$or: [{narration: new RegExp(`^${narration}.*`, "i")},
                {orderReference: orderReference}, {status: status}, {total_price: total_price}]}).exec();

            Transaction.find({$or: [{narration: new RegExp(`^${narration}.*`, "i")},
                {orderReference: orderReference}, {status: status}, {total_price: total_price}]})
                .populate('userId', '-password')
                .populate('payment_channel')
                .skip((currentLimit * currentPage) - currentLimit)
                .sort("-createdAt")
                .limit(currentLimit)
                .exec((err, transaction) => {
                    if (err || !transaction.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No transaction matches that narration!', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Transactions retrieved successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to search transaction.', err);
        }
    },
    filterTransaction: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const transactionQuery = getSearchQuery(req);
            transactionQuery
                .populate('payment_channel')
                .populate('userId', '-password')
                .skip((currentPage - 1) * currentLimit)
                .limit(currentLimit)
                .sort("-createdAt")
                .exec((err, transaction) => {
                    if (err || !transaction.length){
                        respHandler.sendError(res, 404, 'FAILURE', 'No transactions matches this criteria!', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = {
                            data: validate.formatData(transaction),
                            page: currentPage += 1,
                            limit: currentLimit
                        };
                        respHandler.sendSuccess(res, 200, 'Transaction filtered successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to filter transaction.', err);
        }
    },
    getTransaction: (req, res) => {
        try {
            Transaction.findById(req.params.id)
                .populate('userId', '-password')
                .populate('payment_channel')
                .populate('proof_payment')
                .exec((err, transaction) => {
                    if (err || !transaction){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transaction by id.', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = validate.formatData(transaction);
                        respHandler.sendSuccess(res, 200, 'Transaction fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transaction.', err);
        }
    },
    getTransactionByRef: (req, res) => {
        try {
            Transaction.findOne({orderReference: req.params.ref})
                .populate('userId', '-password')
                .populate('payment_channel')
                .sort("-createdAt")
                .exec((err, transaction) => {
                    if (err || !transaction){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transaction by order reference.', err);
                    } else if (validate.resultFound(transaction, res)) {
                        const data = validate.formatData(transaction);
                        respHandler.sendSuccess(res, 200, 'Transaction fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transaction.', err);
        }
    },
    getTransactionsAwaitingPOP: (req, res) => {
        try {
            Transaction.find({$and: [{status: 'INITIATED'}, {userId: req.params.userId}]})
                .populate('payment_channel')
                .sort("-createdAt")
                .exec((err, transactions) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transactions with pending approval.', err);
                    } else {
                        const trans = [];
                        eachAsync(transactions, (transaction, i, done) => {
                            console.log('transaction.payment_channel.name', transaction.payment_channel.name);
                            if(transaction.payment_channel.name.toLowerCase() === 'bank transfer') {
                                trans.push(transaction);
                                done();
                            } else {
                                done();
                            }
                        }, () => {
                            respHandler.sendSuccess(res, 200, 'Transaction fetched successfully', trans);
                        });
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get transactions.', err);
        }
    },
    putTransaction: (req, res) => {
        try {
            const {narration, status} = req.body;
            Transaction.findByIdAndUpdate(req.params.id, {$set: {narration, status}}, { new: true }, (err, transaction) => {
                if (err || !transaction){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update transaction(Narration, Status).', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Transaction updated successfully(Narration, Status)!', transaction);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update transaction');
        }
    },
    deleteTransaction: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Transaction.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete transaction.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Transaction soft deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete transaction by id');
        }
    },
};

module.exports = TransactionController;

function getSearchQuery(req) {
    const { narration, status } = req.body;
    const query = Transaction.find({});
    const queryTerm = [ ];

    if (narration) {
        queryTerm.push({ narration: new RegExp(`^${narration}.*`, "i") });
    }
    if (status) {
        queryTerm.push({ status: status });
    }

    console.log('Query ', queryTerm);
    if (queryTerm.length > 0) {
        console.log('Query  LENGTH', queryTerm.length);
        query.select();
        query.or(queryTerm);
    } else {
        query.select()
    }

    return query;



}