const Testimonial = require('../models/Testimonial');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const TestimonialController = {
    getTestimonials: (req, res) => {
        try {
            Testimonial.find({})
                .exec((err, testimonial) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list testimonials.', err);
                    } else if (validate.resultFound(testimonial, res)) {
                        const data = validate.formatData(testimonial);
                        respHandler.sendSuccess(res, 200, 'Testimonials listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list testimonials.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list testimonials.', err);
        }
    },
    getIsShowingTestimonials: (req, res) => {
        try {
            Testimonial.find({is_showing: true})
                .exec((err, testimonial) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active testimonial.', err);
                    } else if (validate.resultFound(testimonial, res)) {
                        const data = validate.formatData(testimonial);
                        respHandler.sendSuccess(res, 200, 'Testimonial listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get active testimonials.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to get active testimonials.', err);
        }
    },
    createTestimonial: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (userId) => {
                const testimonialObj = req.body;
                testimonialObj.userId = userId;
                Testimonial.create(testimonialObj, (error, testimonial) => {
                    if(error) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create testimonial.', error);
                    } else {
                        respHandler.sendSuccess(res, 200, 'Testimonial created successfully', testimonial);
                    }
                })
            });

        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to add comments as testimonial.', err);
        }
    },
    getTestimonialById: (req, res) => {
        try {
            Testimonial.findById(req.params.id).exec((err, testimonial) => {
                if (err || !testimonial){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get testimonial by id.', err);
                } else if (validate.resultFound(testimonial, res)) {
                    const data = validate.formatData(testimonial);
                    respHandler.sendSuccess(res, 200, 'Testimonial fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get testimonial.', err);
        }
    },
    putTestimonial: (req, res) => {
        try {
            console.log('This is body ', req.body);
            Testimonial.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, testimonial) => {
                if (err || !testimonial){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update testimonial.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Testimonial updated successfully!', testimonial);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update testimonial');
        }
    },
    deleteTestimonial: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Testimonial.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete testimonial.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Testimonial deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete testimonial by id');
        }
    },
};

module.exports = TestimonialController;