const Sponsorship = require('../models/Sponsorship');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const moment = require('moment');
const bcrypt = require('bcrypt');
const encrypt = require('../services/jsEncryptService');
const User = require('../models/Users');
const CompletedSponsor = require('../models/CompletedSponsor');

const SponsorshipController = {
    getSponsorships: (req, res) => {
        try {
            const { page, limit, status } = req.query;
            const {month, year} = req.body;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 900000;
            let cond = {};
            if(status) {
                cond = {status}
            }
            Sponsorship.find(cond)
                .populate('orderId')
                .populate('userId', 'first_name last_name email full_name phone_number')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .sort('expected_end_date')
                .exec((err, sponsorships) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list all sponsorship.', err);
                    } else if (validate.resultFound(sponsorships, res)) {
                        let data;
                        if(req.body && req.body.month) {
                            const matchedFilters = sponsorships.filter(sponsor => {
                                const date = moment(sponsor.expected_end_date);
                                const _month = date.month();
                                // console.log('Year ', date.year(), year, _month, month-1);
                                return ((_month === (month - 1)) && parseInt(year) === date.year());
                            });
                            data = validate.formatData(matchedFilters);
                        } else {
                            data = validate.formatData(sponsorships);
                        }
                        respHandler.sendSuccess(res, 200, 'Sponsorship listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
        }
    },
    getUserSponsorshipHistory: (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 200;
            Sponsorship.find({$and: [{userId: req.params.userId}, {status: {$ne: 'STARTED'}}]})
                .populate('orderId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, sponsorships) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get users sponsorship.', err);
                    } else if (validate.resultFound(sponsorships, res)) {
                        const data = validate.formatData(sponsorships);
                        respHandler.sendSuccess(res, 200, 'Sponsorship listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
        }
    },
    getUserActiveSponsorships: (req, res) => {
        try {
            console.log('USER ', req.params.userId);
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 200;
            Sponsorship.find({$and: [{userId: req.params.userId}, {status: 'STARTED'}]})
                .populate('orderId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, sponsorships) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to get users sponsorship.', err);
                    } else if (validate.resultFound(sponsorships, res)) {
                        const data = validate.formatData(sponsorships);
                        respHandler.sendSuccess(res, 200, 'Sponsorship listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsorships.', err);
        }
    },
    cashOutSponsorship: (req, res) => {
        try {
            const { id, userId } = req.params;
            Sponsorship.findOneAndUpdate({$and: [{userId: userId}, {_id: id}, {status: 'COMPLETED'}]},
                {$set: {status: 'WITHDREW'}}, {new: true})
                .exec((err, sponsorship) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                    } else if (validate.resultFound(sponsorship, res)) {
                        const data = validate.formatData(sponsorship);
                        respHandler.sendSuccess(res, 200, 'Sponsorship updated successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
        }
    },
    markCompletedSponsorship: async (req, res) => {
        try {
            const pos = req.body.pos;
            if(!req.body.password) {
                respHandler.sendError(res, 401, 'FAILURE', 'Not permitted to this resource', {});
            } else {
               const user = await User.findOne({$and: [{email: req.body.email}, {$or: [{role: 'SUPER'}, {role: 'ACCOUNTANT'}]}]}).exec();
                const password = encrypt.decryptString(req.body.password);
                // console.log('PASSWORD ', password, user.password);
                bcrypt.compare(password, user.password, (err, response) => {
                    if(!response) {
                        respHandler.sendError(res, 401, 'FAILURE', 'Not permitted to this resource!', err);
                    } else {
                        Sponsorship.findById(req.body.sponsorId, (e, sponsor) => {
                            if(e || !sponsor) {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', e);
                            } else {
                                const currentDate = moment.now();
                                CompletedSponsor.create({
                                    sponsorId: sponsor._id,
                                    userId: sponsor.userId,
                                    roi_amount: (req.body.type === 'normal') ? sponsor.expected_return : sponsor.roi_data[pos].roi,
                                    roi_date: (req.body.type === 'normal') ? sponsor.expected_end_date : sponsor.roi_data[pos].out_date,
                                    set_date: currentDate,
                                    sponsorType: (req.body.type === 'normal') ? 'default' : 'advance'}, (error, success) => {
                                    if (error) {
                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', e);
                                    } else {
                                        if (req.body.type === 'advance') {
                                            sponsor.roi_data[pos].status = 'COMPLETED';
                                            Sponsorship.findByIdAndUpdate(req.body.sponsorId,
                                                {$set: {roi_data: sponsor.roi_data}}, {new: true})
                                                .exec((err, sponsorship) => {
                                                    if (err) {
                                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                                                    } else {
                                                        respHandler.sendSuccess(res, 200, 'Sponsorship updated successfully', sponsorship);
                                                    }
                                                });

                                            if(sponsor.roi_data[3].status === 'COMPLETED') {
                                                Sponsorship.findByIdAndUpdate(sponsor._id, {$set: {status: 'COMPLETED', completedSponsorRef: success._id, roi_data: sponsor.roi_data}}, {new: true}, () => {});
                                            }
                                        } else {
                                            Sponsorship.findByIdAndUpdate(req.body.sponsorId,
                                                {$set: {status: 'COMPLETED', completedSponsorRef: success._id}}, {new: true})
                                                .exec((err, sponsorship) => {
                                                    if (err) {
                                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                                                    } else {
                                                        respHandler.sendSuccess(res, 200, 'Sponsorship updated successfully', sponsorship);
                                                    }
                                                });
                                        }

                                    }
                                });

                            }
                        });

                    }
                });
            }
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
        }
    },
    cancelSponsorship: (req, res) => {
        try {
            const { id, userId } = req.params;
            Sponsorship.findOneAndUpdate({$and: [{userId: userId}, {_id: id}]},
                {$set: {status: 'CANCELED'}}, {new: true})
                .exec((err, sponsorship) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                    } else if (validate.resultFound(sponsorship, res)) {
                        const data = validate.formatData(sponsorship);
                        respHandler.sendSuccess(res, 200, 'Sponsorship canceled successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
        }
    },
    getSponsorship: (req, res) => {
        try {
            Sponsorship.findById(req.params.id)
                .populate('orderId')
                .exec((err, sponsorship) => {
                if (err || !sponsorship){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsorship by id.', err);
                } else if (validate.resultFound(sponsorship, res)) {
                    const data = validate.formatData(sponsorship);
                    respHandler.sendSuccess(res, 200, 'Sponsorship fetched successfully', data);
                } else {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsorship.', err);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsorship.', err);
        }
    },
    putSponsorship: (req, res) => {
        try {
            const status = req.body.status;
            if(status.toUpperCase() === 'COMPLETED') {
               return respHandler.sendError(res, 401, 'FAILURE', 'Unable to update sponsorship');
            }
            Sponsorship.findByIdAndUpdate(req.params.id, {$set: {status: status}}, { new: true }, (err, sponsorship) => {
                if (err || !sponsorship){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update sponsorship status.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Sponsorship updated successfully!', sponsorship);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship');
        }
    },
    deleteSponsorship: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Sponsorship.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete sponsorship.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Sponsorship deleted successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete sponsorship by id');
        }
    },
};

module.exports = SponsorshipController;