const FarmType = require('../models/FarmType');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const FarmTypeController = {
    getFarmTypes: (req, res) => {
        try {
            FarmType.find({})
                .exec((err, farmType) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm types.', err);
                    } else if (validate.resultFound(farmType, res)) {
                        const data = validate.formatData(farmType);
                        respHandler.sendSuccess(res, 200, 'Farm types listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm types.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list farm types.', err);
        }
    },
    getActiveFarmType: (req, res) => {
        try {
            FarmType.find({status: true})
                .exec((err, farmType) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm type.', err);
                    } else if (validate.resultFound(farmType, res)) {
                        const data = validate.formatData(farmType);
                        respHandler.sendSuccess(res, 200, 'Farm type listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm type.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list active farm type.', err);
        }
    },
    createFarmType: (req, res) => {
        try {
            FarmType.create(req.body, (error, farmType) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create farm type.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm type created successfully', farmType);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new farm type.', err);
        }
    },
    getFarmTypeById: (req, res) => {
        try {
            FarmType.findById(req.params.id).exec((err, farmType) => {
                if (err || !farmType){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm type by id.', err);
                } else if (validate.resultFound(farmType, res)) {
                    const data = validate.formatData(farmType);
                    respHandler.sendSuccess(res, 200, 'Farm type fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get farm type.', err);
        }
    },
    putFarmType: (req, res) => {
        try {
            console.log('This is body ', req.body);
            FarmType.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, farmType) => {
                if (err || !farmType){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update farm type.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Farm type updated successfully!', farmType);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update farm type');
        }
    },
    deleteFarmType: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                FarmType.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete farm type.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Farm type deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete farm type by id');
        }
    },
};

module.exports = FarmTypeController;