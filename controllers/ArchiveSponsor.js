const ArchiveSponsor = require('../models/ArchivedSponsor');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const moment = require('moment');
const bcrypt = require('bcrypt');
const encrypt = require('../services/jsEncryptService');
const User = require('../models/Users');
const CompletedSponsor = require('../models/CompletedSponsor');

const ArchiveSponsorController = {
    getArchiveSponsors: (req, res) => {
        try {
            const status = req.query.status;
            const {month, year} = req.body;
            let cond = {};
            if(status) {
                cond = {order_status: status}
            }
            ArchiveSponsor.find(cond)
                .populate('userId', 'first_name email last_name full_name phone_number')
                .sort('formatted_roi_date')
                .exec((err, sponsors) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsors.', err);
                    } else if (validate.resultFound(sponsors, res)) {
                        let data;
                        if(req.body && req.body.month) {
                            const matchedFilters = sponsors.filter(sponsor => {
                                const date = moment(sponsor.formatted_roi_date);
                                const _month = date.month();
                                console.log('MONTH ', _month, month);
                                return (_month === (month - 1) && parseInt(year) === date.year());
                            });
                             data = validate.formatData(matchedFilters);
                        } else {
                             data = validate.formatData(sponsors);
                        }
                        respHandler.sendSuccess(res, 200, 'Archive sponsor listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsors.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list sponsors.', err);
        }
    },
    getUserArchiveSponsor: (req, res) => {
        try {
            ArchiveSponsor.find({email: req.body.email})
                .populate('userId', 'first_name email last_name full_name phone_number')
                .exec((err, sponsors) => {
                if (err){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsor by user email.', err);
                } else if (validate.resultFound(sponsors, res)) {
                    const data = validate.formatData(sponsors);
                    respHandler.sendSuccess(res, 200, 'Archive sponsor fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsor.', err);
        }
    },
    getArchiveSponsor: (req, res) => {
        try {
            ArchiveSponsor.findById(req.params.id)
                .populate('userId', 'first_name email last_name full_name phone_number')
                .exec((err, sponsor) => {
                if (err || !sponsor){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsor by id.', err);
                } else if (validate.resultFound(sponsor, res)) {
                    const data = validate.formatData(sponsor);
                    respHandler.sendSuccess(res, 200, 'Archive sponsor fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get sponsor.', err);
        }
    },
    putArchiveSponsor: (req, res) => {
        try {
            console.log('This is body ', req.body);
            ArchiveSponsor.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, sponsor) => {
                if (err || !sponsor){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update sponsor.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Archive sponsor updated successfully!', sponsor);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsor');
        }
    },
    deleteArchiveSponsor: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                ArchiveSponsor.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete sponsor.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Archive sponsor deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete sponsor by id');
        }
    },
    markCompletedSponsorship: async (req, res) => {
        try {
            if(!req.body.password) {
                respHandler.sendError(res, 401, 'FAILURE', 'Not permitted to this resource', {});
            } else {
                const user = await User.findOne({$and: [{email: req.body.email}, {$or: [{role: 'SUPER'}, {role: 'ACCOUNTANT'}]}]}).exec();
                const password = encrypt.decryptString(req.body.password);
                bcrypt.compare(password, user.password, (err, response) => {
                    if(!response) {
                        respHandler.sendError(res, 401, 'FAILURE', 'Not permitted to this resource!', err);
                    } else {
                        ArchiveSponsor.findById(req.body.sponsorId, (e, sponsor) => {
                            if(e || !sponsor) {
                                respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', e);
                            } else {
                                const currentDate = moment.now();
                                CompletedSponsor.create({
                                    archiveId: sponsor._id,
                                    userId: sponsor.userId,
                                    roi_amount: sponsor.roi_amount,
                                    roi_date: sponsor.formatted_roi_date,
                                    set_date: currentDate,
                                    email: sponsor.email,
                                    sponsorType: 'default'
                                }, (error, success) => {
                                    if (error) {
                                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', error);
                                    } else {
                                        ArchiveSponsor.findByIdAndUpdate(sponsor._id,
                                            {$set: {order_status: 'COMPLETED', completedSponsorRef: success._id}}, {new: true})
                                            .exec((err, sponsorship) => {
                                                if (err) {
                                                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
                                                } else {
                                                    respHandler.sendSuccess(res, 200, 'Sponsorship updated successfully', sponsorship);
                                                }
                                            });
                                    }
                                });

                            }
                        });

                    }
                });
            }
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update sponsorship.', err);
        }
    }
};

module.exports = ArchiveSponsorController;