const BankSetting = require('../models/BankSetting');
const NextOfKin = require('../models/NextofKin');
const BusinessProfile = require('../models/BusinessProfile');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const paystackService = require('../services/paystackServices');

const ProfileController = {
    getBankSettings: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await BankSetting.countDocuments({}).exec();
            BankSetting.find({})
                .populate('userId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, users) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list bank accounts.', err);
                    }
                    if (validate.resultFound(users, res)) {
                        const data = {
                            data: validate.formatData(users),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Bank account listed successfully', data);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list bank account.', err);
        }
    },
    getNextKins: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await NextOfKin.countDocuments({}).exec();
            NextOfKin.find({})
                .populate('userId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, users) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list next of kins.', err);
                    }
                    if (validate.resultFound(users, res)) {
                        const data = {
                            data: validate.formatData(users),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Next of kin listed successfully', data);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list next of kins.', err);
        }
    },
    getBusinessInfos: async (req, res) => {
        try {
            const { page, limit } = req.query;
            let currentPage = parseInt(page, 10) || 1;
            const currentLimit = parseInt(limit, 10) || 9000;
            const totalCount = await BusinessProfile .countDocuments({}).exec();
            BusinessProfile.find({})
                .populate('userId')
                .skip((currentLimit * currentPage) - currentLimit)
                .limit(currentLimit)
                .exec((err, users) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list business information.', err);
                    }
                    if (validate.resultFound(users, res)) {
                        const data = {
                            data: validate.formatData(users),
                            page: currentPage += 1,
                            limit: currentLimit,
                            total: totalCount
                        };
                        respHandler.sendSuccess(res, 200, 'Business information listed successfully', data);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list business information.', err);
        }
    },
    getBankSetting: (req, res) => {
        try {
            BankSetting.findOne({userId: req.params.userId}).exec((err, bankInfo) => {
                if (err || !bankInfo){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get bank setting.', err);
                }
                if(validate.resultFound(bankInfo, res)) {
                    const data = validate.formatData(bankInfo);
                    respHandler.sendSuccess(res, 200, 'Bank setting fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get bank setting.', err);
        }
    },
    createBankSetting: (req, res) => {
        try {
            if(!req.body.bank_code || !req.body.bank_name) {
                respHandler.sendError(res, 400, 'FAILURE', 'Unable to create bank setting, bank code and name is required.', {});
            } else {
                BankSetting.create(req.body, (err, bankInfo) => {
                    if (err || !bankInfo){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to create bank setting.', err);
                    } else {
                        paystackService.verifyBank(bankInfo, (msg, error) => {
                            respHandler.sendError(res, 401, 'FAILURE', msg, error);
                        }, (msg, data) => {
                            BankSetting.findOneAndUpdate({userId: req.params.userId}, {$set: {account_name: data.data.account_name, verified_account_name: data.data.account_name, verified: true}}, {new: true}, (e, d) => {
                               if(!e){
                                   paystackService.transferRecipient(d);
                               }
                            });
                            respHandler.sendSuccess(res, 200, msg || 'Bank info created successfully!', { bankInfo, data});
                        });
                    }
                });
            }
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to create bank info');
        }
    },
    putBankSetting: (req, res) => {
        try {
            const { _id, userId, bank_name, bank_account, account_name, bvn, documents, bank_code, account_number } = req.body;
            if(_id) {
                if(!req.body.bank_name) {
                    respHandler.sendError(res, 400, 'FAILURE', 'Unable to update bank setting, bank code and name is required.', {});
                } else {
                    paystackService.listBanks((error, body, code) => {
                        if(error) {
                            respHandler.sendError(res, code, 'FAILURE', 'Unable to update bank setting.', error);
                        } else {
                            const allBanks = JSON.parse(body);
                            const selectedBank = allBanks.data.filter((bank) => bank.name.toLowerCase() === req.body.bank_name.toLowerCase());
                            let code = null;
                            if(selectedBank[0]) {
                                 code = selectedBank[0].code
                            }
                            BankSetting.findOneAndUpdate({userId: req.params.userId}, {$set: {
                                userId, bank_name, bank_account, account_name, bvn, documents, bank_code: code, account_number }}, { new: true }, (err, bankInfo) => {
                                if (err || !bankInfo){
                                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update bank info.', err);
                                } else {
                                    paystackService.verifyBank(bankInfo, (msg, error) => {
                                        respHandler.sendError(res, 401, 'FAILURE', msg, error);
                                    }, (msg, data) => {
                                        BankSetting.findOneAndUpdate({userId: req.params.userId}, {$set: {account_name: data.data.account_name, verified_account_name: data.data.account_name, verified: true}}, {new: true}, (e, d) => {
                                            if(!e){
                                                paystackService.transferRecipient(d);
                                            }
                                        });
                                        respHandler.sendSuccess(res, 200, msg || 'Bank info updated successfully!', {bankInfo, data});
                                    });
                                }
                            });
                        }
                    });
                }
            } else {
                ProfileController.createBankSetting(req, res);
            }
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update bank info');
        }
    },



    getNextKin: (req, res) => {
        try {
            NextOfKin.findOne({userId: req.params.userId}).exec((err, nextKin) => {
                if (err || !nextKin){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get next of kin.', err);
                }
                if(validate.resultFound(nextKin, res)) {
                    const data = validate.formatData(nextKin);
                    respHandler.sendSuccess(res, 200, 'Next of kin fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get bank setting.', err);
        }
    },
    createNextKin: (req, res) => {
        try {
            NextOfKin.create(req.body, (err, nextKin) => {
                if (err || !nextKin){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create next of kin.', err);
                }
                respHandler.sendSuccess(res, 200, 'Next of kin created successfully!', nextKin);
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to create next of kin');
        }
    },
    putNextKin: (req, res) => {
        try {
            const { name, phone_number, email, relationship, address, userId, _id } = req.body;
            if(_id) {
                console.log('UPDATE BUSINESS INFORMATION', req.body);
                NextOfKin.findOneAndUpdate({userId: req.params.userId}, {$set: {
                    name, phone_number, email, relationship, address, userId
                }}, {new: true}, (err, nextKin) => {
                    if (err || !nextKin) {
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to update next of kin.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Bank info updated successfully!', nextKin);
                });
            } else {
                ProfileController.createNextKin(req, res);
            }
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update next of kin');
        }
    },



    getBusinessInfo: (req, res) => {
        try {
            BusinessProfile.findOne({userId:req.params.userId}).exec((err, businessInfo) => {
                if (err || !businessInfo){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get business information.', err);
                }
                if(validate.resultFound(businessInfo, res)) {
                    const data = validate.formatData(businessInfo);
                    respHandler.sendSuccess(res, 200, 'Business information fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get business information.', err);
        }
    },
    createBusinessInfo: (req, res) => {
        try {
            BusinessProfile.create(req.body, (err, businessInfo) => {
                if (err || !businessInfo){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create business information.', err);
                }
                respHandler.sendSuccess(res, 200, 'Business information created successfully!', businessInfo);
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to create business information');
        }
    },
    putBusinessInfo: (req, res) => {
        try {
            const {company_name, company_address, company_email, userId, _id, company_phone} = req.body;
            if(_id){
            console.log('UPDATE BUSINESS INFORMATION', req.body);
                BusinessProfile.findOneAndUpdate({userId: req.params.userId}, {$set: {
                    company_name, company_address, company_email, userId, company_phone
                }}, { new: true }, (err, businessInfo) => {
                    if (err || !businessInfo){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to update business info.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Business information updated successfully!', businessInfo);
                });
            } else {
                ProfileController.createBusinessInfo(req, res);
            }

        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update business information');
        }
    }
};

module.exports = ProfileController;
