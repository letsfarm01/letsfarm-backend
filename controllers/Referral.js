const Referral = require('../models/Refarral');
const Users = require('../models/Users');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const mailer = require('../mailing/mailSystem');
const ReferralController = {
    getReferrals: (req, res) => {
        try {
            Referral.find({$and: [{has_joined: true}, {has_sponsor: true}, {is_sponsor_valid: true}]})
                .populate('referree', 'first_name last_name email full_name _id')
                .populate('referral', 'first_name last_name email full_name _id')
                .exec((err, referrals) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list referrals.', err);
                    } else if (validate.resultFound(referrals, res)) {
                        const data = validate.formatData(referrals);
                        respHandler.sendSuccess(res, 200, 'Referral listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list referrals.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list referrals.', err);
        }
    },
    createReferral: (req, res) => {
        try {
            const { email } = req.body;
            controllerService.getLoginUser(req, 'userId', (userId) => {
                Users.findById(userId, (e, user) => {
                    if(user) {
                        Users.countDocuments({email: email}, (error_, count) => {
                            if( !error_ && count < 1) {
                                Referral.create({
                                    referral : user._id,
                                    referral_email: user.email,
                                    referree_email: email,
                                }, (error, referral) => {
                                    console.log('Erro ', error );
                                    if(error && error.message.includes('`email` to be unique')) {
                                        respHandler.sendError(res, 406, 'FAILURE', 'User already referred!', error);
                                    } else if(error && error.message.includes('`email` is invalid')) {
                                        respHandler.sendError(res, 401, 'FAILURE', 'Invalid email address!', error);
                                    } else if(referral) {
                                        mailer.sendHtml({email: email, full_name: email.toString().split('@')[0]},
                                            user.first_name + ' has invited you to join Letsfarm', 'Referral',
                                            {full_name: user.full_name, title: user.first_name + ' has invite you to join Letsfarm',
                                                profile_picture: user.user_image_url ? user.user_image_url : 'https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/logo%2Fimages.png?alt=media&token=700133be-a5b2-4033-9c50-87df27ec2ca5',
                                                url: `${process.env.FRONT_END_URL}sign-up/${user.email}`,
                                                phone_number: user.phone_number, referral_code: user.referral_code});
                                        respHandler.sendSuccess(res, 200, 'Friend referred successfully', {});
                                    }  else {
                                        respHandler.sendError(res, 401, 'FAILURE', 'Unable to refer friend!', error);
                                    }
                                });
                            } else {
                                respHandler.sendError(res, 401, 'FAILURE', 'User already exist on letsfarm!', error_);
                            }
                        });

                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to refer a friend.', e);
                    }
                });
            });

        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to refer a friend.', err);
        }
    },
    getUserReferrals: (req, res) => {
        try {
            Referral.find({$and: [ {referral: req.params.userId}, {is_sponsor_valid: true}]})
                .populate('referree', '-email -password')
                .populate('referral', '-email -password')
                .exec((err, referrals) => {
                    if (err){
                        respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user referree list.', err);
                    } else if (validate.resultFound(referrals, res)) {
                        const data = validate.formatData(referrals);
                        respHandler.sendSuccess(res, 200, 'Referral fetched successfully', data);
                    }
                })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get referral.', err);
        }
    },
    getReferral: (req, res) => {
        try {
            Referral.findById(req.params.id)
                .populate('referree')
                .populate('referral')
                .exec((err, referral) => {
                if (err || !referral){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get referral by id.', err);
                } else if (validate.resultFound(referral, res)) {
                    const data = validate.formatData(referral);
                    respHandler.sendSuccess(res, 200, 'Archive referral fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get user referees.', err);
        }
    },
    deleteReferral: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                Referral.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete referral.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Referee deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete referee by id');
        }
    },
};

module.exports = ReferralController;