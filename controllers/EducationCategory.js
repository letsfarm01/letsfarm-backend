const EduCategory = require('../models/EduCategory');
const respHandler = require('../services/responseHandler');
const validate = require('../services/validateService');
const controllerService = require('../services/controllerServices');

const EduCategoryController = {
    getEduCategory: (req, res) => {
        try {
            EduCategory.find({})
                .exec((err, eduCategory) => {
                    if (err) {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education category.', err);
                    } else if (validate.resultFound(eduCategory, res)) {
                        const data = validate.formatData(eduCategory);
                        respHandler.sendSuccess(res, 200, 'Education Category listed successfully', data);
                    } else {
                        respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education categories.', err);
                    }
                })
        } catch (err) {
            console.log('Error ', err);
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to list education category.', err);
        }
    },
    createEduCategory: (req, res) => {
        try {
            EduCategory.create(req.body, (error, eduCategory) => {
                if(error) {
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to create education category.', error);
                } else {
                    respHandler.sendSuccess(res, 200, 'Education Category created successfully', eduCategory);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to create new education category.', err);
        }
    },
    getEduCategoryById: (req, res) => {
        try {
            EduCategory.findById(req.params.id).exec((err, eduCategory) => {
                if (err || !eduCategory){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education category by id.', err);
                } else if (validate.resultFound(eduCategory, res)) {
                    const data = validate.formatData(eduCategory);
                    respHandler.sendSuccess(res, 200, 'Education Category fetched successfully', data);
                }
            })
        } catch(err) {
            console.log('error ', err);
            respHandler.sendError(res, 404, 'FAILURE', 'Unable to get education category.', err);
        }
    },
    putEduCategory: (req, res) => {
        try {
            console.log('This is body ', req.body);
            EduCategory.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true }, (err, eduCategory) => {
                if (err || !eduCategory){
                    respHandler.sendError(res, 404, 'FAILURE', 'Unable to update education category.', err);
                } else {
                    respHandler.sendSuccess(res, 200, 'Education Category updated successfully!', eduCategory);
                }
            });
        } catch (err){
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to update education category');
        }
    },
    deleteEduCategory: (req, res) => {
        try {
            controllerService.getLoginUser(req, 'userId', (deletedBy) => {
                EduCategory.delete({_id: req.params.id}, deletedBy, (err) => {
                    if (err){
                        return respHandler.sendError(res, 404, 'FAILURE', 'Unable to delete education category.', err);
                    }
                    respHandler.sendSuccess(res, 200, 'Education Category deleted by id successfully!', {});
                });
            });

        } catch (err) {
            respHandler.sendError(res, 400, 'FAILURE', 'Unable to delete education category by id');
        }
    },
};

module.exports = EduCategoryController;