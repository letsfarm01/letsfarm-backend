const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const farmStatusSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    can_sponsor: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


farmStatusSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

farmStatusSchema.plugin(uniqueValidator);

const FarmStatus = mongoose.model('FarmStatus', farmStatusSchema);

// make this available to our Node applications
module.exports = FarmStatus;
