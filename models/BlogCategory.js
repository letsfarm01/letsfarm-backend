const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const blogCategorySchema = new Schema({
    name: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

blogCategorySchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

blogCategorySchema.plugin(uniqueValidator);

const BlogCategory = mongoose.model('BlogCategory', blogCategorySchema);

// make this available to our Node applications
module.exports = BlogCategory;
