const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const shippingAddressSchema = new Schema({
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    phone_number: {
        type: String,
        required: true
    },
    alt_phone_number: {
        type: String,
    },
    default: {
        type: Boolean,
        default: false
    },
    address_line: {
        type: String
    },
    additional_information: {
        type: String
    },
    nearest_bus_stop: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    country: {
        type: String
    },
    postal_code: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    timestamps: true
});


shippingAddressSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

const ShippingAddress = mongoose.model('ShippingAddress', shippingAddressSchema);
// make this available to our Node applications
module.exports = ShippingAddress;
