const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const pushSchema = new Schema({
    pushTo: {// farm Id, All for normal notification
        type: String,
        required: true
    },
    farm_name: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FarmShop'
    },
    read: {
        type: Boolean,
        default: false
    },
    updateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Update'
    }
}, {
    timestamps: true
});


// pushSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
pushSchema.index({ pushTo: 1, updateId: 1 }, { unique: true });
const Push = mongoose.model('Push', pushSchema);

// make this available to our Node applications
module.exports = Push;
