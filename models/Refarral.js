const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const referralSchema = new Schema({
    referral: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    referree: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    referree_email: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
        lowercase: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    referral_email: {
        type: String,
        required: true
    },
    has_joined: {
        type: Boolean,
        default: false
    },
    has_sponsor: {
        type: Boolean,
        default: false
    },
    amount_sponsor: {
        type: Number,
        default: 0
    },
    sponsorId: {
        type: Schema.Types.ObjectId,
        ref: "Sponsorship"
    },
    percentage: {
        type: Number,
        default: 2
    },
    currency: {
        type: String,
        default: 'NGN'
    },
    expected_bonus: {
        type: Number,
        default: 0
    },
    is_cycle_completed: {
        type: Boolean,
        default: false
    },
    is_bonus_earned: {
        type: Boolean,
        default: false
    },
    is_sponsor_valid: {
        type: Boolean,
        default: true
    },
}, {
    timestamps: true
});


referralSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

referralSchema.plugin(uniqueValidator);

const Referral = mongoose.model('Referral', referralSchema);

// make this available to our Node applications
module.exports = Referral;
