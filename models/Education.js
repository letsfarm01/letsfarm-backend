const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const educationSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    message_body: {
        type: String,
        required: true
    },
    author: {
        type: String
    },
    img_link: {
        type: String
    },
    video_link: {
        type: String
    },
    total_likes: {
        type: Number,
        default: 0
    },
    total_dislikes: {
        type: Number,
        default: 0
    },
    comments_count: {
        type: Number,
        default: 0
    },
    publish: {
        type: Boolean,
        default: false
    },
    edu_category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'EducationCategory',
        required: true
    }
}, {
    timestamps: true
});


educationSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
const Education = mongoose.model('Education', educationSchema);
// make this available to our Node applications
module.exports = Education;
