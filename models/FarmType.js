const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const farmTypeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


farmTypeSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

farmTypeSchema.plugin(uniqueValidator);

const FarmType = mongoose.model('FarmType', farmTypeSchema);

// make this available to our Node applications
module.exports = FarmType;
