const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const faqSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    category: {
        type: String,
        enum: ['GENERAL', 'FISH', 'CROPS'],
        default: 'GENERAL'
    },
    active: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


faqSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

faqSchema.plugin(uniqueValidator);

const FAQ = mongoose.model('FAQ', faqSchema);

// make this available to our Node applications
module.exports = FAQ;
