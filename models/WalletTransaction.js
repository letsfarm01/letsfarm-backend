const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const walletTransactionSchema = new Schema({
    narration: {
        type: String,
        required: true
    },
    old_wallet_balance: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        enum: ['DEBIT', 'CREDIT']
    },
    new_wallet_balance: {
        type: Number,
        required: true
    },
    amount: {
        type: Number
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    transactionId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Transaction',
        unique: true,
        required: true
    }
}, {
    timestamps: true
});


walletTransactionSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

walletTransactionSchema.plugin(uniqueValidator);

const WalletTransaction = mongoose.model('WalletTransaction', walletTransactionSchema);

// make this available to our Node applications
module.exports = WalletTransaction;
