const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const deviceNotifySchema = new Schema({
    deviceId: {
        type: String,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
    },
    email: {
        type: String
    }
}, {
    timestamps: true
});


deviceNotifySchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

deviceNotifySchema.plugin(uniqueValidator);

const DeviceIds = mongoose.model('DeviceId', deviceNotifySchema);

// make this available to our Node applications
module.exports = DeviceIds;
