const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const teamGroupSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
    },
    description: {
        type: String
    },
    sort_order: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});


teamGroupSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

teamGroupSchema.plugin(uniqueValidator);

const TeamGroup = mongoose.model('TeamGroup', teamGroupSchema);

// make this available to our Node applications
module.exports = TeamGroup;
