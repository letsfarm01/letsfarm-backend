const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const updateSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    message_body: {
        type: String,
        required: true
    },
    img_link: {
        type: String
    },
    publish: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});


// updateSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
const Update = mongoose.model('Update', updateSchema);
// make this available to our Node applications
module.exports = Update;
