const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const paystackReferenceSchema = new Schema({
    authorization_code: {
        type: String,
        unique: true
    },
    userId: {
        type: String,
        required: true
    },
    card_type: {
        type: String
    },
    last4: {
        type: String,
        unique: true
    },
    exp_month: {
        type: Number
    },
    exp_year: {
        type: Number
    },
    bin: {
        type: String
    },
    bank: {
        type: String
    },
    channel: {
        type: String
    },
    reusable: {
        type: Boolean
    },
    country_code: {
        type: String
    },
    signature: {
        type: String
    }
}, {
    timestamps: true
});

paystackReferenceSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

paystackReferenceSchema.plugin(uniqueValidator);

const PaystackReference = mongoose.model('PaystackReference', paystackReferenceSchema);

// make this available to our Node applications
module.exports = PaystackReference;
