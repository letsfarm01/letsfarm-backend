const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const paymentChannelSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    is_active: {
        type: Boolean,
        default: false
    },
    can_fund: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


paymentChannelSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

paymentChannelSchema.plugin(uniqueValidator);

const PaymentChannel = mongoose.model('PaymentChannel', paymentChannelSchema);

// make this available to our Node applications
module.exports = PaymentChannel;
