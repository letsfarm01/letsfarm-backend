const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const blogCommentSchema = new Schema({
    blogId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Blog',
        required: true
    },
    comment: {
        type: String
    }
}, {
    timestamps: true
});

blogCommentSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

blogCommentSchema.plugin(uniqueValidator);

const BlogComment = mongoose.model('BlogComment', blogCommentSchema);

// make this available to our Node applications
module.exports = BlogComment;
