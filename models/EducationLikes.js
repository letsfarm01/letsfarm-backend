const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
// const mongoose_delete = require('mongoose-delete');

const eduLikesSchema = new Schema({
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    educationId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Education',
        required: true
    },
    actionType: {
        type: String,
        enum: ['LIKE', 'DISLIKE'],
        required: true
    },
    userIdentity: {
        type: String
    }
}, {
    timestamps: true
});

// eduLikesSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

eduLikesSchema.plugin(uniqueValidator);

const EducationLikes = mongoose.model('EducationLike', eduLikesSchema);

// make this available to our Node applications
module.exports = EducationLikes;
