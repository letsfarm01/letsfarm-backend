const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const nextOfKinSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    phone_number: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    relationship: {
        type: String
    },
    address: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});


nextOfKinSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
nextOfKinSchema.index({ userId: 1 }, { unique: true });

const NextOfKin = mongoose.model('NextOfKin', nextOfKinSchema);
// make this available to our Node applications
module.exports = NextOfKin;
