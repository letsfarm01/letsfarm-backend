const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const teamMemberSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    about: {
        type: String
    },
    email: {
        type: String
    },
    position: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    teamGroup: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'TeamGroup',
        required: true
    },
    is_active: {
        type: Boolean,
        default: true
    },
    twitter: {
        type: String
    },
    linkedin: {
        type: String
    },

}, {
    timestamps: true
});


teamMemberSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

teamMemberSchema.plugin(uniqueValidator);

const TeamMember = mongoose.model('TeamMember', teamMemberSchema);

// make this available to our Node applications
module.exports = TeamMember;
