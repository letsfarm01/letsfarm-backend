const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const testimonialSchema = new Schema({
    email: {
        type: String,
        trim: true,
        lowercase: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    full_name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    image_url: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    is_showing: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});


testimonialSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

testimonialSchema.plugin(uniqueValidator);

const Testimonial = mongoose.model('Testimonial', testimonialSchema);

// make this available to our Node applications
module.exports = Testimonial;
