const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const archiveSponsorSchema = new Schema({
    farm_name: {
        type: String,
        required: true
    },
    quantity: {
        type: Number
    },
    email: {
        type: String
    },
    first_name: {
        type: String
    },
    name: {
        type: String
    },
    last_name: {
        type: String
    },
    order_number: {
        type: String
    },
    order_total: {
        type: String
    },
    order_status: {
        type: String
    },
    date_of_sponsor: {
        type: String
    },
    roi_date: {
        type: String
    },
    formatted_roi_date: {
        type: Date
    },
    roi_amount: {
        type: String
    },
    completedSponsorRef: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'CompletedSponsor'
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});


archiveSponsorSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

archiveSponsorSchema.plugin(uniqueValidator);

const ArchiveSponsor = mongoose.model('ArchiveSponsor', archiveSponsorSchema);

// make this available to our Node applications
module.exports = ArchiveSponsor;
