const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');
const moment = require('moment');

const productShopSchema = new Schema({
    product_name: {
        type: String,
        required: true
    },
    product_location: {
        type: String
    },
    price: {
        type: Number,
        default: 0.0,
        required: true
    },
    currency: {
        type: String,
        enum: ['NGN', 'USD'],
        default: 'NGN'
    },
    display_img: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    total_in_stock: {
        type: Number
    },
    remaining_in_stock: {
        type: Number
    },
    average_rating: {
        type: Number
    },
    total_review: {
        type: Number
    },
    product_status: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductStatus',
        required: true
    },
    product_type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductType',
        required: true
    }
}, {
    timestamps: true
});


productShopSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

productShopSchema.pre('save', function (next){
    this.remaining_in_stock = this.total_in_stock;
    next();
});

const FarmProduct = mongoose.model('FarmProduct', productShopSchema);

// make this available to our Node applications
module.exports = FarmProduct;
