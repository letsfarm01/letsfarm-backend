const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const versionSchema = new Schema({
    min_allowed_android_version: {
        type: String,
        required: true
    },
    min_allowed_ios_version: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

versionSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

versionSchema.plugin(uniqueValidator);

const Version = mongoose.model('Version', versionSchema);

// make this available to our Node applications
module.exports = Version;
