const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const businessProfileSchema = new Schema({
    company_name: {
        type: String,
        required: true
    },
    company_email: {
        type: String,
        required: true
    },
    company_address: {
        type: String
    },
    company_phone: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});


businessProfileSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
businessProfileSchema.index({ userId: 1 }, { unique: true });

const BusinessProfile = mongoose.model('BusinessProfile', businessProfileSchema);
// make this available to our Node applications
module.exports = BusinessProfile;
