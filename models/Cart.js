const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');
const FarmShop = require('./FarmShop');
const FarmProduct = require('./FarmProduct');


const cartSchema = new Schema({
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    farmId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'FarmShop',
        // required: true
    },
    productId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'FarmProduct',
    },
    cart_type: { // very important
        type: String,
        default: 'FARM_SHOP',
        enum: ['FARM_SHOP', 'FARM_PRODUCT'],
    },
    quantity: {
        type: Number,
        default: 1
    },
    total_price: {
        type: Number,
        default: 0.0
    },
    checkout_status: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});


cartSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
cartSchema.pre('save', function (next){
    if(this.cart_type === 'FARM_PRODUCT') {
        FarmProduct.findById(this.productId).exec((err, product) => {
            this.total_price = (parseInt(this.quantity, 10) * parseFloat(product.price));
            next();
        });
    } else {
        FarmShop.findById(this.farmId).exec((err, shop) => {
            this.total_price = (parseInt(this.quantity, 10) * parseFloat(shop.amount_to_invest));
            next();
        });
    }
});

cartSchema.plugin(uniqueValidator);

const Cart = mongoose.model('Cart', cartSchema);

// make this available to our Node applications
module.exports = Cart;
