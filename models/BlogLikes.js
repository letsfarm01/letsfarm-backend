const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const blogLikesSchema = new Schema({
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    blogId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Blog',
        required: true
    },
    actionType: {
        type: String,
        enum: ['LIKE', 'DISLIKE'],
        required: true
    },
    userIdentity: {
        type: String
    }
}, {
    timestamps: true
});

blogLikesSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

blogLikesSchema.plugin(uniqueValidator);

const BlogLikes = mongoose.model('BlogLike', blogLikesSchema);

// make this available to our Node applications
module.exports = BlogLikes;
