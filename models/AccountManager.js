const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const accountManagerSchema = new Schema({
    topic: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    resolved: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});


accountManagerSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });


const AccountManager = mongoose.model('AccountManager', accountManagerSchema);

// make this available to our Node applications
module.exports = AccountManager;
