const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const transactionSchema = new Schema({
    narration: {
        type: String,
        required: true
    },
    orderReference: {
        type: String,
        required: true
    },
    total_price: {
        type: Number,
        required: true
    },
    payment_channel: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'PaymentChannel',
        required: true
    },
    payment_proof: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'BankPaymentProof'
    },
    transactionType: {
        type: String,
        enum: ['WALLET', 'OTHERS', 'FARM_PRODUCT'],
        default: 'OTHERS'
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    paymentReference: {
        type: String
    },
    status: {
        type: String,
        enum: ['CREATED', 'INITIATED', 'ON_HOLD', 'CONFIRMED', 'REJECTED', 'FAILED', 'CANCELED'],
        default: 'CREATED'
    },
    paystack_status: {
        type: String
    }
}, {
    timestamps: true
});


transactionSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

transactionSchema.plugin(uniqueValidator);

const Transaction = mongoose.model('Transaction', transactionSchema);

// make this available to our Node applications
module.exports = Transaction;
