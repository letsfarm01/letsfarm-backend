const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const bankSchema = new Schema({
    bank_name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
    },
    bank_code: {
        type: String
    }
}, {
    timestamps: true
});


bankSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

bankSchema.plugin(uniqueValidator);

const Bank = mongoose.model('Bank', bankSchema);

// make this available to our Node applications
module.exports = Bank;
