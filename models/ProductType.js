const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const productTypeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


productTypeSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

productTypeSchema.plugin(uniqueValidator);

const ProductType = mongoose.model('ProductType', productTypeSchema);

// make this available to our Node applications
module.exports = ProductType;
