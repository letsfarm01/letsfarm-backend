const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const productOrderSchema = new Schema({
    product_name: {
        type: String,
        required: true
    },
    orderReference: {
        type: String,
        required: true
    },
    currency: {
        type: String,
        default: 'NGN'
    },
    qty_remaining_before_order: {
        type: Number,
        required: true
    },
    total_price: {
        type: Number,
        required: true
    },
    price_per_unit: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number
    },
    cartType: {
        type: String
    },
    status: {
        type: String,
        enum: ['PROCESSING', 'PENDING_PAYMENT', 'PAID', 'FAILED', 'SHIPPED', 'DELIVERED'],
        default: 'PROCESSING'
    },
    payment_channel: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'PaymentChannel',
        required: true
    },
    shipping_address: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'ShippingAddress'
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    productId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'FarmProduct',
        required: true
    },
    cartId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Cart',
        required: true
    }
}, {
    timestamps: true
});


productOrderSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

productOrderSchema.plugin(uniqueValidator);

const ProductOrders = mongoose.model('ProductOrder', productOrderSchema);

// make this available to our Node applications
module.exports = ProductOrders;
