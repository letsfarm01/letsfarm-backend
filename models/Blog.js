const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');

const blogSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    message_body: {
        type: String,
        required: true
    },
    author: {
        type: String
    },
    top_post: {
        type: Boolean,
        default: false
    },
    img_link: {
        type: String
    },
    large_img_link: {
        type: String
    },
    video_link: {
        type: String
    },
    total_likes: {
        type: Number,
        default: 0
    },
    total_dislikes: {
        type: Number,
        default: 0
    },
    comments_count: {
        type: Number,
        default: 0
    },
    publish: {
        type: Boolean,
        default: false
    },
    blog_category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'BlogCategory'
    }
}, {
    timestamps: true
});


blogSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });
const Blog = mongoose.model('Blog', blogSchema);
// make this available to our Node applications
module.exports = Blog;
