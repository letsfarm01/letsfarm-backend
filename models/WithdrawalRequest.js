const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const withdrawalRequestSchema = new Schema({
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    wallet_version_before_approval: {},
    new_version_after_approval: {},
    bank_name: {
        type: String
    },
    account_number: {
        type: String
    },
    account_name: {
        type: String
    },
    bvn: {
        type: String
    },
    paystackReference: {
        type: String
    },
    paystack: {},
    status: {
        type: String,
        enum: ['PENDING', 'APPROVED', 'REJECTED'],
        default: 'PENDING'
    },
    approvalTime: {
        type: Date
    },
    rejectTime: {
        type: Date
    },
    paymentStatus: {
        type: String,
        enum: [ 'AWAITING_PAYSTACK', 'FAILED', 'SUCCESS', 'PENDING'],
        default: 'AWAITING_PAYSTACK'
    }
}, {
    timestamps: true
});


withdrawalRequestSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

withdrawalRequestSchema.plugin(uniqueValidator);

const WithdrawalRequest = mongoose.model('WithdrawalRequest', withdrawalRequestSchema);

// make this available to our Node applications
module.exports = WithdrawalRequest;
