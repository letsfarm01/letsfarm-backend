const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const sponsorshipSchema = new Schema({
    orderId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Order',
        required: true
    },
    farmId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'FarmShop'
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    completedSponsorRef: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'CompletedSponsor'
    },
    itemRemaining: {
        type: Number
    },
    roi_interval: {
        type: Number
    },
    expected_return: {
        type: Number,
        required: true
    },
    expected_end_date: {
        type: Date
    },
    start_date: {
        type: Date
    },
    status: {
        type: String,
        enum: ['STARTED', 'COMPLETED', 'WITHDREW', 'CANCELED', 'REFUNDED', 'PART_WITHDRAW'],
        default: 'STARTED'
    },
    roi_data: {
        type: Array,
        default: []
    },
    advanceType: {
        type: String,
        enum: ['advance', 'default'],
        default: 'default'
    }
}, {
    timestamps: true
});


sponsorshipSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

sponsorshipSchema.plugin(uniqueValidator);

const Sponsorship = mongoose.model('Sponsorship', sponsorshipSchema);

// make this available to our Node applications
module.exports = Sponsorship;
