const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const contactUsSchema = new Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    name: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});


contactUsSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

contactUsSchema.plugin(uniqueValidator);

const ContactUs = mongoose.model('ContactUs', contactUsSchema);

// make this available to our Node applications
module.exports = ContactUs;
