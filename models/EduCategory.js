const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const eduCategorySchema = new Schema({
    name: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

eduCategorySchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

eduCategorySchema.plugin(uniqueValidator);

const EducationCategory = mongoose.model('EducationCategory', eduCategorySchema);

// make this available to our Node applications
module.exports = EducationCategory;
