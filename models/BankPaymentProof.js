const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const bankPaymentProofSchema = new Schema({
    transactionId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Transaction',
        required: true,
        unique: true,
        dropDups: true
    },
     userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    transactionRef: {
        type: String,
        required: true
    },
    message: {
        type: String
    },
    bank_name: {
        type: String
    },
    account_name: {
        type: String
    },
    amount: {
        type: Number
    },
    documents: {
        type: Array,
        default: []
    },
    is_confirmed: {
        type: Boolean,
        default: false
    },
    condition: {
        type: String
    }
}, {
    timestamps: true
});


bankPaymentProofSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

bankPaymentProofSchema.plugin(uniqueValidator);

const BankPaymentProof = mongoose.model('BankPaymentProof', bankPaymentProofSchema);

// make this available to our Node applications
module.exports = BankPaymentProof;
