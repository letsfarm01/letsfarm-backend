const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');
/*
const nanoid = require('nanoid');
const controllerService = require('../services/controllerServices');
*/

// create a user schema
const userSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    phone_number: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
        lowercase: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    is_verified: {
        type: Boolean,
        default: false
    },
    is_auth_sign_up: {
      type: Boolean,
      default: false
    },
    social_id: {
      type: String
    },
    auth_type: {
        type: String,
        enum: ['NO_AUTH', 'GOOGLE', 'FACEBOOK'],
        default: 'NO_AUTH'
    },
    role: {
        type: String,
        // 'BLOGGER', 'ACCOUNTANT', 'CONTENT_MANAGER'
        enum: ['USER', 'SUPER', 'BLOGGER', 'ACCOUNTANT', 'CONTENT_MANAGER'], //SUPER, never allow anyone create a super user
        default: 'USER'
    },
    privileges: {
      type: Array,
      default: ['user_privilge_ids']
    },
    password: {
        type: String
    },
    referral_email: {
        type: String,
        trim: true,
        uppercase: true,
    },
    user_image_url: {
        type: String
    },
    channel_notice: {
        type: String
    },
    address: {
        type: String
    },
    state_province: {
        type: String
    },
    city: {
        type: String
    },
    country: {
        type: String
    },
    gender: {
        type: String
    },
    dob: {
        type: String
    },
    referral_done: {
        type: Boolean,
        default: false
    },
    referral_code: {
        type: String
    },
    username: {
        type: String
    },
    migrated: {
        type: Boolean,
        default: false
    }
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
}, {
    timestamps: true
});

userSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

userSchema.plugin(uniqueValidator);

userSchema
    .virtual("full_name")
    .get(function() {
        return `${this.first_name} ${this.last_name}`;
    });

const Users = mongoose.model('User', userSchema);

// make this available to our Node applications
module.exports = Users;
