const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');
const moment = require('moment');

const farmShopSchema = new Schema({
    farm_name: {
        type: String,
        required: true
    },
    farm_location: {
        type: String,
        required: true
    },
    percentage_to_gain: {
        type: Number,
        required: true
    },
    amount_to_invest: {
        type: Number,
        default: 0.0,
        required: true
    },
    currency: {
      type: String,
        enum: ['NGN', 'USD'],
        default: 'NGN'
    },
    duration: {
        type: Number,
        required: true
    },
    duration_type: {
        type: String,
        enum: ['DAY', 'WEEK', 'MONTH', 'YEAR'],
        default: 'MONTH'
    },
    display_img: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    alertSent: {
        type: Boolean,
        default: false
    },
    total_in_stock: {
        type: Number
    },
    remaining_in_stock: {
        type: Number
    },
    roi_interval: {
        type: Number
    },
    opening_date: {
        type: Date,
        default: moment(new Date())
    },
    closing_date: {
        type: Date,
        default: moment(new Date()).add(9, 'months')
    },
    number_of_sponsors: {
        type: Number
    },
    farm_status: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FarmStatus',
        required: true
    },
    farm_type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FarmType',
        required: true
    }
}, {
    timestamps: true
});


farmShopSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

farmShopSchema.pre('save', function (next){
   this.remaining_in_stock = this.total_in_stock;
    next();
});

const FarmShop = mongoose.model('FarmShop', farmShopSchema);

// make this available to our Node applications
module.exports = FarmShop;
