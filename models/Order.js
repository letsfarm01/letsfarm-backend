const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const orderSchema = new Schema({
    farm_name: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    duration_type: {
        type: String,
        required: true
    },
    orderReference: {
        type: String,
        required: true
    },
    currency: {
        type: String,
        default: 'NGN'
    },
    roi: {
        type: Number,
        required: true
    },
    qty_remaining_before_order: {
        type: Number,
        required: true
    },
    total_price: {
        type: Number,
        required: true
    },
    price_per_unit: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number
    },
    payment_channel: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'PaymentChannel',
        required: true
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    farmId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'FarmShop',
        required: true
    },
    cartId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Cart',
        required: true
    }
}, {
    timestamps: true
});


orderSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

orderSchema.plugin(uniqueValidator);

const Order = mongoose.model('Order', orderSchema);

// make this available to our Node applications
module.exports = Order;
