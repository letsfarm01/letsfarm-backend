const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const newsletterSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
        lowercase: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    }
}, {
    timestamps: true
});


newsletterSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

newsletterSchema.plugin(uniqueValidator);

const Newsletter = mongoose.model('Newsletter', newsletterSchema);

// make this available to our Node applications
module.exports = Newsletter;
