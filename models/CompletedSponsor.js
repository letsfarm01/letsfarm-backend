const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const completedSponsorSchema = new Schema({
    sponsorId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Sponsorship'
    },
    archiveId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'ArchiveSponsor'
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    email: {
        type: String
    },
    roi_amount: {
        type: Number,
        required: true
    },
    set_date: {
        type: Date
    },
    roi_date: {
        type: Date
    },
    status: {
        type: String,
        enum: ['PENDING', 'PAID', 'UNABLE_TO_UPDATE_WALLET', 'UNABLE_TO_CREATE_WALLET_TRANSACTION'],
        default: 'PENDING'
    },
    sponsorType: {
        type: String,
        enum: ['advance', 'default'],
        default: 'default'
    }
}, {
    timestamps: true
});


completedSponsorSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

completedSponsorSchema.plugin(uniqueValidator);

const CompletedSponsor = mongoose.model('CompletedSponsor', completedSponsorSchema);

// make this available to our Node applications
module.exports = CompletedSponsor;
