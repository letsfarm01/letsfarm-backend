const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const productStatusSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    can_order: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
});


productStatusSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

productStatusSchema.plugin(uniqueValidator);

const ProductStatus = mongoose.model('ProductStatus', productStatusSchema);

// make this available to our Node applications
module.exports = ProductStatus;
