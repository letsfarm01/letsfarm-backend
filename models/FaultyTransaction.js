const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const faultyTransactionSchema = new Schema({
    narration: {
        type: String
    },
    orderReference: {
        type: String,
        required: true
    },
    payment_channel: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'PaymentChannel'
    },
    transactionId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Transaction',
        required: true
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    expected_end_date: {
        type: Date
    },
    expected_start_date: {
        type: Date
    },
}, {
    timestamps: true
});


faultyTransactionSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

faultyTransactionSchema.plugin(uniqueValidator);

const FaultyTransaction = mongoose.model('FaultyTransaction', faultyTransactionSchema);

// make this available to our Node applications
module.exports = FaultyTransaction;
