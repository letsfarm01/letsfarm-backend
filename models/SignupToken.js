const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');


const signupTokenSchema = new Schema({
    token: {
        type: String,
        required: true
    },
    userId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        default: moment(new Date())
    },
    expiredIn: {
        type: Date,
        default: moment(new Date()).add(4, "days")// add expiring date time
    },
    expired: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});


const SignupToken = mongoose.model('SignupToken', signupTokenSchema);

// make this available to our Node applications
module.exports = SignupToken;
