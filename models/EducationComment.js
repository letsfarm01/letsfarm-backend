const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongoose_delete = require('mongoose-delete');

const educationCommentSchema = new Schema({
    educationId: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Education',
        required: true
    },
    comment: {
        type: String
    }
}, {
    timestamps: true
});

educationCommentSchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

educationCommentSchema.plugin(uniqueValidator);

const EducationComment = mongoose.model('EducationComment', educationCommentSchema);

// make this available to our Node applications
module.exports = EducationComment;
