const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_delete = require('mongoose-delete');
const moment = require('moment');
const repaySchema = new Schema({
    year: {
        type: String,
        required: true
    },
    status: {
      type: String,
      enum: ['COMPLETE', 'ONGOING', 'PENDING'],
      default: 'PENDING'
    },
    commencement_date: {
      type: Date,
      default: moment.now()
    },
    completed_date: {
      type: Date,
      default: moment.now()
    },
    month: {
        type: String,
        required: true
    },
    month_number: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
});


repaySchema.plugin(mongoose_delete, { deletedAt : true, deletedBy : true, overrideMethods: true });

const Repay = mongoose.model('Repay', repaySchema);

// make this available to our Node applications
module.exports = Repay;
