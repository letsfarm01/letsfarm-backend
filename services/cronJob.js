const config = require('../config/constants');
const cron = require('node-cron');
const controllerService = require('../services/controllerServices');
const pushNotify = require('../services/pushnotificationService');
const eachAsync = require('each-async');
const request = require('request');
const moment = require('moment');
const nanoid = require('nanoid');
const configAuth = require('../config/constants');
const mailJet = require("../mailing/mailSystem");
const FarmShop = require('../models/FarmShop');
const FarmStatus = require('../models/FarmStatus');
const Referral = require('../models/Refarral');
const Users = require('../models/Users');
const Sponsor = require('../models/Sponsorship');
const ArchiveSponsor = require('../models/ArchivedSponsor');
const CompletedSponsor = require('../models/CompletedSponsor');
const winston = require('../config/winston');

const cronJobServices = {

    runCronJobs: () => {
        console.log('CRON JOB STARTED SUCCESSFULLY!');
        allScheduler();
    },
    updateFarmStatus: () => {
        try {
            FarmStatus.findOne({name: 'Sold Out'}, (error, farmStatus) => {
                if(error) {
                    winston.logger.log('error', `Unable to update farm shop because cron could not fetch status --- ${JSON.stringify(error)}`);
                } else {
                    FarmShop.find({})
                        .populate('farm_type')
                        .populate('farm_status')
                        .exec((err, farmShops) => {
                            if(err) {
                                winston.logger.log('error', `Unable to update farm shop status --- ${JSON.stringify(err)}`);
                            } else {
                                eachAsync(farmShops, (farmShop, i, done) => {
                                    if(farmShop && (
                                        farmShop.remaining_in_stock <= 0
                                            || moment(Date.now()).isAfter(moment(farmShop.closing_date)))) {
                                FarmShop.findByIdAndUpdate(farmShop._id, {$set: {farm_status: farmStatus._id}}, {new: true}, () => {
                                    done();
                                })
                                    } else {
                                        done();
                                    }
                                }, () => {
                                    winston.logger.log('info', `Farm shop up to date ---@ ---${moment.now()}`);
                                });
                            }
                        });
                }
            });
        } catch(e){
            winston.logger.log('error', `Unable to update farm shop status`);
        }
    },
    startPendingFarm: () => {
        try {
            FarmStatus.findOne({name: 'Now Selling'}, (error, farmStatus) => {
                if(error) {
                    winston.logger.log('error', `Unable to update farm shop because cron could not fetch status --- ${JSON.stringify(error)}`);
                } else {
                    FarmShop.find({$and: [{remaining_in_stock: {$gt: 0}}, {farm_status: {$ne: farmStatus._id}}]})
                        .populate('farm_type')
                        .populate('farm_status')
                        .exec((err, farmShops) => {
                            if(err) {
                                winston.logger.log('error', `Unable to update farm shop status --- ${JSON.stringify(err)}`);
                            } else {
                                eachAsync(farmShops, (farmShop, i, done) => {
                                    if(farmShop && (
                                        farmShop.remaining_in_stock > 1
                                            && moment(Date.now()).isAfter(moment(farmShop.opening_date))
                                            && moment(Date.now()).isBefore(moment(farmShop.closing_date)))) {
                                FarmShop.findByIdAndUpdate(farmShop._id, {$set: {farm_status: farmStatus._id, alertSent: true}}, {new: true}, () => {
                                    if(!farmShops.alertSent) {
                                        pushNotify.send(farmShop.farm_name, 'New Farm Opening Alert, Sponsor now!!!');
                                    }
                                    done();
                                })} else {
                                        done();
                                    }
                                }, () => {
                                    winston.logger.log('info', `Start pending farm ---@ ---${moment.now()}`);
                                });
                            }
                        });
                }
            });
        } catch(e){
            winston.logger.log('error', `Unable to update farm shop status`);
        }
    },
    updateSponsorshipStatus: () => {
        try {
            Sponsor.find({$and: [{status: 'STARTED'}, {start_date: {$gt: moment.now()}}] }, (error, sponsors) => {
                if(error) {
                    winston.logger.log('error', `Unable to update sponsor status --- ${JSON.stringify(error)}`);
                } else {
                    eachAsync(sponsors, (sponsor, i, done) => {
                            if(sponsor.advanceType === 'advance') {
                                // handle roi array ...each of them is a payout.
                                // set the status of each after payment
                                handleAdvanceSponsorship(sponsor, i, done);
                            } else {
                                // handle not advance
                                handleNormalSponsorship(sponsor, i, done);
                                }

                    }, (response) => {
                        winston.logger.log('info', `Updating started sponsorship ---@ ---${moment.now()}`);
                        cronJobServices.updateArchiveSponsorshipStatus();
                    } )
                }
            });
        } catch(e){
            winston.logger.log('error', `Unable to update sponsor status`);
        }
    },
    updateArchiveSponsorshipStatus:() => {
        ArchiveSponsor.find({status: 'PROCESSING'}, (error, sponsors) => {
            if(error) {
                winston.logger.log('error', `Unable to update archive sponsor status --- ${JSON.stringify(error)}`);
            } else {
                eachAsync(sponsors, (sponsor, i, done) => {
                    const currentDate = moment.now();
                    const readyDate = sponsor.formatted_roi_date;
                    // const minDate = 7Days;
                    const removeMinDateFromReadyDate = moment(readyDate).subtract(7, 'days');
                    if(moment(currentDate).isSameOrAfter(removeMinDateFromReadyDate)) {
                        CompletedSponsor.create({
                            archiveId: sponsor._id,
                            userId: sponsor.userId,
                            roi_amount: sponsor.expected_return,
                            roi_date: sponsor.expected_end_date,
                            set_date: currentDate,
                            email: sponsor.email,
                            sponsorType: 'default'
                        }, (error, success) => {
                            if(error) {
                                done();
                            } else {
                                ArchiveSponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'COMPLETED', completedSponsorRef: success._id}}, {new: true}, () => {
                                    done();
                                });
                            }
                        });
                    } else {
                        done();
                    }

                }, (response) => {
                    winston.logger.log('info', `Updating started sponsorship ---@ ---${moment.now()}`);
                } )
            }
        });
    },
    checkPendingReferral: () => {
        try {
            Referral.find({$and: [{has_joined: true}, {has_sponsor: false}]}, (error, referrals) => {
                if(error || !referrals.length) {
                    // do nothing about error
                } else {
                    eachAsync(referrals, (referral, i, done) => {
                        Users.findOne({$and: [{_id: referral.referree}, {referral_done: false}]}, (error, userFound) => {
                            if(error || !userFound) {
                                done(); // they have paid their dues
                            } else {
                                // they have not pay the user who refer them
                                Sponsor.findOne({userId: userFound._id})
                                    .populate('orderId', 'total_price')
                                    .exec((sError, sponsor) => {
                                    if(sError || !sponsor) {
                                        done(); // they are yet to sponsor a farm
                                    } else {
                                        // referree has sponsor a farm, time to gift to referral
                                        const percentage = parseFloat(referral.percentage || 0);
                                        const bonus = (parseFloat(percentage / 100) * parseFloat(sponsor.orderId.total_price)) || 0;
                                        let is_sponsor_valid = true;
                                        if(parseFloat(sponsor.orderId.total_price) < 50000 ) {
                                            is_sponsor_valid =  false;
                                        }
                                        Referral.findByIdAndUpdate(referral._id, {$set: {
                                            has_sponsor: true,
                                            amount_sponsor: sponsor.orderId.total_price,
                                            sponsorId: sponsor._id,
                                            expected_bonus: (is_sponsor_valid) ? bonus : 0,
                                            is_sponsor_valid: is_sponsor_valid
                                        }}, {new: true}, (uError, referralUpdate) => {
                                            if(uError) {
                                                done();
                                            } else {
                                                // update User referral_done
                                                Users.findByIdAndUpdate(userFound._id, {$set: {referral_done: true}}, {new: true}, () => {
                                                    done();
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    }, () => {
                        winston.logger.log('info', `Cron job for referral manager completed @ ---${moment.now()}`);
                    });
                }
            });
        } catch(e){
            winston.logger.log('error', `Unable to manage pending referral`);
        }
    }


};
module.exports = cronJobServices;
function handleAdvanceSponsorship(sponsor, i ,  done) {

    eachAsync(sponsor.roi_data, (roi_data, j , finish) => {
        if(roi_data.status ===  'STARTED') {
            const currentDate = moment.now();
            const readyDate = roi_data.out_date;
            // const minDate = 7Days;
            const removeMinDateFromReadyDate = moment(readyDate).subtract(4, 'days');
            if(moment(currentDate).isSameOrAfter(removeMinDateFromReadyDate)) {
                // this means sponsorship is complete
                // move the sponsorship to the fundWallet model so the admin can click fund wallet api

                // sponsorId
                // userId
                // roi_amount
                // roi_date
                // set_date
                // archiveId
                // sponsorType
                // email

                CompletedSponsor.create({
                    sponsorId: sponsor._id,
                    userId: sponsor.userId,
                    roi_amount: roi_data.roi,
                    roi_date: roi_data.out_date,
                    set_date: currentDate,
                    sponsorType: 'advance'}, (error, success) => {
                    if(error) {
                        finish();
                    } else {
                        sponsor.roi_data[j].status = 'COMPLETED'; // = {...sponsor.roi_data[j], }
                        if(sponsor.roi_data[3].status === 'COMPLETED') {
                          Sponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'COMPLETED', completedSponsorRef: success._id, roi_data: sponsor.roi_data}}, {new: true}, () => {
                              finish();
                          });
                        } else {
                            finish(sponsor.roi_data);
                        }
                    }
                });
            } else {
                finish();
            }
        } else {
            finish();
        }
    }, (response) => {
        if(response) {
            Sponsor.findByIdAndUpdate(sponsor._id, {$set: {roi_data: response}}, {new: true}, () => {
                done();
            });
        } else {
            done();
        }
    })
}
function handleNormalSponsorship(sponsor, i, done) {
    const currentDate = moment.now();
    const readyDate = sponsor.expected_end_date;
    // const minDate = 7Days;
    const removeMinDateFromReadyDate = moment(readyDate).subtract(7, 'days');
    if(moment(currentDate).isSameOrAfter(removeMinDateFromReadyDate)) {
        // this means sponsorship is complete
        // move the sponsorship to the fundWallet model so the admin can click fund wallet api

        // sponsorId
        // userId
        // roi_amount
        // roi_date
        // set_date
        // archiveId
        // sponsorType
        // email

        CompletedSponsor.create({
            sponsorId: sponsor._id,
            userId: sponsor.userId,
            roi_amount: sponsor.expected_return,
            roi_date: sponsor.expected_end_date,
            set_date: currentDate,
            sponsorType: 'default'}, (error, success) => {
            if(error) {
                 done();
            } else {
                Sponsor.findByIdAndUpdate(sponsor._id, {$set: {status: 'COMPLETED', completedSponsorRef: success._id}}, {new: true}, () => {
                    done();
                });
            }
        });
    } else {
        done();
    }
}
function allScheduler() {
    // 30 12 * * * - every 12:30
    // * * * * * * - every seconds
    // * * * * * - every minutes
    // https://www.npmjs.com/package/node-cron
    const task = cron.schedule('0 21 * * *', () => {
        console.log('Running a job at 9:00PM at Europe/London timezone');
        cronJobServices.updateFarmStatus();
    }, {
        scheduled: false
    });
    task.start();

    const setCompletedFarmTask = cron.schedule('0 23 * * *', () => {
        console.log('Running a job at 11:00PM at Europe/London timezone');
        cronJobServices.updateSponsorshipStatus();
    }, {
        scheduled: false
    });
    setCompletedFarmTask.start();


    const startFarmTask = cron.schedule('0 0 0 * * *', () => {
        console.log('Running a job at 12:00AM at Europe/London timezone');
        cronJobServices.startPendingFarm();
    }, {
        scheduled: false
    });
    startFarmTask.start();

    const checkPendingReferral = cron.schedule('56 20 * * *', () => {
        console.log('Running a job at 08:30PM at Europe/London timezone');
        cronJobServices.checkPendingReferral();
    }, {
        scheduled: false
    });
    checkPendingReferral.start();
}



/*

function sendTestMail() {
    let recipient = [
        "arokoyuolalekan@gmail.com", "olalekan.arokoyu@upperlink.ng"];
    const timeIn =  "09:89";
    mailJet.nodeMailer({subject: 'Upperlink Attendance',
        message: `Upperlink Limited has requested you give a reason for your lateness/absence on:
<br>
Time IN: 80:98
<br>
Log Date: 544567890
<br>
Attendance: ukjhghjk
<br>
Lateness Charge: 87654
<br><br>
    Here is a <a href="654567890-">link</a> for you to make comment on the reason for your action.
    <br><br>
    Please click <a href="https://biochoice.ng/login">here</a> to login to your account to view daily attendance.`,
        name: `Arokoyu Olalekan`,  title: "Upperlink Attendance",
        recipients: recipient})
}

function dailyMailScheduler() {
    // 0 9 * * * - every 09:00
    // * * * * * * - every seconds
    // * * * * * - every minutes
    // https://www.npmjs.com/package/node-cron
    const task = cron.schedule('0 9 * * *', () => {
        console.log('Running a job at 10:00 at Europe/London timezone');
        createDailyAttendance();
    }, {
        scheduled: false
    });
    task.start();
}
function commentChecker() {
    // 0 9 * * * - every 09:00
    // * * * * * * - every seconds
    // * * * * * - every minutes
    // https://www.npmjs.com/package/node-cron
    // Every Hour check number of comments in that month
    cron.schedule('*!/2 * * * *', () => {
        console.log('running every 1 minutes');
        checkForNumberOfComment()
    });
}

function doSystemBackup() {
    // 0 9 * * * - every 09:00
    // * * * * * * - every seconds
    // * * * * * - every minutes
    // * * 1 * * - every 1st of each month
    // * * * * Sunday - every sunday of each week

    // https://www.npmjs.com/package/node-cron
    // Every Hour check number of comments in that month
    const db = cron.schedule('30 12 * * Sunday', () => {
        console.log('running every sunday of the week');
        backupSystem();
    }, {
        scheduled: false
    });
    db.start();
    
}

function sendMailToAdminMonthly() {
    // 0 9 * * * - every 09:00
    // * * * * * * - every seconds
    // * * * * * - every minutes
    // https://www.npmjs.com/package/node-cron
    // Every Hour check number of comments in that month
   const t =  cron.schedule('* * 22 * *', () => {
        console.log('running every 22nd of each month');
        sendAdministrativeMail();
    }, {
        scheduled: false
    });
   t.start();
}*/
