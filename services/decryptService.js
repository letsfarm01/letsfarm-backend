const CryptoJS = require("crypto-js");

exports.decrypt = {
    object: (cipherText, secretKey = process.env.SECRET_KEY) => {
        const bytes  = CryptoJS.AES.decrypt(cipherText.toString(), secretKey);
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    },
    string: function (cipherText, secretKey=process.env.SECRET_KEY){
        const bytes  = CryptoJS.AES.decrypt(cipherText.toString(), secretKey);
        return bytes.toString(CryptoJS.enc.Utf8);
    }
};
