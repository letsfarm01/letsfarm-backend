const Tokens = require('../models/Tokens');
const Users = require('../models/Users');
const jwt = require('jsonwebtoken');
const config = require('../config/constants');
const moment = require('moment');
const fs = require('fs');
const nanoid = require('nanoid');
const ba64 = require("ba64");
const jwtService = require('../services/jwtService');
const jsEncryptService = require('../services/jsEncryptService');
const mailer = require('../mailing/mailSystem');
const winston = require('../config/winston');
const jwtDecode = require('jwt-decode');

const controllerServices = {
    checkAuthorizationToken: function (req, res, next, role=['USER', 'SUPER']) {
            // console.log('Authorization: ', req.headers.authorization);
            if (req.headers && req.headers.authorization) {
                const authorization = req.headers.authorization;
                try {
                    const decodedUser = jwtService.jwt.decodeJWT(authorization).catch((e) => {
                        sendError(res, 401, 'FAILURE', 'Faulty Token :___: ' + e.message );
                    });
                    decodedUser.then((result) => {
                        if(result){
                            confirmDataValidation(result.data, req, res, next, role);
                        } else {}
                    });
                } catch (e) {
                    sendError(res, 401, 'FAILURE', 'User not authorized to perform action.');
                }
            } else {
                sendError(res, 401, 'FAILURE', 'Authorization failed, token not provided');
            }
    },
    validateFundWallet: (req, res, next) => {
        // console.log('HEADERS  = = Letsfarm', req.headers.letsfarm, 'STAMP === ', req.headers.stamp);
        if (req.headers && req.headers.letsfarm && req.headers.stamp) {
           const decryptedData =  jsEncryptService.decryptString(req.headers.letsfarm);
           // console.log('decryptedData = = ', decryptedData);
           const stampTime = jsEncryptService.decryptString(req.headers.stamp.split('}-')[1]);//'AROKOYU OLALEKAN OJO {arokoyuolalekan@gmail.com}';//req.headers.stamp;
           const stampName = req.headers.stamp.split(' {')[0];//'AROKOYU OLALEKAN OJO {arokoyuolalekan@gmail.com}';//req.headers.stamp;
          console.log('STAMP', stampTime);
           if(stampName !== 'AROKOYU OLALEKAN OJO'){
               sendError(res, 401, 'FAILURE', 'Unknown error occurred, please retry.');
           } else if(!moment(stampTime).isBetween(moment().subtract(6, 'days'), moment().subtract(3, 'days'))) {
                sendError(res, 401, 'FAILURE', '2 Unknown error occurred, please retry.');
            } else {
               try {
                   const decodedUser = jwtDecode(decryptedData);
                   if(decodedUser) {
                       if(decodedUser && decodedUser.secretToken === 'GeOOHiHY~7Ix_e7zJTxlz' && decodedUser.accessKey === '12384-09875poiuyty-987to890pojrt9-2976tred;sdfguytr08j-987to890pojrt9-2tred;sdfguytr08j-987to890pojrt9-2tred;sdfguytr08j-arokoyuolalekan'){
                           next();
                       } else {
                           sendError(res, 401, 'FAILURE', '3 Unknown error occurred, please retry.');
                       }
                   } else {
                       sendError(res, 401, 'FAILURE', 'User not authorized to perform action.');
                   }
               } catch (e) {
                   sendError(res, 401, 'FAILURE', 'User not authorized to perform action.');
               }
            }
        } else {
            sendError(res, 401, 'FAILURE', 'Unknown error occurred, please retry.');
        }
    },
    uploadBase64: function (userId, imageObject) {
        try {
            const imageName = 'image-' + nanoid();

            // // console.log('Uploading files ', imageObject.profile_picture);
            const image64 = imageObject.profile_picture;
            const data = decodeBase64Image(image64);
            // console.log('Data =>  ', data);

            const imageTypeDetected = data.type.match(/\/(.*?)$/);
            // console.log('Image Type ', imageTypeDetected);
            const userUploadedImagePath   = 'public_html/biochoice_api/public/images/' + imageName + '.' + imageTypeDetected[1];
            // console.log('Image Save Url ', userUploadedImagePath);

            // Save decoded binary image to disk
            try {
                // console.log('Data Retrieved ', data.data);
                fs.writeFile(userUploadedImagePath, data.data, () =>
                {
                    // console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
                })
            } catch (error) {
                // console.log('Errrororor ', error);
            }
        } catch (error) {
            // console.log('Erro =>>> ', error);
        }
    },
    usingBa64: function (id, image, folder, cb) {
       try {
           const imageName = 'image-' + nanoid().split('-').join('_');
           // // console.log('Directio ', __dirname);

           if(!image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)){ // if image base64 identify have been striped off e.g from android
               image = 'data:image/png;base64,'+ image;
           }

           const directory = __dirname.split('services').join('');
           let ext = ba64.getExt(image);
           console.log('Extension here ', directory, ext);
           ba64.writeImage(`${directory}/public/images/${imageName}`, image, function(err){
               if (err) throw err;
                cb(`${process.env.IMAGE_URL}${imageName}.${ext}`);
           });
       } catch (err) {
           console.log('Error ', err);
       }
    },
    calculateTransactionCharge: (amount, cb) => {
        let amountToPay = parseFloat(amount);
        let amountCap = 2500;
        let charge = 0;
        let flatFee = 0.0;
        let decimalFee = (1.5 / 100);
        if(amountToPay >= amountCap) {
            flatFee = 100;
        }
        let finalAmount = +((amountToPay + flatFee) / (1 -  decimalFee)).toFixed(2);

        charge  = +(finalAmount - amountToPay).toFixed(2);
        if(charge > 2000) {
            charge = 2000;
            finalAmount = amountToPay + charge;
        }
        cb(charge, amountToPay, finalAmount);
    },
    convertDataToJWT: function(data) {
        const jsonData = JSON.parse(JSON.stringify(data));
        return jwt.sign(jsonData, config.SECRETENTITY, {
            expiresIn: '1 year' // expires in 1 year
        });
    },
    getRandomInt: (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    },
    sendToAdmins: (message, title, subjectParam) => {
        Users.find({role: 'SUPER'}, (e, users) => {
            let recipients = [];
            for(const user of users) {
                recipients.push({
                    "Email": user.email,
                    "Name": user.full_name
                });
            }
            winston.logger.log('info', `${message} -- we alert all ADMINS-${JSON.stringify(recipients)}`);
            mailer.sendHtmlManyTo(recipients,
                subjectParam, 'General', {full_name: `Letsfarm Adminstrators`,
                    title: title, message: message});
        });
    },
    getLoginUser: (req, key, cb = null) => {
        const auth = req.headers.authorization;
        let user;
        if(auth) {
            user = jwtService.jwt.decodeJWT(auth);
            user.then((result) => {
                // console.log('result.data ', result.data);
                if(cb) {
                    cb(result.data[key]);
                } else {
                    return result.data[key];
                }
            }).catch(() => {
                if(cb) {
                    cb({});
                } else {
                    return {};
                }
            })
        }
    }
};
module.exports = controllerServices;

function confirmDataValidation(decodedUser, req, res, next, role) {
    // checkSecretValidity(decodedUser, req, res, next);
    checkSecretValidityV1(decodedUser, req, res, next, role)
}

function checkSecretValidityV1(decodedUser, req, res, next, role) {
    // console.log('decodedUser', decodedUser);
    if(decodedUser.accessKey === process.env.ACTION_KEY){
        checkSecretValidity(decodedUser, req, res, next, role);
    } else {
        sendError(res, 401, 'FAILURE', 'Authorization failed, token is not valid or has expired');
    }
    return false;

}
function checkSecretValiditySystem(decodedUser, req, res, next) {
    // console.log('decodedUser');
    if(decodedUser === config.SYSTEM_KEY){
        next();
    } else {
        sendError(res, 401, 'FAILURE', 'Authorization failed, token is not valid or has expired');
    }
    return false;
}
function checkSecretValidity(decodedUser, req, res, next, role) {
    // // console.log('Decoded User ,', decodedUser);
    Tokens.findOne({$and: [{token: decodedUser.secretToken}, {userId: decodedUser.userId}]}, (err, tokenSet) => {
        // console.log('Savwe the token ', tokenSet);
        if(!tokenSet) {
            sendError(res, 401, 'FAILURE', 'Authorization failed, token is not valid or has expired');
        } else {
            if (tokenSet.expired === true) {
                deleteTheToken(decodedUser, tokenSet._id, req, res, next);
            } else if(tokenSet && moment(Date.now()).isAfter(moment(tokenSet.expiredIn))) {
                // console.log('failed token', tokenSet);
                deleteTheToken(decodedUser, tokenSet._id, req, res, next);
            } else {
                console.log('Roles ', role, tokenSet.role, (role.constructor.name === 'Array' && role.includes(tokenSet.role)), (role.constructor.name === 'String' && role === tokenSet.role));
                if((role.constructor.name === 'Array' && role.includes(tokenSet.role)) || (role.constructor.name === 'String' && role === tokenSet.role)){
                    updateSecretToken(decodedUser,tokenSet._id, req, res, next)
                } else {
                    sendError(res, 401, 'FAILURE', 'Authorization failed, only admin can perform this action.');
                }
               /* if(role === 'SUPER') {
                   if(tokenSet.role === 'SUPER' || tokenSet.role === 'BLOGGER') {
                       updateSecretToken(decodedUser,tokenSet._id, req, res, next)
                   } else {
                       sendError(res, 401, 'FAILURE', 'Authorization failed, only admin can perform this action.');
                   }
                } else {
                    updateSecretToken(decodedUser,tokenSet._id, req, res, next)
                }*/
                // console.log('---- update', tokenSet);
            }
        }
    });
    return false;
}

function deleteTheToken(decodedUser, token, req, res, next) {
    Tokens.findOneAndRemove({$or: [{_id: token},{userId: decodedUser.userId}]}, (err, tokenSet) => {
        sendError(res, 401, 'FAILURE', 'Token timeout');
    });
}

function updateSecretToken(decodedUser, token, req, res, next) {
// set either expired or lastUsed
    const addHours =  moment(Date.now()).add(9, 'hours');
    Tokens.findByIdAndUpdate({_id: token}, {$set: {lastUsed: Date.now(), expiredIn: addHours, expired: false}}, {new: true},
        () => {
            next();
        });
}


function sendError (res, code, status, message, error){
    const err = new Error(message);
    err.status = status;
    err.code = code;
    err.msg = message;
    (error) ? err.ERROR = error : '';
    res.status(code).send(err);
}


// Source: http://stackoverflow.com/questions/20267939/nodejs-write-base64-image-file
function decodeBase64Image(dataString)
{
    const matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    const response = {};

    if (matches.length !== 3)
    {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}
