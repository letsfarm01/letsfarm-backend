const JSEncrypt = require("node-jsencrypt");
const jsEncryptService =  {
    decryptData: (input) => {
        try {
            if (!input) {
                return null;
            }
            const decrypt = new JSEncrypt();
            decrypt.setPrivateKey(process.env._PRIVATE_KEY);
            const decrypted = decrypt.decrypt(input);
            // tslint:disable-next-line:no-console
            // console.log("DECRYPTED IN SERVICE", decrypted);
            return JSON.parse(decrypted);
        } catch (e) {
            return null;
        }
    },
     decryptString: (input = null) => {
        try {
            if (!input) {
                return null;
            }
            const decrypt = new JSEncrypt();
            decrypt.setPrivateKey(process.env._PRIVATE_KEY);
            const decrypted = decrypt.decrypt(input);
            // tslint:disable-next-line:no-console
            // console.log("DECRYPTED IN SERVICE", decrypted);
            return decrypted;
        } catch (e) {
            return null;
        }
    },
    encryptString: (input) => {
        try {
            if (!input) {
                return null;
            }
            const encrypt = new JSEncrypt();
            encrypt.setPublicKey(process.env._PUBLIC_KEY);
            return encrypt.encrypt(input);
        } catch (e) {
            return null;
        }
    }

};
module.exports = jsEncryptService;
