const controllerService = require('../services/controllerServices');
const eachAsync = require('each-async');
const request = require('request');
const DeviceId = require('../models/DevicesId');

const notificationServices = {
 send: async (title="Letsfarm Update", message="Lookout for new farm opening!", data={}) => {
     const registrationToken = [];
     const deviceIds = await DeviceId.find({}).exec();
     deviceIds.forEach((device) => {
         registrationToken.push(device.deviceId); // just for fun
         sendNotificationToUser(device.deviceId, title, message, (res) => {
             console.log('Notification sent ', res.body);
         }, data);
     });



 }
};
module.exports = notificationServices;


function filterBadTokens(token) {
    DeviceId.findOneAndDelete({deviceId: token}, () => {});
}

function sendNotificationToUser(token, title, message, onSuccess, data) {
    request({
        url: 'https://fcm.googleapis.com/fcm/send',
        method: 'POST',
        headers: {
            'Content-Type' :' application/json',
            'Authorization': process.env.messagingSenderId
        },
        body: JSON.stringify({
            notification: {
                title: title,
                body: message,
                icon: "ic_launcher",
            },
            icon: "ic_launcher",
            // color: 'green',
            topic: 'Letsfarm',
            // picture: 'https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2Fimage52.77098775888138?alt=media&token=e61a2545-9792-4077-8fa7-82bc9a7ef13e',
            // image: 'https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2Fimage52.77098775888138?alt=media&token=e61a2545-9792-4077-8fa7-82bc9a7ef13e',
            // contentAvailable: true,
            collapse_key: "type_f",
            restrictedPackageName: 'co.letsfarm.letsfarm',
            // delayWhileIdle: true,
            timeToLive: 60 * 3 * 24,
            to : token,
            android: {
                priority: 'high'
            },
            // data: data, //you can send only notification or only data(or include both)
            apns: {
                headers: {
                    "apns-priority": 5
                }
            }
        })
    }, function(error, response, body) {
        if (error) {
            console.error('Unable to notify this token : -' + token, error);
            filterBadTokens(token);
        } else if (response.statusCode >= 400) {
            filterBadTokens(token);
            console.error('22 HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage);
        } else {
            onSuccess(response);
        }
    });
}
