const CryptoJS = require("crypto-js");

exports.encrypt = {
    object: (data, secretKey = process.env.SECRET_KEY) => {
        return CryptoJS.AES.encrypt(JSON.stringify(data), secretKey);
    },
    string: function (data, secretKey=process.env.SECRET_KEY){
        return CryptoJS.AES.encrypt(data, secretKey);
    }
};
