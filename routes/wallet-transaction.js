const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const WalletTransaction = require('../controllers/WalletTransaction');
const validator = require('express-validator/check');
const WalletTransactionRouter = express.Router();

/**
 *  Generic Wallet Transaction apis.
 *  */
WalletTransactionRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => WalletTransaction.getWalletTransactions(req, res, next));

WalletTransactionRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => WalletTransaction.getWalletTransaction(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => WalletTransaction.putWalletTransaction(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => WalletTransaction.deleteWalletTransaction(req, res, next));

WalletTransactionRouter.route('/user/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => WalletTransaction.getUserWalletTransactions(req, res));

WalletTransactionRouter.route('/filter-transaction')
    .post((req, res, next) => WalletTransaction.filterWalletTransaction(req, res));

WalletTransactionRouter.route('/search-transaction')
    .post((req, res, next) => WalletTransaction.searchWalletTransaction(req, res));

module.exports = WalletTransactionRouter;
