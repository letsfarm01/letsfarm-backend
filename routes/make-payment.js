
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const FundWallet = require('../controllers/FundWallet');
const validator = require('express-validator/check');
const FundWalletRouter = express.Router();

/**
 *  Generic Fund Wallet apis.
 *  */
FundWalletRouter.route('/')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => FundWallet.createFundWallet(req, res));
FundWalletRouter.route('/charge-existing-card')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => FundWallet.fundWalletWithExitingCard(req, res));
FundWalletRouter.route('/paystack-verify-payment')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => FundWallet.verifyPayment(req, res));

FundWalletRouter.route('/confirm-bank-transfer')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => FundWallet.confirmBankTransfer(req, res));

FundWalletRouter.route('/cancel-proof')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => FundWallet.cancelTransaction(req, res));

FundWalletRouter.route('/uncancel-proof')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => FundWallet.reInitTransaction(req, res));

FundWalletRouter.route('/saved-cards/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
    if(validateService.validateObjectId(res, req)) {
        controllerService.checkAuthorizationToken(req, res, next);
    }
}, (req, res, next) => FundWallet.getUserSavedCards(req, res, next));
FundWalletRouter.route('/saved-cards/:id')
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => FundWallet.deleteSavedCard(req, res, next));

module.exports = FundWalletRouter;
