
const express = require('express');
const controllerService = require('../services/controllerServices');
const AM = require('../controllers/AccountManager');
const AccountManagerRouter = express.Router();
const validateService = require('../services/validateService');
const validator = require('express-validator/check');
/**
 *  Generic AccountManager apis.
 *  */
AccountManagerRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => AM.getAccountManagers(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => AM.createMessage(req, res));

AccountManagerRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => AM.resolveMessage(req, res, next));
module.exports = AccountManagerRouter;
