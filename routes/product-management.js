
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const productManager = require('../controllers/ProductManager');
const validator = require('express-validator/check');
const productManagerRouter = express.Router();

/**
 *  Generic product Manager apis.
 *  */
productManagerRouter.route('/farm-product')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => productManager.checkoutProductCart(req, res));

productManagerRouter.route('/charge-existing-card')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => productManager.checkoutWithExitingCard(req, res));

productManagerRouter.route('/paystack-verify')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => productManager.verifyPayment(req, res));


module.exports = productManagerRouter;
