
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Blog = require('../controllers/Blog');
const validator = require('express-validator/check');
const BlogRouter = express.Router();

/**
 *  Generic Blog apis.
 *  */
BlogRouter.route('/')
    .get((req, res, next) => Blog.getBlogs(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => Blog.createBlog(req, res));
BlogRouter.route('/normal-posts')
    .get((req, res, next) => Blog.getNormalBlogs(req, res, next));
BlogRouter.route('/top-post')
    .get((req, res, next) => Blog.getTopPost(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => Blog.makeTopPost(req, res, next));

BlogRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            Blog.getBlogById(req, res, next)
        }
    })
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => Blog.putBlog(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => Blog.deleteBlog(req, res, next));

BlogRouter.route('/search-blog')
    .post((req, res, next) => Blog.searchBlog(req, res));

BlogRouter.route('/filter-blog')
    .post((req, res, next) => Blog.filterBlog(req, res));

module.exports = BlogRouter;
