const express = require('express');
const router = express.Router();

/* swagger. */
router.get('/', (req, res) => {
    res.redirect(`${process.env.BASE_URL}${process.env.VERSION}/swagger-doc-letsfarm`)
});
module.exports = router;
