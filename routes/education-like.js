
const express = require('express');
const EducationLike = require('../controllers/EducationLike');
const EducationLikeRouter = express.Router();

/**
 *  Generic EducationLike apis.
 *  */
EducationLikeRouter.route('/')
   .post((req, res, next) => EducationLike.likePost(req, res));
EducationLikeRouter.route('/check-user-status')
   .post((req, res, next) => EducationLike.checkFeelingOnPost(req, res));

module.exports = EducationLikeRouter;
