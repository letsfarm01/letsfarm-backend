
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Order = require('../controllers/Order');
const validator = require('express-validator/check');
const OrderRouter = express.Router();

/**
 *  Generic Order apis.
 *  */
OrderRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => Order.getOrders(req, res, next));

OrderRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Order.getOrder(req, res, next));

OrderRouter.route('/order-reference/:ref')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Order.getOrderByRef(req, res));

OrderRouter.route('/user/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Order.getUserOrders(req, res));

module.exports = OrderRouter;
