
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const EduCategory = require('../controllers/EducationCategory');
const validator = require('express-validator/check');
const EduCategoryRouter = express.Router();

/**
 *  Generic EduCategory apis.
 *  */
EduCategoryRouter.route('/')
    .get((req, res, next) => EduCategory.getEduCategory(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => EduCategory.createEduCategory(req, res));

EduCategoryRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => EduCategory.getEduCategoryById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => EduCategory.putEduCategory(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => EduCategory.deleteEduCategory(req, res, next));

module.exports = EduCategoryRouter;
