
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Sponsorship = require('../controllers/Sponsorship');
const validator = require('express-validator/check');
const SponsorshipRouter = express.Router();

/**
 *  Generic Sponsorship apis.
 *  */
SponsorshipRouter.route('/')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => Sponsorship.getSponsorships(req, res, next));

SponsorshipRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => Sponsorship.getSponsorship(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => Sponsorship.putSponsorship(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => Sponsorship.deleteSponsorship(req, res, next));

SponsorshipRouter.route('/history/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next);
            }
        },
        (req, res, next) => Sponsorship.getUserSponsorshipHistory(req, res, next));

SponsorshipRouter.route('/active/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'USER', 'ACCOUNTANT']);
            }
        },
        (req, res, next) => Sponsorship.getUserActiveSponsorships(req, res, next));

SponsorshipRouter.route('/cash-out/:userId/:id')
    .post([validator.param('userId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next);
            }
        },
        (req, res, next) => Sponsorship.cashOutSponsorship(req, res, next));

SponsorshipRouter.route('/completed')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => Sponsorship.markCompletedSponsorship(req, res, next));

SponsorshipRouter.route('/cancel/:userId/:id')
    .post([validator.param('userId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next);
            }
        },
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => Sponsorship.cancelSponsorship(req, res, next));
module.exports = SponsorshipRouter;
