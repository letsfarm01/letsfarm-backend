
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Wallet = require('../controllers/Wallet');
const validator = require('express-validator/check');
const WalletRouter = express.Router();

/**
 *  Generic Wallet apis.
 *  */
WalletRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => Wallet.getWallets(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => Wallet.createWallet(req, res));

WalletRouter.route('/withdraw')
.post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
    (req, res, next) => Wallet.withdrawWallet(req, res));

WalletRouter.route('/user/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Wallet.getUserWallet(req, res, next));

WalletRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Wallet.getWallet(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Wallet.putWallet(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Wallet.deleteWallet(req, res, next));

module.exports = WalletRouter;
