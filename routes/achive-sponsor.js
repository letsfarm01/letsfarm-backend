
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const ArchiveSponsor = require('../controllers/ArchiveSponsor');
const validator = require('express-validator/check');
const ArchiveSponsorRouter = express.Router();

/**
 *  Generic ArchiveSponsor apis.
 *  */
ArchiveSponsorRouter.route('/')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => ArchiveSponsor.getArchiveSponsors(req, res, next));

ArchiveSponsorRouter.route('/user/email')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'USER', 'ACCOUNTANT']),
        (req, res, next) => ArchiveSponsor.getUserArchiveSponsor(req, res, next));

ArchiveSponsorRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ArchiveSponsor.getArchiveSponsor(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => ArchiveSponsor.putArchiveSponsor(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => ArchiveSponsor.deleteArchiveSponsor(req, res, next));
ArchiveSponsorRouter.route('/completed')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => ArchiveSponsor.markCompletedSponsorship(req, res, next));


module.exports = ArchiveSponsorRouter;
