
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const contactUs = require('../controllers/ContactUs');
const validator = require('express-validator/check');
const contactUsRouter = express.Router();

/**
 *  Generic contactUs apis.
 *  */
contactUsRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'), (req, res, next) => contactUs.getContactUs(req, res, next))
    .post((req, res, next) => contactUs.createContactUs(req, res))

contactUsRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => contactUs.getContactUsById(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => contactUs.deleteContactUs(req, res, next));

module.exports = contactUsRouter;
