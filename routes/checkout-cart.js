
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const CheckoutCart = require('../controllers/CheckoutCart');
const validator = require('express-validator/check');
const CheckoutCartRouter = express.Router();

/**
 *  Generic CheckoutCart apis.
 *  */
CheckoutCartRouter.route('/farm-shop')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => CheckoutCart.createCheckoutCart(req, res));

CheckoutCartRouter.route('/calculate-charge')
    .post((req, res, next) => CheckoutCart.calculateCharge(req, res));

CheckoutCartRouter.route('/charge-existing-card')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => CheckoutCart.checkoutWithExitingCard(req, res));

CheckoutCartRouter.route('/paystack-verify')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => CheckoutCart.verifyPayment(req, res));

/*
CheckoutCartRouter.route('/paystack-verify-manually')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => CheckoutCart.paystackConfirmedTransactionManually(req, res));
*/

CheckoutCartRouter.route('/paystack-webhook')
    .post((req, res, next) => CheckoutCart.paystackWebhook(req, res));
CheckoutCartRouter.route('/confirm-bank-transfer')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => CheckoutCart.confirmBankTransfer(req, res));

module.exports = CheckoutCartRouter;
