
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const FarmShop = require('../controllers/FarmShop');
const validator = require('express-validator/check');
const FarmShopRouter = express.Router();

/**
 *  Generic FarmShop apis.
 *  */
FarmShopRouter.route('/')
    .get((req, res, next) => FarmShop.getFarmShops(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => FarmShop.createFarmShop(req, res));

FarmShopRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            FarmShop.getFarmShopById(req, res, next)
        }
    })
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmShop.putFarmShop(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmShop.deleteFarmShop(req, res, next));

FarmShopRouter.route('/search-farm-shop')
    .post((req, res, next) => FarmShop.searchFarmShop(req, res));

FarmShopRouter.route('/filter-farm-shop')
    .post((req, res, next) => FarmShop.filterFarmShop(req, res));

module.exports = FarmShopRouter;
