const express = require('express');
const bodyParser = require('body-parser');
const authentication = require('../controllers/Authentication');
const controllerService = require('../services/controllerServices');
const authRouter = express.Router();
authRouter.use(bodyParser.json());

authRouter.route('/')
    .all((req, res, next) => authentication.returnMessage(req, res, next));
authRouter.route('/login')
    .post((req, res, next) => authentication.authenticateUser(req, res, next) );
authRouter.route('/social-login')
    .post((req, res, next) => authentication.socialLogin(req, res, next, next) );
authRouter.route('/recaptcha')
    .post((req, res, next) => authentication.recaptchaProcess(req, res, next) );
authRouter.route('/logout')
    .post((req, res, next) => authentication.logoutUser(req, res, next));

authRouter.route('/register')
    .post((req, res, next) => authentication.register(req, res));

authRouter.route('/social-register')
    .post((req, res, next) => authentication.socialRegister(req, res, next));

authRouter.route('/reset-password')
    .post((req, res) => authentication.resetPassword(req, res));

authRouter.route('/forgot-password')
    .post((req, res) => authentication.forgotPassword(req, res));

authRouter.route('/verify-signup')
    .post((req, res) => authentication.verifySignUp(req, res));

authRouter.route('/resend-verify-link')
    .post((req, res) => authentication.resendVerificationLink(req, res));

authRouter.route('/change-password')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next), (req, res) => authentication.changePassword(req, res));

module.exports = authRouter;


// User Control System
