
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const ShippingAddress = require('../controllers/ShippingAddress');
const validator = require('express-validator/check');
const ShippingAddressRouter = express.Router();

ShippingAddressRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => ShippingAddress.getShippingAddress(req, res))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => ShippingAddress.createShippingAddress(req, res));


ShippingAddressRouter.route('/user/:userId/default')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.getUserDefaultShippingAddresses(req, res, next));
ShippingAddressRouter.route('/user/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.getUserShippingAddresses(req, res, next))
    .put([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.setDefaultAddress(req, res, next));

ShippingAddressRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.getShippingAddressById(req, res, next))

    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.putShippingAddress(req, res, next))

    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => ShippingAddress.deleteShippingAddress(req, res, next));


module.exports = ShippingAddressRouter;
