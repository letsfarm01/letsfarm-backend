
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const WithdrawalRequest = require('../controllers/WithdrawalRequest');
const validator = require('express-validator/check');
const WithdrawalRequestRouter = express.Router();

/**
 *  Generic WithdrawalRequest apis.
 *  */
WithdrawalRequestRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.getWithdrawalRequests(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['USER']),
        (req, res, next) => WithdrawalRequest.newWithdrawalRequest(req, res));

WithdrawalRequestRouter.route('/accept-request')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.acceptWithdrawalRequest(req, res));

WithdrawalRequestRouter.route('/manual-accept-request')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.manualApproveWithdrawalRequest(req, res));

WithdrawalRequestRouter.route('/reject-request')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.rejectWithdrawalRequest(req, res));

WithdrawalRequestRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.getWithdrawalRequest(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => WithdrawalRequest.putWithdrawalRequest(req, res, next));

module.exports = WithdrawalRequestRouter;
