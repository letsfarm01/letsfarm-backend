
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const PaymentChannel = require('../controllers/PaymentChannel');
const validator = require('express-validator/check');
const PaymentChannelRouter = express.Router();

/**
 *  Generic PaymentChannel apis.
 *  */
PaymentChannelRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => PaymentChannel.getPaymentChannels(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => PaymentChannel.createPaymentChannel(req, res));

PaymentChannelRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => PaymentChannel.getPaymentChannel(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => PaymentChannel.putPaymentChannel(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => PaymentChannel.deletePaymentChannel(req, res, next));

module.exports = PaymentChannelRouter;
