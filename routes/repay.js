
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Repay = require('../controllers/Repay');
const validator = require('express-validator/check');
const RepayRouter = express.Router();

/**
 *  Generic Repay apis.
 *  */
RepayRouter.route('/generate-repay')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => Repay.createRepay(req, res));
RepayRouter.route('/search-ongoing-repay')
    .post((req, res, next) => Repay.getRepaySponsorship(req, res));
RepayRouter.route('/repay-by-year/:year')
    .get((req, res, next) => Repay.getRepays(req, res, next));
RepayRouter.route('/get-current-year')
    .get((req, res, next) => Repay.getCurrentYear(req, res, next));
RepayRouter.route('/auto-get-repay')
    .get((req, res, next) => Repay.autoGetRepay(req, res, next));
RepayRouter.route('/:id')
    /*.get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Repay.getRepay(req, res, next))*/
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Repay.putRepay(req, res, next))
    /*.delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => Repay.deleteRepay(req, res, next));*/

module.exports = RepayRouter;
