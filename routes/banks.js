
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Banks = require('../controllers/Bank');
const validator = require('express-validator/check');
const BanksRouter = express.Router();

/**
 *  Generic Banks apis.
 *  */
BanksRouter.route('/')
    .get((req, res, next) => Banks.getBanks(req, res, next))
    .post((req, res, next) => Banks.createBank(req, res));

BanksRouter.route('/payment/bank-transfer/old')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Banks.getOldPaymentProofs(req, res));
BanksRouter.route('/payment/bank-transfer')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Banks.getPaymentProofs(req, res))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Banks.saveProof(req, res));

module.exports = BanksRouter;
