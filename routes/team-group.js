
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const TeamGroup = require('../controllers/TeamGroup');
const validator = require('express-validator/check');
const TeamGroupRouter = express.Router();

/**
 *  Generic TeamGroup apis.
 *  */
TeamGroupRouter.route('/')
    .get((req, res, next) => TeamGroup.getTeamGroups(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => TeamGroup.createTeamGroup(req, res));

TeamGroupRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => TeamGroup.getTeamGroup(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => TeamGroup.putTeamGroup(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => TeamGroup.deleteTeamGroup(req, res, next));

module.exports = TeamGroupRouter;
