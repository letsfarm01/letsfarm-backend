
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const newsletter = require('../controllers/Newsletter');
const validator = require('express-validator/check');
const newsletterRouter = express.Router();

/**
 *  Generic newsletter apis.
 *  */
newsletterRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'), (req, res, next) => newsletter.getNewsletters(req, res, next))
    .post((req, res, next) => newsletter.createNewsletter(req, res))

newsletterRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => newsletter.getNewsletter(req, res, next))

    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => newsletter.putNewsletter(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => newsletter.deleteNewsletter(req, res, next));

module.exports = newsletterRouter;
