
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Profile = require('../controllers/Profile');
const validator = require('express-validator/check');
const ProfileRouter = express.Router();

/**
 *  Generic Profile apis.
 *  */
ProfileRouter.route('/bank-settings-admin')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Profile.getBankSettings(req, res, next));

ProfileRouter.route('/next-of-kin-admin')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Profile.getNextKins(req, res, next));

ProfileRouter.route('/business-info-admin')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Profile.getBusinessInfos(req, res, next));

ProfileRouter.route('/bank-setting/:userId')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Profile.createBankSetting(req, res))
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['USER', 'SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => Profile.getBankSetting(req, res, next))
    .put([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Profile.putBankSetting(req, res, next));

ProfileRouter.route('/next-of-kin/:userId')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Profile.createNextKin(req, res))
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['USER', 'SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => Profile.getNextKin(req, res, next))
    .put([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Profile.putNextKin(req, res, next));


ProfileRouter.route('/business-info/:userId')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Profile.createBusinessInfo(req, res))
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['USER', 'SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => Profile.getBusinessInfo(req, res, next))
    .put([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Profile.putBusinessInfo(req, res, next));


module.exports = ProfileRouter;
