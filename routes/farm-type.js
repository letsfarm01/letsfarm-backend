
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const farmType = require('../controllers/FarmType');
const validator = require('express-validator/check');
const farmTypeRouter = express.Router();

/**
 *  Generic farmType apis.
 *  */
farmTypeRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => farmType.getFarmTypes(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => farmType.createFarmType(req, res));
farmTypeRouter.route('/active-farm-types')
    .get((req, res, next) => farmType.getActiveFarmType(req, res, next));

farmTypeRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => farmType.getFarmTypeById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => farmType.putFarmType(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => farmType.deleteFarmType(req, res, next));

module.exports = farmTypeRouter;
