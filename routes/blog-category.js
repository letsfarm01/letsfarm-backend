
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const BlogCategory = require('../controllers/BlogCategory');
const validator = require('express-validator/check');
const BlogCategoryRouter = express.Router();

/**
 *  Generic BlogCategory apis.
 *  */
BlogCategoryRouter.route('/')
    .get((req, res, next) => BlogCategory.getBlogCategories(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => BlogCategory.createBlogCategory(req, res));

BlogCategoryRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => BlogCategory.getBlogCategoryById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => BlogCategory.putBlogCategory(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => BlogCategory.deleteBlogCategory(req, res, next));

module.exports = BlogCategoryRouter;
