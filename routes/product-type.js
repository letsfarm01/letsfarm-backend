
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const productType = require('../controllers/ProductType');
const validator = require('express-validator/check');
const productTypeRouter = express.Router();

/**
 *  Generic productType apis.
 *  */
productTypeRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => productType.getProductTypes(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => productType.createProductType(req, res));
productTypeRouter.route('/active-product-types')
    .get((req, res, next) => productType.getActiveProductType(req, res, next));

productTypeRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => productType.getProductTypeById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => productType.putProductType(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => productType.deleteProductType(req, res, next));

module.exports = productTypeRouter;
