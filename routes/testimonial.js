
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const testimonial = require('../controllers/Testimonial');
const validator = require('express-validator/check');
const testimonialRouter = express.Router();

/**
 *  Generic testimonial apis.
 *  */
testimonialRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => testimonial.getTestimonials(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => testimonial.createTestimonial(req, res));
testimonialRouter.route('/is_showing')
    .get((req, res, next) => testimonial.getIsShowingTestimonials(req, res, next));

testimonialRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => testimonial.getTestimonialById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => testimonial.putTestimonial(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => testimonial.deleteTestimonial(req, res, next));

module.exports = testimonialRouter;
