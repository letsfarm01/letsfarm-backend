
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const CompletedSponsor = require('../controllers/CompletedSponsor');
const validator = require('express-validator/check');
const completedSponsorRouter = express.Router();

/**
 *  Generic CompletedSponsor apis.
 *  */
completedSponsorRouter.route('/')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => CompletedSponsor.getCompletedSponsors(req, res, next));

completedSponsorRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => CompletedSponsor.getCompletedSponsor(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => CompletedSponsor.putCompletedSponsor(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    },  (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => CompletedSponsor.deleteCompletedSponsor(req, res, next));

completedSponsorRouter.route('/sendToWallet')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => controllerService.validateFundWallet(req, res, next),
        (req, res, next) => CompletedSponsor.sendOneToWallet(req, res, next));


module.exports = completedSponsorRouter;
