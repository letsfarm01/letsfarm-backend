
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const FAQ = require('../controllers/FAQ');
const validator = require('express-validator/check');
const FAQRouter = express.Router();

/**
 *  Generic FAQ apis.
 *  */
FAQRouter.route('/')
    .get((req, res, next) => FAQ.getFAQs(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => FAQ.createFAQ(req, res));

FAQRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FAQ.getFAQ(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FAQ.putFAQ(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FAQ.deleteFAQ(req, res, next));

module.exports = FAQRouter;
