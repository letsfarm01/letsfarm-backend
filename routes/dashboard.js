
const express = require('express');
const controllerService = require('../services/controllerServices');
const Dashboard = require('../controllers/Dashboard');
const DashboardRouter = express.Router();

/**
 *  Generic Dashboard apis.
 *  */
DashboardRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Dashboard.getDashboard(req, res, next));

DashboardRouter.route('/user/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Dashboard.adminGetUserDashboard(req, res, next));

DashboardRouter.route('/admin')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER', 'ACCOUNTANT']),
        (req, res, next) => Dashboard.getAdminDashboard(req, res, next));

DashboardRouter.route('/farm-available')
.get((req, res, next) => Dashboard.getFarmShopStatus(req, res, next));

DashboardRouter.route('/app-version')
.get((req, res, next) => Dashboard.getLatestVersion(req, res, next))
.post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
    (req, res, next) => Dashboard.addVersion(req, res, next));

module.exports = DashboardRouter;
