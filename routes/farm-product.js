
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const FarmProduct = require('../controllers/FarmProduct');
const validator = require('express-validator/check');
const FarmProductRouter = express.Router();

/**
 *  Generic FarmProduct apis.
 *  */
FarmProductRouter.route('/')
    .get((req, res, next) => FarmProduct.getFarmProducts(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => FarmProduct.createFarmProduct(req, res));

FarmProductRouter.route('/distinct')
    .get((req, res, next) => FarmProduct.getDistinctFarmProducts(req, res, next));

FarmProductRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            FarmProduct.getFarmProductById(req, res, next)
        }
    })
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmProduct.putFarmProduct(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmProduct.deleteFarmProduct(req, res, next));

FarmProductRouter.route('/search-farm-product')
    .post((req, res, next) => FarmProduct.searchFarmProduct(req, res));

FarmProductRouter.route('/filter-farm-product')
    .post((req, res, next) => FarmProduct.filterFarmProduct(req, res));

module.exports = FarmProductRouter;
