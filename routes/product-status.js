
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const ProductStatus = require('../controllers/ProductStatus');
const validator = require('express-validator/check');
const ProductStatusRouter = express.Router();

/**
 *  Generic ProductStatus apis.
 *  */
ProductStatusRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => ProductStatus.getProductStatus(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => ProductStatus.createProductStatus(req, res));
ProductStatusRouter.route('/active-product-status')
    .get((req, res, next) => ProductStatus.getActiveProductStatus(req, res, next));

ProductStatusRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => ProductStatus.getProductStatusById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => ProductStatus.putProductStatus(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => ProductStatus.deleteProductStatus(req, res, next));

module.exports = ProductStatusRouter;
