
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Referral = require('../controllers/Referral');
const validator = require('express-validator/check');
const ReferralRouter = express.Router();

/**
 *  Generic Referral apis.
 *  */
ReferralRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']),
        (req, res, next) => Referral.getReferrals(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Referral.createReferral(req, res, next));


ReferralRouter.route('/user/:userId')
    .get([validator.param('userId').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Referral.getUserReferrals(req, res, next));

ReferralRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Referral.getReferral(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'ACCOUNTANT']);
        }
    }, (req, res, next) => Referral.deleteReferral(req, res, next));

module.exports = ReferralRouter;
