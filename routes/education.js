
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Education = require('../controllers/Education');
const validator = require('express-validator/check');
const EducationRouter = express.Router();

/**
 *  Generic Education apis.
 *  */
EducationRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => Education.getEducations(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']),
        (req, res, next) => Education.createEducation(req, res));

EducationRouter.route('/publish')
    .get((req, res, next) => Education.getPublishedEducations(req, res, next));

EducationRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            Education.getEducationById(req, res, next)
        }
    })
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => Education.putEducation(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => Education.deleteEducation(req, res, next));

EducationRouter.route('/search-education')
    .post((req, res, next) => Education.searchEducation(req, res));

EducationRouter.route('/filter-education')
    .post((req, res, next) => Education.filterEducation(req, res));

module.exports = EducationRouter;
