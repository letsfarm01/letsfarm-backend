
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const FarmStatus = require('../controllers/FarmStatus');
const validator = require('express-validator/check');
const FarmStatusRouter = express.Router();

/**
 *  Generic FarmStatus apis.
 *  */
FarmStatusRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => FarmStatus.getFarmStatus(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => FarmStatus.createFarmStatus(req, res));
FarmStatusRouter.route('/active-farm-status')
    .get((req, res, next) => FarmStatus.getActiveFarmStatus(req, res, next));

FarmStatusRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmStatus.getFarmStatusById(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmStatus.putFarmStatus(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => FarmStatus.deleteFarmStatus(req, res, next));

module.exports = FarmStatusRouter;
