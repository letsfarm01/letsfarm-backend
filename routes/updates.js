
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Updates = require('../controllers/Updates');
const validator = require('express-validator/check');
const UpdatesRouter = express.Router();

/**
 *  Generic Updates apis.
 *  */
UpdatesRouter.route('/')
    .get((req, res, next) => Updates.getUpdates(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER']),
        (req, res, next) => Updates.createUpdate(req, res));
UpdatesRouter.route('/get-farm-updates-and-notifications')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['USER']),
        (req, res, next) => Updates.getFarmUpdates(req, res, next));
UpdatesRouter.route('/push-updates')
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, ['SUPER']),
        (req, res, next) => Updates.pushUpdates(req, res, next));

UpdatesRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            Updates.getUpdateById(req, res, next)
        }
    })
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER']);
        }
    }, (req, res, next) => Updates.putUpdate(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER']);
        }
    }, (req, res, next) => Updates.deleteUpdate(req, res, next));

module.exports = UpdatesRouter;
