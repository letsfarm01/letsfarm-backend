
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const BlogComment = require('../controllers/BlogComment');
const validator = require('express-validator/check');
const BlogCommentRouter = express.Router();

/**
 *  Generic BlogComment apis.
 *  */
BlogCommentRouter.route('/')
    .post((req, res, next) => BlogComment.createBlogComment(req, res));
BlogCommentRouter.route('/education/:educationId')
    .get(
        [validator.param('educationId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
            }
        },
        (req, res, next) => BlogComment.getBlogComments(req, res, next));

BlogCommentRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => BlogComment.getBlogCommentById(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => BlogComment.deleteBlogComment(req, res, next));

module.exports = BlogCommentRouter;
