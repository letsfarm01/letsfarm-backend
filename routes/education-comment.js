
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const EducationComment = require('../controllers/EducationComment');
const validator = require('express-validator/check');
const EducationCommentRouter = express.Router();

/**
 *  Generic EducationComment apis.
 *  */
EducationCommentRouter.route('/')
    .post((req, res, next) => EducationComment.createEducationComment(req, res));
EducationCommentRouter.route('/education/:educationId')
    .get(
        [validator.param('educationId').isMongoId().trim()], (req, res, next) => {
            if(validateService.validateObjectId(res, req)) {
                controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
            }
        },
        (req, res, next) => EducationComment.getEducationComments(req, res, next));

EducationCommentRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => EducationComment.getEducationCommentById(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, ['SUPER', 'BLOGGER']);
        }
    }, (req, res, next) => EducationComment.deleteEducationComment(req, res, next));

module.exports = EducationCommentRouter;
