
const express = require('express');
const BlogLike = require('../controllers/BlogLike');
const BlogLikeRouter = express.Router();

/**
 *  Generic BlogLike apis.
 *  */
BlogLikeRouter.route('/')
    .post((req, res, next) => BlogLike.likePost(req, res));
BlogLikeRouter.route('/check-user-status')
    .post((req, res, next) => BlogLike.checkFeelingOnPost(req, res));

module.exports = BlogLikeRouter;
