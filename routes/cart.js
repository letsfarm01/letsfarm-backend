
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const Cart = require('../controllers/Cart');
const validator = require('express-validator/check');
const CartRouter = express.Router();

/**
 *  Generic Cart apis.
 *  */
CartRouter.route('/')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => Cart.getCarts(req, res, next))
    .post((req, res, next) => Cart.createCart(req, res));

CartRouter.route('/product-to-cart-bulk')
    .post((req, res, next) => Cart.bulkProductToCart(req, res));

CartRouter.route('/product-to-cart')
    .post((req, res, next) => Cart.createProductCart(req, res));

CartRouter.route('/user-count/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Cart.getUserCartCount(req, res, next));

CartRouter.route('/user-cart/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Cart.getUserCarts(req, res, next));

CartRouter.route('/user-product-cart/:userId')
    .get((req, res, next) => controllerService.checkAuthorizationToken(req, res, next),
        (req, res, next) => Cart.getUserProductCarts(req, res, next));

CartRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Cart.getCart(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Cart.putCart(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => Cart.deleteCart(req, res, next));

module.exports = CartRouter;
