
const express = require('express');
const validateService = require('../services/validateService');
const controllerService = require('../services/controllerServices');
const TeamMember = require('../controllers/TeamMember');
const validator = require('express-validator/check');
const TeamMemberRouter = express.Router();

/**
 *  Generic TeamMember apis.
 *  */
TeamMemberRouter.route('/')
    .get((req, res, next) => TeamMember.getTeamMembers(req, res, next))
    .post((req, res, next) => controllerService.checkAuthorizationToken(req, res, next, 'SUPER'),
        (req, res, next) => TeamMember.createTeamMember(req, res));

TeamMemberRouter.route('/:id')
    .get([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next);
        }
    }, (req, res, next) => TeamMember.getTeamMember(req, res, next))
    .put([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => TeamMember.putTeamMember(req, res, next))
    .delete([validator.param('id').isMongoId().trim()], (req, res, next) => {
        if(validateService.validateObjectId(res, req)) {
            controllerService.checkAuthorizationToken(req, res, next, 'SUPER');
        }
    }, (req, res, next) => TeamMember.deleteTeamMember(req, res, next));

module.exports = TeamMemberRouter;
